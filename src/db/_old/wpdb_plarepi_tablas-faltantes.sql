-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 26-03-2020 a las 16:59:19
-- Versión del servidor: 5.7.23
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wpdb_plarepi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_actionscheduler_actions`
--

DROP TABLE IF EXISTS `baum_actionscheduler_actions`;
CREATE TABLE IF NOT EXISTS `baum_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `hook` (`hook`),
  KEY `status` (`status`),
  KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  KEY `args` (`args`),
  KEY `group_id` (`group_id`),
  KEY `last_attempt_gmt` (`last_attempt_gmt`),
  KEY `claim_id` (`claim_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3041 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_actionscheduler_actions`
--

INSERT INTO `baum_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(3016, 'action_scheduler/migration_hook', 'complete', '2020-03-24 18:47:20', '2020-03-24 18:47:20', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585075640;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585075640;}', 1, 1, '2020-03-24 18:47:25', '2020-03-24 12:47:25', 0, NULL),
(3017, 'action_scheduler/migration_hook', 'complete', '2020-03-24 18:47:27', '2020-03-24 18:47:27', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585075647;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585075647;}', 1, 1, '2020-03-24 18:47:29', '2020-03-24 12:47:29', 0, NULL),
(3018, 'action_scheduler/migration_hook', 'complete', '2020-03-24 20:54:42', '2020-03-24 20:54:42', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585083282;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585083282;}', 1, 1, '2020-03-24 20:55:23', '2020-03-24 14:55:23', 0, NULL),
(3019, 'action_scheduler/migration_hook', 'complete', '2020-03-24 20:55:22', '2020-03-24 20:55:22', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585083322;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585083322;}', 0, 1, '2020-03-24 20:55:23', '2020-03-24 14:55:23', 0, NULL),
(3020, 'action_scheduler/migration_hook', 'complete', '2020-03-24 20:55:24', '2020-03-24 20:55:24', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585083324;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585083324;}', 1, 1, '2020-03-24 20:55:56', '2020-03-24 14:55:56', 0, NULL),
(3021, 'wc-admin_import_customers', 'complete', '2020-03-25 15:48:49', '2020-03-25 15:48:49', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151329;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151329;}', 2, 1, '2020-03-25 15:49:10', '2020-03-25 09:49:10', 0, NULL),
(3022, 'wc-admin_import_orders', 'complete', '2020-03-25 15:48:49', '2020-03-25 15:48:49', '[3023]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151329;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151329;}', 2, 1, '2020-03-25 15:49:10', '2020-03-25 09:49:10', 0, NULL),
(3023, 'wc-admin_import_customers', 'complete', '2020-03-25 15:51:34', '2020-03-25 15:51:34', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151494;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151494;}', 2, 1, '2020-03-25 15:51:47', '2020-03-25 09:51:47', 0, NULL),
(3024, 'wc-admin_import_orders', 'complete', '2020-03-25 15:51:34', '2020-03-25 15:51:34', '[3024]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151494;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151494;}', 2, 1, '2020-03-25 15:51:47', '2020-03-25 09:51:47', 0, NULL),
(3025, 'wc-admin_import_customers', 'complete', '2020-03-25 15:53:44', '2020-03-25 15:53:44', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151624;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151624;}', 2, 1, '2020-03-25 15:55:16', '2020-03-25 09:55:16', 0, NULL),
(3026, 'wc-admin_import_orders', 'complete', '2020-03-25 15:55:25', '2020-03-25 15:55:25', '[3024]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151725;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151725;}', 2, 1, '2020-03-25 15:55:34', '2020-03-25 09:55:34', 0, NULL),
(3027, 'wc-admin_import_customers', 'complete', '2020-03-25 15:55:36', '2020-03-25 15:55:36', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585151736;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585151736;}', 2, 1, '2020-03-25 15:55:43', '2020-03-25 09:55:43', 0, NULL),
(3028, 'wc-admin_import_customers', 'complete', '2020-03-25 16:00:14', '2020-03-25 16:00:14', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585152014;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585152014;}', 2, 1, '2020-03-25 16:00:32', '2020-03-25 10:00:32', 0, NULL),
(3029, 'wc-admin_import_orders', 'complete', '2020-03-25 16:00:14', '2020-03-25 16:00:14', '[3025]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585152014;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585152014;}', 2, 1, '2020-03-25 16:00:32', '2020-03-25 10:00:32', 0, NULL),
(3030, 'wc-admin_import_orders', 'complete', '2020-03-25 16:32:43', '2020-03-25 16:32:43', '[3025]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585153963;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585153963;}', 2, 1, '2020-03-25 16:33:34', '2020-03-25 10:33:34', 0, NULL),
(3031, 'wc-admin_import_orders', 'complete', '2020-03-25 17:38:54', '2020-03-25 17:38:54', '[3023]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585157934;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585157934;}', 2, 1, '2020-03-25 17:38:58', '2020-03-25 11:38:58', 0, NULL),
(3032, 'wc-admin_import_customers', 'complete', '2020-03-25 17:47:44', '2020-03-25 17:47:44', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585158464;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585158464;}', 2, 1, '2020-03-25 17:53:37', '2020-03-25 11:53:37', 0, NULL),
(3033, 'wc-admin_import_orders', 'complete', '2020-03-25 17:47:44', '2020-03-25 17:47:44', '[3026]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585158464;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585158464;}', 2, 1, '2020-03-25 17:53:37', '2020-03-25 11:53:37', 0, NULL),
(3034, 'wc-admin_import_customers', 'complete', '2020-03-25 22:20:09', '2020-03-25 22:20:09', '[2]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585174809;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585174809;}', 2, 1, '2020-03-25 22:22:12', '2020-03-25 16:22:12', 0, NULL),
(3035, 'wc-admin_import_orders', 'complete', '2020-03-25 22:20:09', '2020-03-25 22:20:09', '[3029]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585174809;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585174809;}', 2, 1, '2020-03-25 22:22:12', '2020-03-25 16:22:12', 0, NULL),
(3036, 'wc-admin_import_orders', 'complete', '2020-03-25 22:30:21', '2020-03-25 22:30:21', '[3029]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585175421;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585175421;}', 2, 1, '2020-03-25 22:30:29', '2020-03-25 16:30:29', 0, NULL),
(3037, 'wc-admin_import_customers', 'complete', '2020-03-25 22:30:31', '2020-03-25 22:30:31', '[2]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585175431;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585175431;}', 2, 1, '2020-03-25 22:35:15', '2020-03-25 16:35:15', 0, NULL),
(3038, 'wc-admin_import_customers', 'complete', '2020-03-25 22:40:34', '2020-03-25 22:40:34', '[3]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585176034;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585176034;}', 2, 1, '2020-03-25 22:40:42', '2020-03-25 16:40:42', 0, NULL),
(3039, 'wc-admin_import_orders', 'complete', '2020-03-25 22:40:34', '2020-03-25 22:40:34', '[3030]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585176034;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585176034;}', 2, 1, '2020-03-25 22:40:42', '2020-03-25 16:40:42', 0, NULL),
(3040, 'wc-admin_import_orders', 'complete', '2020-03-25 23:07:30', '2020-03-25 23:07:30', '[3030]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585177650;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585177650;}', 2, 1, '2020-03-26 02:26:23', '2020-03-25 20:26:23', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_actionscheduler_claims`
--

DROP TABLE IF EXISTS `baum_actionscheduler_claims`;
CREATE TABLE IF NOT EXISTS `baum_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`claim_id`),
  KEY `date_created_gmt` (`date_created_gmt`)
) ENGINE=MyISAM AUTO_INCREMENT=514 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_actionscheduler_groups`
--

DROP TABLE IF EXISTS `baum_actionscheduler_groups`;
CREATE TABLE IF NOT EXISTS `baum_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `slug` (`slug`(191))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_actionscheduler_groups`
--

INSERT INTO `baum_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_actionscheduler_logs`
--

DROP TABLE IF EXISTS `baum_actionscheduler_logs`;
CREATE TABLE IF NOT EXISTS `baum_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_id`),
  KEY `action_id` (`action_id`),
  KEY `log_date_gmt` (`log_date_gmt`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_actionscheduler_logs`
--

INSERT INTO `baum_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 3016, 'action created', '2020-03-24 18:47:20', '2020-03-24 18:47:20'),
(2, 3016, 'action started via WP Cron', '2020-03-24 18:47:25', '2020-03-24 18:47:25'),
(3, 3016, 'action complete via WP Cron', '2020-03-24 18:47:25', '2020-03-24 18:47:25'),
(4, 3017, 'action created', '2020-03-24 18:47:27', '2020-03-24 18:47:27'),
(5, 3017, 'action started via WP Cron', '2020-03-24 18:47:29', '2020-03-24 18:47:29'),
(6, 3017, 'action complete via WP Cron', '2020-03-24 18:47:29', '2020-03-24 18:47:29'),
(7, 3018, 'action created', '2020-03-24 20:54:42', '2020-03-24 20:54:42'),
(8, 3018, 'action started via WP Cron', '2020-03-24 20:55:23', '2020-03-24 20:55:23'),
(9, 3018, 'action complete via WP Cron', '2020-03-24 20:55:23', '2020-03-24 20:55:23'),
(10, 3019, 'action started via WP Cron', '2020-03-24 20:55:23', '2020-03-24 20:55:23'),
(11, 3019, 'action complete via WP Cron', '2020-03-24 20:55:23', '2020-03-24 20:55:23'),
(12, 3020, 'action created', '2020-03-24 20:55:24', '2020-03-24 20:55:24'),
(13, 3020, 'action started via Async Request', '2020-03-24 20:55:56', '2020-03-24 20:55:56'),
(14, 3020, 'action complete via Async Request', '2020-03-24 20:55:56', '2020-03-24 20:55:56'),
(15, 3021, 'acción creada', '2020-03-25 15:48:44', '2020-03-25 15:48:44'),
(16, 3022, 'acción creada', '2020-03-25 15:48:44', '2020-03-25 15:48:44'),
(17, 3021, 'action started via Async Request', '2020-03-25 15:49:10', '2020-03-25 15:49:10'),
(18, 3021, 'action complete via Async Request', '2020-03-25 15:49:10', '2020-03-25 15:49:10'),
(19, 3022, 'action started via Async Request', '2020-03-25 15:49:10', '2020-03-25 15:49:10'),
(20, 3022, 'action complete via Async Request', '2020-03-25 15:49:10', '2020-03-25 15:49:10'),
(21, 3023, 'acción creada', '2020-03-25 15:51:29', '2020-03-25 15:51:29'),
(22, 3024, 'acción creada', '2020-03-25 15:51:29', '2020-03-25 15:51:29'),
(23, 3023, 'action started via Async Request', '2020-03-25 15:51:47', '2020-03-25 15:51:47'),
(24, 3023, 'action complete via Async Request', '2020-03-25 15:51:47', '2020-03-25 15:51:47'),
(25, 3024, 'action started via Async Request', '2020-03-25 15:51:47', '2020-03-25 15:51:47'),
(26, 3024, 'action complete via Async Request', '2020-03-25 15:51:47', '2020-03-25 15:51:47'),
(27, 3025, 'acción creada', '2020-03-25 15:53:39', '2020-03-25 15:53:39'),
(28, 3025, 'action started via WP Cron', '2020-03-25 15:55:15', '2020-03-25 15:55:15'),
(29, 3025, 'action complete via WP Cron', '2020-03-25 15:55:16', '2020-03-25 15:55:16'),
(30, 3026, 'acción creada', '2020-03-25 15:55:20', '2020-03-25 15:55:20'),
(31, 3027, 'acción creada', '2020-03-25 15:55:31', '2020-03-25 15:55:31'),
(32, 3026, 'action started via WP Cron', '2020-03-25 15:55:34', '2020-03-25 15:55:34'),
(33, 3026, 'action complete via WP Cron', '2020-03-25 15:55:34', '2020-03-25 15:55:34'),
(34, 3027, 'action started via Async Request', '2020-03-25 15:55:43', '2020-03-25 15:55:43'),
(35, 3027, 'action complete via Async Request', '2020-03-25 15:55:43', '2020-03-25 15:55:43'),
(36, 3028, 'acción creada', '2020-03-25 16:00:09', '2020-03-25 16:00:09'),
(37, 3029, 'acción creada', '2020-03-25 16:00:09', '2020-03-25 16:00:09'),
(38, 3028, 'action started via WP Cron', '2020-03-25 16:00:32', '2020-03-25 16:00:32'),
(39, 3028, 'action complete via WP Cron', '2020-03-25 16:00:32', '2020-03-25 16:00:32'),
(40, 3029, 'action started via WP Cron', '2020-03-25 16:00:32', '2020-03-25 16:00:32'),
(41, 3029, 'action complete via WP Cron', '2020-03-25 16:00:32', '2020-03-25 16:00:32'),
(42, 3030, 'acción creada', '2020-03-25 16:32:38', '2020-03-25 16:32:38'),
(43, 3030, 'action started via WP Cron', '2020-03-25 16:33:33', '2020-03-25 16:33:33'),
(44, 3030, 'action complete via WP Cron', '2020-03-25 16:33:34', '2020-03-25 16:33:34'),
(45, 3031, 'acción creada', '2020-03-25 17:38:49', '2020-03-25 17:38:49'),
(46, 3031, 'action started via Async Request', '2020-03-25 17:38:58', '2020-03-25 17:38:58'),
(47, 3031, 'action complete via Async Request', '2020-03-25 17:38:58', '2020-03-25 17:38:58'),
(48, 3032, 'acción creada', '2020-03-25 17:47:39', '2020-03-25 17:47:39'),
(49, 3033, 'acción creada', '2020-03-25 17:47:39', '2020-03-25 17:47:39'),
(50, 3032, 'action started via WP Cron', '2020-03-25 17:53:37', '2020-03-25 17:53:37'),
(51, 3032, 'action complete via WP Cron', '2020-03-25 17:53:37', '2020-03-25 17:53:37'),
(52, 3033, 'action started via WP Cron', '2020-03-25 17:53:37', '2020-03-25 17:53:37'),
(53, 3033, 'action complete via WP Cron', '2020-03-25 17:53:37', '2020-03-25 17:53:37'),
(54, 3034, 'acción creada', '2020-03-25 22:20:04', '2020-03-25 22:20:04'),
(55, 3035, 'acción creada', '2020-03-25 22:20:04', '2020-03-25 22:20:04'),
(56, 3034, 'action started via WP Cron', '2020-03-25 22:22:12', '2020-03-25 22:22:12'),
(57, 3034, 'action complete via WP Cron', '2020-03-25 22:22:12', '2020-03-25 22:22:12'),
(58, 3035, 'action started via WP Cron', '2020-03-25 22:22:12', '2020-03-25 22:22:12'),
(59, 3035, 'action complete via WP Cron', '2020-03-25 22:22:12', '2020-03-25 22:22:12'),
(60, 3036, 'acción creada', '2020-03-25 22:30:16', '2020-03-25 22:30:16'),
(61, 3037, 'acción creada', '2020-03-25 22:30:26', '2020-03-25 22:30:26'),
(62, 3036, 'action started via WP Cron', '2020-03-25 22:30:29', '2020-03-25 22:30:29'),
(63, 3036, 'action complete via WP Cron', '2020-03-25 22:30:29', '2020-03-25 22:30:29'),
(64, 3037, 'action started via WP Cron', '2020-03-25 22:35:15', '2020-03-25 22:35:15'),
(65, 3037, 'action complete via WP Cron', '2020-03-25 22:35:15', '2020-03-25 22:35:15'),
(66, 3038, 'acción creada', '2020-03-25 22:40:29', '2020-03-25 22:40:29'),
(67, 3039, 'acción creada', '2020-03-25 22:40:29', '2020-03-25 22:40:29'),
(68, 3038, 'action started via Async Request', '2020-03-25 22:40:42', '2020-03-25 22:40:42'),
(69, 3038, 'action complete via Async Request', '2020-03-25 22:40:42', '2020-03-25 22:40:42'),
(70, 3039, 'action started via Async Request', '2020-03-25 22:40:42', '2020-03-25 22:40:42'),
(71, 3039, 'action complete via Async Request', '2020-03-25 22:40:42', '2020-03-25 22:40:42'),
(72, 3040, 'acción creada', '2020-03-25 23:07:25', '2020-03-25 23:07:25'),
(73, 3040, 'action started via WP Cron', '2020-03-26 02:26:23', '2020-03-26 02:26:23'),
(74, 3040, 'action complete via WP Cron', '2020-03-26 02:26:23', '2020-03-26 02:26:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_admin_notes`
--

DROP TABLE IF EXISTS `baum_wc_admin_notes`;
CREATE TABLE IF NOT EXISTS `baum_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_admin_notes`
--

INSERT INTO `baum_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-24 18:47:23', NULL, 0),
(2, 'wc-admin-store-notice-setting-moved', 'info', 'en_US', 'Looking for the Store Notice setting?', 'It can now be found in the Customizer.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-24 18:47:23', NULL, 0),
(3, 'wc-admin-add-first-product', 'info', 'en_US', 'Add your first product', 'Grow your revenue by adding products to your store. Add products manually, import from a sheet, or migrate from another platform.', 'product', '{}', 'unactioned', 'woocommerce-admin', '2020-03-24 18:47:25', NULL, 0),
(4, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-24 18:47:27', NULL, 0),
(5, 'wc-admin-onboarding-profiler-reminder', 'update', 'en_US', 'Welcome to WooCommerce! Set up your store and start selling', 'We\'re here to help you going through the most important steps to get your store up and running.', 'info', '{}', 'actioned', 'woocommerce-admin', '2020-03-25 06:47:45', NULL, 0),
(6, 'wc-admin-orders-milestone', 'info', 'en_US', 'First order', 'Congratulations on getting your first order from a customer! Learn how to manage your orders.', 'trophy', '{}', 'unactioned', 'woocommerce-admin', '2020-03-25 16:47:29', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_admin_note_actions`
--

DROP TABLE IF EXISTS `baum_wc_admin_note_actions`;
CREATE TABLE IF NOT EXISTS `baum_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`),
  KEY `note_id` (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_admin_note_actions`
--

INSERT INTO `baum_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', 'actioned', 0),
(2, 2, 'open-customizer', 'Open Customizer', 'customize.php', 'actioned', 0),
(3, 3, 'add-a-product', 'Add a product', 'http://localhost/plarepiwp/wp-admin/post-new.php?post_type=product', 'actioned', 1),
(4, 4, 'connect', 'Connect', '?page=wc-addons&section=helper', 'actioned', 0),
(5, 5, 'continue-profiler', 'Continue Store Setup', 'http://localhost/plarepiwp/wp-admin/admin.php?page=wc-admin&enable_onboarding=1', 'unactioned', 1),
(6, 5, 'skip-profiler', 'Skip Setup', 'http://localhost/plarepiwp/wp-admin/admin.php?page=wc-admin&reset_profiler=0', 'actioned', 0),
(7, 6, 'learn-more', 'Aprender más', 'https://docs.woocommerce.com/document/managing-orders/', 'actioned', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_category_lookup`
--

DROP TABLE IF EXISTS `baum_wc_category_lookup`;
CREATE TABLE IF NOT EXISTS `baum_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`category_tree_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_category_lookup`
--

INSERT INTO `baum_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(29, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_customer_lookup`
--

DROP TABLE IF EXISTS `baum_wc_customer_lookup`;
CREATE TABLE IF NOT EXISTS `baum_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_customer_lookup`
--

INSERT INTO `baum_wc_customer_lookup` (`customer_id`, `user_id`, `username`, `first_name`, `last_name`, `email`, `date_last_active`, `date_registered`, `country`, `postcode`, `city`, `state`) VALUES
(1, 1, 'admin_wpstarter', 'Keylor', 'Plarepi', 'keylor@baum.digital', '2020-03-26 06:00:00', '2018-04-30 20:55:36', '', '', '', ''),
(2, 2, 'keylor.mailinator', 'Keylor', 'Mailinator', 'keylorcr@mailinator.com', '2020-03-25 06:00:00', '2020-03-26 04:19:56', '', '', '', ''),
(3, 3, 'fabian.lorem', 'Fabián', 'Lorem', 'fabiancr@mailinator.com', '2020-03-25 06:00:00', '2020-03-26 04:40:23', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_order_coupon_lookup`
--

DROP TABLE IF EXISTS `baum_wc_order_coupon_lookup`;
CREATE TABLE IF NOT EXISTS `baum_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`coupon_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_order_product_lookup`
--

DROP TABLE IF EXISTS `baum_wc_order_product_lookup`;
CREATE TABLE IF NOT EXISTS `baum_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `customer_id` (`customer_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_order_product_lookup`
--

INSERT INTO `baum_wc_order_product_lookup` (`order_item_id`, `order_id`, `product_id`, `variation_id`, `customer_id`, `date_created`, `product_qty`, `product_net_revenue`, `product_gross_revenue`, `coupon_amount`, `tax_amount`, `shipping_amount`, `shipping_tax_amount`) VALUES
(1, 3023, 3016, 3019, 1, '2020-03-25 09:48:44', 1, 18000, 18000, 0, 0, 0, 0),
(5, 3025, 3016, 3019, 1, '2020-03-25 10:00:09', 1, 18000, 18000, 0, 0, 0, 0),
(4, 3024, 3016, 3019, 1, '2020-03-25 09:51:29', 1, 18000, 18000, 0, 0, 0, 0),
(6, 3026, 3016, 3018, 1, '2020-03-25 11:47:39', 1, 36000, 36000, 0, 0, 0, 0),
(7, 3029, 3016, 3019, 2, '2020-03-25 16:20:04', 1, 18000, 18000, 0, 0, 0, 0),
(8, 3030, 3016, 3018, 3, '2020-03-25 16:40:29', 1, 36000, 36000, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_order_stats`
--

DROP TABLE IF EXISTS `baum_wc_order_stats`;
CREATE TABLE IF NOT EXISTS `baum_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `date_created` (`date_created`),
  KEY `customer_id` (`customer_id`),
  KEY `status` (`status`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_wc_order_stats`
--

INSERT INTO `baum_wc_order_stats` (`order_id`, `parent_id`, `date_created`, `date_created_gmt`, `num_items_sold`, `total_sales`, `tax_total`, `shipping_total`, `net_total`, `returning_customer`, `status`, `customer_id`) VALUES
(3023, 0, '2020-03-25 09:48:44', '2020-03-25 15:48:44', 1, 18000, 0, 0, 18000, 1, 'wc-cancelled', 1),
(3024, 0, '2020-03-25 09:51:29', '2020-03-25 15:51:29', 1, 18000, 0, 0, 18000, 0, 'wc-processing', 1),
(3025, 0, '2020-03-25 10:00:09', '2020-03-25 16:00:09', 1, 18000, 0, 0, 18000, 1, 'wc-processing', 1),
(3026, 0, '2020-03-25 11:47:39', '2020-03-25 17:47:39', 1, 36000, 0, 0, 36000, 1, 'wc-processing', 1),
(3029, 0, '2020-03-25 16:20:04', '2020-03-25 22:20:04', 1, 18000, 0, 0, 18000, 0, 'wc-processing', 2),
(3030, 0, '2020-03-25 16:40:29', '2020-03-25 22:40:29', 1, 36000, 0, 0, 36000, 0, 'wc-cancelled', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_wc_order_tax_lookup`
--

DROP TABLE IF EXISTS `baum_wc_order_tax_lookup`;
CREATE TABLE IF NOT EXISTS `baum_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`tax_rate_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
