-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 05-08-2019 a las 22:16:26
-- Versión del servidor: 5.7.23
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wpdb_wpstarter`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_ajaxsearchpro`
--

DROP TABLE IF EXISTS `baum_ajaxsearchpro`;
CREATE TABLE IF NOT EXISTS `baum_ajaxsearchpro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_ajaxsearchpro_priorities`
--

DROP TABLE IF EXISTS `baum_ajaxsearchpro_priorities`;
CREATE TABLE IF NOT EXISTS `baum_ajaxsearchpro_priorities` (
  `post_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`blog_id`),
  KEY `post_blog_id` (`post_id`,`blog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_ajaxsearchpro_statistics`
--

DROP TABLE IF EXISTS `baum_ajaxsearchpro_statistics`;
CREATE TABLE IF NOT EXISTS `baum_ajaxsearchpro_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_id` int(11) NOT NULL,
  `keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `num` int(11) NOT NULL,
  `last_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_asp_index`
--

DROP TABLE IF EXISTS `baum_asp_index`;
CREATE TABLE IF NOT EXISTS `baum_asp_index` (
  `doc` bigint(20) NOT NULL DEFAULT '0',
  `term` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `term_reverse` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `blogid` mediumint(9) NOT NULL DEFAULT '0',
  `content` mediumint(9) NOT NULL DEFAULT '0',
  `title` mediumint(9) NOT NULL DEFAULT '0',
  `comment` mediumint(9) NOT NULL DEFAULT '0',
  `tag` mediumint(9) NOT NULL DEFAULT '0',
  `link` mediumint(9) NOT NULL DEFAULT '0',
  `author` mediumint(9) NOT NULL DEFAULT '0',
  `category` mediumint(9) NOT NULL DEFAULT '0',
  `excerpt` mediumint(9) NOT NULL DEFAULT '0',
  `taxonomy` mediumint(9) NOT NULL DEFAULT '0',
  `customfield` mediumint(9) NOT NULL DEFAULT '0',
  `post_type` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `item` bigint(20) NOT NULL DEFAULT '0',
  `lang` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  UNIQUE KEY `doctermitem` (`doc`,`term`,`blogid`),
  KEY `term_ptype_bid_lang` (`term`(20),`post_type`(20),`blogid`,`lang`(10)),
  KEY `rterm_ptype_bid_lang` (`term_reverse`(20),`post_type`(20),`blogid`,`lang`(10))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_asp_index`
--

INSERT INTO `baum_asp_index` (`doc`, `term`, `term_reverse`, `blogid`, `content`, `title`, `comment`, `tag`, `link`, `author`, `category`, `excerpt`, `taxonomy`, `customfield`, `post_type`, `item`, `lang`) VALUES
(8, 'btndefault', 'tluafedntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btnprimary', 'yramirpntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'primary', 'yramirp', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btn', 'ntb', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'buttonalt', 'tlanottub', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'alt', 'tla', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'button', 'nottub', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'usa', 'asu', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'laborum', 'murobal', 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '10019', '91001', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ny', 'yn', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'manhattan', 'nattahnam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '700', '007', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'suite', 'etius', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '7th', 'ht7', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ave', 'eva', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '811', '118', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'inc.', '.cni', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'company', 'ynapmoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'just', 'tsuj', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'philosophical', 'lacihposolihp', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'better', 'retteb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'even', 'neve', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'p&gt', 'tg&p', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'blockquote&gt', 'tg&etouqkcolb', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'child', 'dlihc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'up', 'pu', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '&lt', 'tl&', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'marked', 'dekram', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'quote', 'etouq', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'witty', 'yttiw', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'famous', 'suomaf', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'some', 'emos', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sort', 'tros', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '4', '4', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '5', '5', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '6', '6', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'link', 'knil', 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'text', 'txet', 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'heading', 'gnidaeh', 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'value', 'eulav', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'name', 'eman', 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'description', 'noitpircsed', 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '3', '3', 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '2', '2', 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '1', '1', 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'item', 'meti', 1, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'list', 'tsil', 1, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'bunny', 'ynnub', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'buck', 'kcub', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'of', 'fo', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'big', 'gib', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'element.&nbsp', 'psbn&.tnemele', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'support', 'troppus', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'html5', '5lmth', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'not', 'ton', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'does', 'seod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'agent', 'tnega', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'user', 'resu', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'your', 'ruoy', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'video', 'oediv', 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '&nbsp', 'psbn&', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'svg', 'gvs', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'picture', 'erutcip', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'missing', 'gnissim', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'linked', 'deknil', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'image', 'egami', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'var', 'rav', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'default', 'tluafed', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sup', 'pus', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'u', 'u', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sub', 'bus', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'strong', 'gnorts', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'span', 'naps', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'small', 'llams', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'samp', 'pmas', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 's', 's', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'inside', 'edisni', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'q', 'q', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'mark', 'kram', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ins', 'sni', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'kbd', 'dbk', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'i', 'i', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'del', 'led', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'dfn', 'nfd', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'em', 'me', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'cite', 'etic', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'code', 'edoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'b', 'b', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'examples', 'selpmaxe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'with', 'htiw', 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'title', 'eltit', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'and', 'dna', 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'an', 'na', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'abbr', 'rbba', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'example', 'elpmaxe', 1, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'a', 'a', 1, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'element', 'tnemele', 1, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-dark', 'krad-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'the', 'eht', 1, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-light', 'thgil-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-danger', 'regnad-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-warning', 'gninraw-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-success', 'sseccus-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-default', 'tluafed-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.btn-primary', 'yramirp-ntb.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'bootstrap', 'partstoob', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.button', 'nottub.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, '.button.alt', 'tla.nottub.', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'buttons:', ':snottub', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'laborum.', '.murobal', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'est', 'tse', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'id', 'di', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'anim', 'mina', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'deserunt', 'tnuresed', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'mollit', 'tillom', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'qui', 'iuq', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'officia', 'aiciffo', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'culpa', 'apluc', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sunt', 'tnus', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'proident', 'tnediorp', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'non', 'non', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'cupidatat', 'tatadipuc', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'occaecat', 'taceacco', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sint', 'tnis', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'excepteur', 'ruetpecxe', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'pariatur', 'rutairap', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'nulla', 'allun', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'eu', 'ue', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'fugiat', 'taiguf', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'cillum', 'mullic', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'esse', 'esse', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'velit', 'tilev', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'voluptate', 'etatpulov', 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'in', 'ni', 1, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'reprehenderit', 'tiredneherper', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'irure', 'eruri', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'aute', 'etua', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'duis', 'siud', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'consequat', 'tauqesnoc', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'commodo', 'odommoc', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ea', 'ae', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ex', 'xe', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'aliquip', 'piuqila', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'nisi', 'isin', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'laboris', 'sirobal', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ullamco', 'ocmallu', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'exercitation', 'noitaticrexe', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'nostrud', 'durtson', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'quis', 'siuq', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'minim', 'minim', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'veniam', 'mainev', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ad', 'da', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'enim', 'mine', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'aliqua', 'auqila', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'magna', 'angam', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'dolore', 'erolod', 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'et', 'te', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'labore', 'erobal', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ut', 'tu', 1, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'incididunt', 'tnudidicni', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'tempor', 'ropmet', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'eiusmod', 'domsuie', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'do', 'od', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sed', 'des', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'elit', 'tile', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'adipisicing', 'gnicisipida', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'consectetur', 'rutetcesnoc', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'amet', 'tema', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'sit', 'tis', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'dolor', 'rolod', 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'ipsum', 'muspi', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'lorem', 'merol', 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'success', 'sseccus', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btnsuccess', 'sseccusntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'warning', 'gninraw', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btnwarning', 'gninrawntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'danger', 'regnad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btndanger', 'regnadntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'light', 'thgil', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btnlight', 'thgilntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'dark', 'krad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'btndark', 'kradntb', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'element&nbsp', 'psbn&tnemele', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'inc', 'cni', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'contenido___', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(8, 'contenido', 'odinetnoc', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, 'esta', 'atse', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, 'es', 'se', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, 'la', 'al', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, 'página', 'anigáp', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, '1', '1', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(6, 'página 1', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, 'es'),
(4, 'más', 'sám', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'ver', 'rev', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'hendrerit', 'tirerdneh', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'felis', 'silef', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(2943, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, '1', '1', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'vacante 1', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2943, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, '2', '2', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'vacante 2', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2945, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, '3', '3', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'vacante 3', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2946, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, '4', '4', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'vacante 4', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2947, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, '5', '5', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'vacante 5', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2948, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, '6', '6', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'vacante 6', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2949, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, '7', '7', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'dapibus', 'subipad', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'eget', 'tege', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'diam', 'maid', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'eros', 'sore', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'turpis', 'siprut', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'et', 'te', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'laoreet', 'teeroal', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'ex.', '.xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'ex', 'xe', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'vacante 7', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(2950, 'vacante', 'etnacav', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'post', 0, ''),
(4, 'inicio___', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'inicio', 'oicini', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'tellus', 'sullet', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'proin', 'niorp', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'velit', 'tilev', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'posuere', 'ereusop', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'aliquet', 'teuqila', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'efficitur', 'ruticiffe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'vivamus', 'sumaviv', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'ullamcorper', 'reprocmallu', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'nisi', 'isin', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'lorem', 'merol', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'ipsum', 'muspi', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'dolor', 'rolod', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'sit', 'tis', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'amet', 'tema', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'consectetur', 'rutetcesnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'adipiscing', 'gnicsipida', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'elit', 'tile', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'etiam', 'maite', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'quis', 'siuq', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'congue', 'eugnoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'ex', 'xe', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'mauris', 'siruam', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'nec', 'cen', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'sapien', 'neipas', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'a', 'a', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'quam', 'mauq', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'pulvinar', 'ranivlup', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'auctor', 'rotcua', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'sed', 'des', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'vitae', 'eativ', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'orci', 'icro', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'aliquam', 'mauqila', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'commodo', 'odommoc', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'neque', 'euqen', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'eget', 'tege', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'mollis', 'sillom', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'magna', 'angam', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'pharetra', 'arterahp', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'ligula', 'alugil', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'vel', 'lev', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'tempor', 'ropmet', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'dui', 'iud', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'urna', 'anru', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, ''),
(4, 'et', 'te', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'page', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_asp_synonyms`
--

DROP TABLE IF EXISTS `baum_asp_synonyms`;
CREATE TABLE IF NOT EXISTS `baum_asp_synonyms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `synonyms` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`,`lang`),
  KEY `keyword_lang` (`keyword`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_commentmeta`
--

DROP TABLE IF EXISTS `baum_commentmeta`;
CREATE TABLE IF NOT EXISTS `baum_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_comments`
--

DROP TABLE IF EXISTS `baum_comments`;
CREATE TABLE IF NOT EXISTS `baum_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_links`
--

DROP TABLE IF EXISTS `baum_links`;
CREATE TABLE IF NOT EXISTS `baum_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_options`
--

DROP TABLE IF EXISTS `baum_options`;
CREATE TABLE IF NOT EXISTS `baum_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2433 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_options`
--

INSERT INTO `baum_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wp-starter', 'yes'),
(2, 'home', 'http://localhost/wp-starter', 'yes'),
(3, 'blogname', 'WordPress Starter', 'yes'),
(4, 'blogdescription', 'by Baum', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'keylor@baum.digital', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '1', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:11:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:35:\"ajax-search-pro/ajax-search-pro.php\";i:2;s:27:\"autoptimize/autoptimize.php\";i:3;s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";i:4;s:31:\"page-builder-everywhere/pbe.php\";i:5;s:41:\"real-media-library/real-media-library.php\";i:6;s:33:\"w3-total-cache/w3-total-cache.php\";i:7;s:41:\"wordpress-importer/wordpress-importer.php\";i:8;s:27:\"wp-optimize/wp-optimize.php\";i:9;s:23:\"wp-smushit/wp-smush.php\";i:10;s:33:\"wps-hide-login/wps-hide-login.php\";}', 'yes'),
(2389, 'w3tc_extensions_hooks', '{\"actions\":[],\"filters\":[],\"next_check_date\":1565112940}', 'yes'),
(483, 'asp_version', '4988', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '-6', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'Divi', 'yes'),
(41, 'stylesheet', 'Divi-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '1', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:5:{s:27:\"autoptimize/autoptimize.php\";s:21:\"autoptimize_uninstall\";s:53:\"child-theme-configurator/child-theme-configurator.php\";s:22:\"chld_thm_cfg_uninstall\";s:25:\"sucuri-scanner/sucuri.php\";s:19:\"sucuriscanUninstall\";s:45:\"ewww-image-optimizer/ewww-image-optimizer.php\";s:30:\"ewww_image_optimizer_uninstall\";s:27:\"wp-optimize/wp-optimize.php\";s:21:\"wpo_uninstall_actions\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '4', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'baum_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:14:\"ure_edit_roles\";b:1;s:16:\"ure_create_roles\";b:1;s:16:\"ure_delete_roles\";b:1;s:23:\"ure_create_capabilities\";b:1;s:23:\"ure_delete_capabilities\";b:1;s:18:\"ure_manage_options\";b:1;s:15:\"ure_reset_roles\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'es_CR', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:16:{s:19:\"wp_inactive_widgets\";a:0:{}s:19:\"pbe-above-header-wa\";a:1:{i:0;s:13:\"custom_html-2\";}s:19:\"pbe-below-header-wa\";a:0:{}s:13:\"pbe-footer-wa\";a:0:{}s:20:\"pbe-above-content-wa\";a:0:{}s:20:\"pbe-below-content-wa\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:1:{i:0;s:13:\"custom_html-3\";}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:0:{}s:9:\"sidebar-6\";a:0:{}s:9:\"sidebar-7\";a:0:{}s:18:\"header-code-widget\";a:0:{}s:18:\"footer-code-widget\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'nonce_key', 'FznYvCcc(syiQu1_XqBi!r1u.%zq1)>z@B~/>GIRsYqD}7%1^V&&POr6`i$26f^q', 'no'),
(108, 'nonce_salt', 'mPk|8n_+Lw{A!91EEM=wXu|P%`>I:^Y?&dR8A2Q7ZX~ZX:P:6YAis`q_?:IVa|e<', 'no'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:3:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:0:\"\";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:13:{i:1565041090;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1565050527;a:1:{s:21:\"wpo_plugin_cron_tasks\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1565058114;a:1:{s:15:\"ao_cachechecker\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1565060136;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1565071440;a:1:{s:29:\"mc4wp_refresh_mailchimp_lists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1565103112;a:1:{s:25:\"sucuriscan_scheduled_scan\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1565103376;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1565105048;a:1:{s:21:\"et_builder_fonts_cron\";a:1:{s:32:\"552cbb9d6515dadbbc4718ad75114f08\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:1:{s:8:\"interval\";s:5:\"daily\";}s:8:\"interval\";i:86400;}}}i:1565108588;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1565113311;a:1:{s:26:\"importer_scheduled_cleanup\";a:1:{s:32:\"2937749bdb63b97e582698a4b62671c0\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:57;}}}}i:1565121289;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1566572189;a:1:{s:32:\"et_core_page_resource_auto_clear\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2592000;}}}s:7:\"version\";i:2;}', 'yes'),
(120, 'auth_key', 'sAXB#LUbMd{LV@HHr=t(O~h_;AAA-G&Lk*UXe4bcale0tizoMv@3o:hQeWbqF_U~', 'no'),
(754, 'wp_mail_smtp_debug', 'a:1:{i:0;s:110:\"Mailer: Default (none)\r\nPHPMailer was able to connect to SMTP server but failed while trying to send an email.\";}', 'no'),
(121, 'auth_salt', 'w_Jj )hAbMSbjgkmNJ_u}etW&kZX`>xGb-&esa7O33`7qK(H/Q>ZgtqAezST(/?0', 'no'),
(2407, '_site_transient_timeout_theme_roots', '1565032496', 'no'),
(2408, '_site_transient_theme_roots', 'a:3:{s:10:\"Divi-child\";s:7:\"/themes\";s:4:\"Divi\";s:7:\"/themes\";s:7:\"josefin\";s:7:\"/themes\";}', 'no'),
(122, 'logged_in_key', 'O+~+*D9e/fwo]mkf}}.JG!.4,CJo$+7Bgl_aleIpnvZT}0=&0&[lWH5FE5XGhf6h', 'no'),
(123, 'logged_in_salt', 'Tc|euSj)rc*z{2($$xBdbwk9<$mZjo.vHTAtG!dcK8W9sYfYfrSty(-ASFUp?Wrm', 'no'),
(374, 'theme_mods_Divi-child', 'a:7:{i:0;b:0;s:18:\"custom_css_post_id\";i:20;s:16:\"et_pb_css_synced\";s:3:\"yes\";s:18:\"nav_menu_locations\";a:3:{s:12:\"primary-menu\";i:2;s:14:\"secondary-menu\";i:0;s:11:\"footer-menu\";i:0;}s:39:\"et_updated_layouts_built_for_post_types\";s:3:\"yes\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1565030713;s:4:\"data\";a:13:{s:19:\"wp_inactive_widgets\";a:0:{}s:19:\"pbe-above-header-wa\";a:0:{}s:19:\"pbe-below-header-wa\";a:0:{}s:13:\"pbe-footer-wa\";a:0:{}s:20:\"pbe-above-content-wa\";a:0:{}s:20:\"pbe-below-content-wa\";a:0:{}s:18:\"header-code-widget\";a:1:{i:0;s:13:\"custom_html-2\";}s:18:\"footer-code-widget\";a:1:{i:0;s:13:\"custom_html-3\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:0:{}}}s:13:\"et_pb_widgets\";a:2:{s:5:\"areas\";a:0:{}s:6:\"number\";i:4;}}', 'yes'),
(1481, 'db_upgraded', '', 'yes'),
(450, 'PO_updating_plugin', '', 'yes'),
(451, 'PO_custom_post_type_support', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(452, 'PO_fuzzy_url_matching', '1', 'yes'),
(453, 'PO_version_num', '10.1', 'yes'),
(454, 'PO_disable_plugins_by_role', '0', 'yes'),
(455, 'PO_enabled_roles', 'a:0:{}', 'yes'),
(456, 'PO_pt_stored', 'a:0:{}', 'yes'),
(457, 'PO_display_debug_msg', '0', 'yes'),
(458, 'PO_debug_roles', 'a:1:{i:0;s:13:\"administrator\";}', 'yes'),
(459, 'PO_mobile_user_agents', 'a:20:{i:0;s:6:\"mobile\";i:1;s:4:\"bolt\";i:2;s:4:\"palm\";i:3;s:8:\"series60\";i:4;s:7:\"symbian\";i:5;s:6:\"fennec\";i:6;s:5:\"nokia\";i:7;s:6:\"kindle\";i:8;s:6:\"minimo\";i:9;s:8:\"netfront\";i:10;s:10:\"opera mini\";i:11;s:10:\"opera mobi\";i:12;s:12:\"semc-browser\";i:13;s:7:\"skyfire\";i:14;s:8:\"teashark\";i:15;s:5:\"uzard\";i:16;s:7:\"android\";i:17;s:10:\"blackberry\";i:18;s:6:\"iphone\";i:19;s:4:\"ipad\";}', 'yes'),
(476, '_amn_smtp_last_checked', '1534982400', 'yes'),
(469, 'wp_mail_smtp_version', '1.2.5', 'yes'),
(470, 'wp_mail_smtp', 'a:2:{s:4:\"mail\";a:4:{s:10:\"from_email\";s:20:\"richard@baum.digital\";s:9:\"from_name\";s:17:\"Wordpress Starter\";s:6:\"mailer\";s:4:\"mail\";s:11:\"return_path\";b:0;}s:4:\"smtp\";a:1:{s:7:\"autotls\";b:1;}}', 'no'),
(489, 'asp_updates_lc', '1565043328', 'yes');
INSERT INTO `baum_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(490, 'asp_updates', 'VERSION: 4988\r\nVERSION_STRING: 4.16.3\r\nREQUIRES: 3.5\r\nTESTED: 5.2\r\nDOWNLOADED: 10500\r\nLAST_UPDATED: 2019-07-30\r\n\r\n---SUPPORT===\r\n\r\n===KNOWLEDGE_BASE===\r\n[Updating from older versions](https://wp-dreams.com/knowledge-base/updating-from-older-versions/)\r\n[The images are not showing, what is wrong?](https://wp-dreams.com/knowledge-base/the-images-are-not-showing-what-is-wrong/)\r\n[Advanced Custom Fields Image field as image](https://wp-dreams.com/knowledge-base/advanced-custom-fields-image-field-as-image/)\r\n[Some images are broken, what to do?](https://wp-dreams.com/knowledge-base/some-images-are-broken-what-to-do/)\r\n[Style changes does not apply after saving](https://wp-dreams.com/knowledge-base/style-changes-does-not-apply-after-saving/)\r\n[The layout and theme is not changing after saving](https://wp-dreams.com/knowledge-base/the-layout-and-theme-is-not-changing-after-saving/)\r\n[Some elements look different (weird margins, paddings, width, etc..), what to do?](https://wp-dreams.com/knowledge-base/some-elements-look-different-weird-margins-paddings-width-etc-what-to-do/)\r\n[When I type in something, the search wheel is spinning, but nothing happens. Why?](https://wp-dreams.com/knowledge-base/when-i-type-in-something-the-search-wheel-is-spinning-but-nothing-happens-why/)\r\n[Tiny input box on certain page positions](https://wp-dreams.com/knowledge-base/tiny-input-box-on-certain-page-positions/)\r\n[No results for some keywords](https://wp-dreams.com/knowledge-base/no-results-for-some-keywords/)\r\n[Demo setup](https://wp-dreams.com/knowledge-base/demo-setup/)\r\n[How to remove ‘Variation #xyz of’ from product variation title](https://wp-dreams.com/knowledge-base/how-to-remove-variation-xyz-of-from-product-variation-title/)\r\n[How to change the results URL to something else?](https://wp-dreams.com/knowledge-base/how-to-change-the-results-url-to-something-else/)\r\n[Filtering WooCommerce hidden catalog products](https://wp-dreams.com/knowledge-base/filtering-woocommerce-hidden-catalog-products/)\r\n[Two column search setup](https://wp-dreams.com/knowledge-base/two-column-search-setup/)\r\n[How to add “Try these” keywords under the search as seen on the demo?](https://wp-dreams.com/knowledge-base/how-to-add-try-these-keywords-under-the-search-as-seen-on-the-demo/)\r\n[Showing the category titles in the result title](https://wp-dreams.com/knowledge-base/showing-the-category-titles-in-the-result-title/)\r\n[Showing the post type name in result title](https://wp-dreams.com/knowledge-base/showing-the-post-type-name-in-post-title/)\r\n[Numbering the results](https://wp-dreams.com/knowledge-base/numbering-the-results/)\r\n[Search logic explained](https://wp-dreams.com/knowledge-base/search-logic-explained/)\r\n[Result Templating](https://wp-dreams.com/knowledge-base/result-templating/)\r\n\r\n===UPDATE_NOTES===\r\n\r\n===UPDATE_NOTES_PER_VERSION===\r\n==4982==\r\n<p><b>Ajax Search Pro 4.15 notice</b>: Core Javascript files have been changed in this version, make sure to clear the cache, if you use any! (minify, page, CDN, browser)</p>\r\n<p>{hide_button}</p>\r\n==\r\n\r\n==4971==\r\n<p><b>Ajax Search Pro 4.13.1 notice</b>: Core Javascript files have been changed in this version, make sure to clear the cache, if you use any! (minify, page, CDN, browser)</p>\r\n<p>{hide_button}</p>\r\n==\r\n==4965==\r\n<p><b>Ajax Search Pro v4.11.9 notice</b>: Core Javascript files have been changed in this version, make sure to clear the cache, if you use any! (minify, page, CDN, browser)</p>\r\n<p>{hide_button}</p>\r\n==\r\n==4962==\r\n<p><b>Ajax Search Pro v4.11.6 notice</b>: Core Javascript files have been changed in this version, make sure to clear the cache, if you use any! (minify, page, CDN, browser)</p>\r\n<p>{hide_button}</p>\r\n==\r\n                          \r\n===CHANGELOG===\r\n==4.16.3==\r\n2019.07.30\r\nAdded\r\n - PODs custom table fields now can be indexed\r\n - Random results order for the simple results\r\nFixed\r\n - TCPDF - Update to latest release (6.2.26)\r\n - Polylang - String translations are now correctly registered\r\n - Search override - Now works with the correct fields, when triggered without the parameters\r\n - Removed nameless files (git/terminal/config files), that may triggered some security plugins for false positives\r\n - Remove the old \"alpha(opacity=100)\" CSS filters\r\n - Browser back button - The from-to date gets filled incorrectly, yielding no results\r\n - PeepSo search fixes\r\n==\r\n\r\n==4.16.2==\r\n2019.06.27\r\nFixed\r\n - Custom field filters - {get_values} shorthand and the empty \'select all\' option now working properly\r\n - Meta boxes - Now properly save on Attachment editor screens\r\n - Search override - GET and POST method now correctly work\r\n - WooCommerce & Polylang - Product variations are now shown in the correct language\r\n - Index Table - The CPT languages are correctly parsed\r\n - Datepicker UI - Time zone differences are now correctly calculated when selecting the date\r\n==\r\n\r\n==4.16.1==\r\n2019.06.24\r\nFixed\r\n - Reset button - Now correctly resets the slider items\r\n - Custom field filter - drop-down option group fixed\r\n - Custom field filter - labels fixed\r\n==\r\n\r\n==4.16==\r\n2019.06.20\r\nAdded\r\n - Filters API - adding, removing and modifying front-end filters via code is now possible\r\n - JS API - Functions to check if the filters have been changed, and if the filters state is initial\r\n - PHP API - filters_changed, filters_initial query arguments added, to check if the search arguments have been changed on the front end\r\nChanged\r\n - Reset button - now allows closing the results when clicked\r\n - WP 5.2 compatibility checked\r\nFixed\r\n - Reset button - correctly resets after redirection\r\n - Multisite - Fixed a performance related issue, when using the get_site_option and update_site_option functions\r\n - NoUI slider - Hidden dragger fixed due to incorrect transformation CSS property\r\n==\r\n\r\n==4.15.2==\r\n2019.04.30\r\nAdded\r\n- \"date_format\" - field parameter for advanced title and content fields, to specify a custom date format\r\n- From/To custom field date filters - added inclusive options as well, so that the selected date can be included\r\n- Datepicker fields - allowing empty value as default, as well as adjustable placeholder text\r\nFixed\r\n- Fixed an Elementor override bug - when a search phrase was not defined, the Elementor search archive was not working correctly\r\n- Themeco X Pro builder - shortcode contents are now correctly indexed\r\n- \"Chosen\" drop-down filter z-index fixes\r\n- Renaming search instances now works correctly\r\n==\r\n\r\n==4.15.1==\r\n2019.04.16\r\nAdded\r\n- Grouping can be set by title, instead of the default ID\r\nChanged\r\n- Taxonomy Term search - Added an option to control search in term titles\r\n- Advanced Title and Content fields - \'_price_html\' field with WooCommerce will display the price requested as HTML, including the sale and original price if available.\r\n- Product variation titles in results -> now using the get_the_title() method instead of the previous bypass. WooCommerce apparently corrected this.\r\nFixed\r\n- Custom field filters - brackets within names of custom fields are now accepted\r\n- Custom URL for user results now works correctly\r\n- Back-end file uploader - now attaches to the correct inputs\r\n- Corrected the input trigger methods, now the search properly cancels or continues where required\r\n- Reset button - now correctly resets checkbox filters, where all the checkboxes are unchecked by default\r\n- WooCommerce - Exclude hidden catalog and search product_variations now working correctly\r\n- Polylang - The plugin now returns taxonomy terms from the correct language\r\n==\r\n\r\n==4.15==\r\n2019.03.13\r\nAdded\r\n- Importing search from the Lite version\r\n- Results box width, margin and snap - floating results box can snap to the left, right or center of the search bar, as well as can have a custom width and margins.\r\n- Primary and Secondary ordering fields for user results (including custom fields)\r\nChanged\r\n- Percentage values for isotopic item height is now allowed (it\'s relative to the container width, not the container height)\r\n- Custom Field Drop-down filters - now support adding option groups within the drop-down via syntax: \'__option_group||This is a group\'\r\n- Custom Field Drop-down filters - now support multiple values within one option, using the \'::\' delimiter: \'value1::value2::value3||Option name\'\r\n- Synonyms: Added an option to allow using the synonyms as keywords as well. In case of keyword => (synonym1, synonym2 ..) the words synonym1 and synonym2 .. act as keywords and are synonyms to each other.\r\n- Search override: Visiting the search results page via URL will automatically trigger the override feature with the default filter values, for the selected search instance\r\n- Compact final width - now supports separate tablet and mobile device widths\r\n- Custom icons (magnifier, loading etc..) - color/selection options disabled when active\r\nFixed\r\n- Accessibility: Search option labels now contain the label text (invisible) to comply with Wave checking tool (previous release had a CSS implemented to prepare this change)\r\n- IOS/Touch devices - clicking on the \'More results\' button now does not close the results momentarily\r\n- CF/Taxonomy searchable drop-down filter - does no longer trigger the search when entering the search phrase\r\n- Keyword suggestions - Title/Tag/Tax suggestions now properly respect the passed options\r\n- Back-end: Changed the #preview selector to something more specific, to prevent conflicts with some other plugins\r\n- Attachment page URL forced correctly, when chosen\r\n- Vertical results: Hitting up/down keys, no longer prevent the screen scrolling, when the results scrollbar is not visible\r\n- Fixed an array declaration issue in the helpers file, that causes problems with some PHP versions/directives (from [] to array())\r\n- Custom uploaded loading icons now work properly\r\n- Custom field filters - empty custom field values are now properly checked\r\n==\r\n\r\n==4.14.6==\r\n2019.01.16\r\nFixed\r\n- Tablet item height for isotopic results is now correctly passed\r\n- Redirection to first results on enter key and magnifier events corrected\r\n- Duplication of search instances is now working again\r\n- Autocomplete & Suggestions - Post categories are now possible to use as source\r\n- User results - HTML properly stripped and removed, and description context enabled\r\n- Post Type Filters - correctly memorized when redirecting to the results page\r\n- Minor Typo fixes\r\n- Language files updated\r\n==\r\n\r\n==4.14.5==\r\n2019.01.08\r\nAdded\r\n- Separate Mobile, Tablet and Desktop search box width, and isotopic result width/height options\r\n- Results information box - it is now possible to display the phrase entered and the number of results found on top of the results list\r\n- get_asp_result_field($field) & the_asp_result_field($field) functions - Can be used in search.php theme file, within the loop. It returns the search result fields (such as the image, relevance etc..), that are exclusive to Ajax Search Pro.\r\nChanges\r\n- \'asp_init_search_bar\' event added, triggers when a search bar is initialized\r\n- [multisite] - Search instances are assigned ownership, and only the assigned admin or super admin can access them. (old search instances are assigned to \'anyone\' in the admins group, super admin can change them)\r\n- PODs - Tested for Custom PODs relation to Post type extensions - searchable via the Index Table\r\n- Back-end UI improvements #1 - some inputs have changed, so that they automatically resize based on the input size.\r\n- Back-end UI improvements #1 - updates to option descriptions & more intuitiveness improvements\r\nFixes\r\n- String translations for custom field filter values are now registered properly\r\n- Back-end: Options now correctly save when the server has a very low max_input_vars setting (<500) in effect, via a parse_str wrapper\r\n- Isotopic results - better & faster mobile orientation and browser resize event handling during displaying paginated results.\r\n- (ignored, not an issue) Post Format label is now available on the display mode list, under the taxonomy term filters panel\r\n- Search in \'private\' posts is now only visible for users above editor capabilities\r\n- Theme Loader - Now properly resets some custom stylings\r\n- Custom Field Filters - Multi-select options now have an adjustable logic, just like checkboxes\r\n- The \'No results\' and \'Did you mean?\' back-end options now accept HTML tags.\r\n==\r\n\r\n==4.14.4==\r\n2018.12.10\r\nAdded\r\n - Gutenberg editor modules & compatibility test & WordPress 5.0 compatibility tested\r\n - New theme group - Max UI (instead of curvy)\r\nChanged\r\n - Themes - Input background changes, on some themes, the input field background changed to transparent, to aviod confusion with the overall box background.\r\n - When tables are not created, the plugin now outputs the generated SQL and instructions to execute it in order to create the tables properly.\r\n - Advanced fields - Taxonomy syntax accepts more parameters: hide_empty, childless, hierarchical\r\n - Index Table - Now re-tries failed index queries, expecting a much fluid indexing process\r\nFixed\r\n - Custom field date filter - ACF date field choice no longer displays an information message (yymmdd), as it is incorrect. (it works correctly)\r\n - Advanced fields - ACF choice field labels are displayed, when the field return mode is set to \'Both\' (array)\r\n - Content type filter - \'comments\' content type added, moved from the Generic filters section\r\n - \"No results\" bug - container no longer displayed when loading \'more results\' (due to inaccurate or unset results count)\r\n - More results - no longer displayed, when clicking the close (x) icon, and re-entering the same phrase again\r\n - Date filter - the date picker now appears correctly on the back-end\r\n - More robust https detection, as in some cases incorrectly configured site URLs resulted in wrong protocol urls\r\n - Priority saving issue fix - affecting Multitisites only\r\n - User exclusion - now working with all/anonymous user options\r\n - Index table engine - Fixed a query format, when using the \'OR\' keyword logic\r\n - Custom field checkboxes filter - fixed and issue where the \'Exactly LIKE\' operator would not work correctly\r\n==\r\n\r\n==4.14.3==\r\n2018.11.09\r\nAdded\r\n- Filter by user meta support (including 3rd party, like UM support)\r\n- Advanced title and content fields supporting custom taxonomy terms\r\n- Option to Exclude taxonomy term results where the post count is 0 (empty terms).\r\n- WP 5.0 compatibility tested\r\nFixed\r\n- Isotopic layout - Swiping over non-paginated items does no longer close the results list.\r\n- Meta filters - Slider and Range slider now automatically sets the handle values, when left empty on the back-end.\r\n==\r\n\r\n==4.14.2==\r\n2018.10.27\r\nFixed\r\n - When the search box visibility is disabled - facet change triggering search is no longer forced, as there is a search button available.\r\n - Minor script literal fix, for IE11 compatibility\r\n - noUi slider script updated\r\n - Range slider:  mobile fixes - now correctly triggers on drag end events, drag size increased for better comfort\r\n==\r\n\r\n==4.14.1==\r\n2018.10.08\r\nFixed\r\n - ACF - get field error, on some rare configurations fixed\r\n - Post type options - no longer show some ACF related, template and form post types to avoid confusion\r\n - Better minification of the script files (~10%)\r\n==\r\n\r\n==4.14==\r\n2018.10.06\r\nAdded  \r\n - Select all checkboxes option for custom field filters\r\n - Infinite scroll mode for the \'Load more results\' feature\r\n - Option to explicitly disable isotopic layout navigation\r\n - More results loader icon added, when clicking on the more results link    \r\nChanged\r\n - Smoother scrolling with the scroll script\r\n - UI: Mobile keyboard now hides initially, but only when the results list first opens or is reopened.       \r\nFixed         \r\n - Load more results fix - on auto-populate enabled, the default results limit is used, to prevent loading of incorrect items.\r\n - Isotopic layout - smoother appending new items to the end of the isotope list, as well as smarter column calculation\r\n - Product - Tags (2x) in the taxonomy terms list - these are no longer visible, as they are not actual product tags\r\n - ACF - Get fields function now includes another bypass, to fix some problems with custom field filters\r\n - Lazy loader script moved out of original scope for better compatibility\r\n - Attachment search fixed, where in some cases the search keywords were not applied\r\n - Minor cosmetic bugfixes    \r\n== \r\n\r\n==4.13.4==\r\n2018.09.03\r\nAdded\r\n - Cache method is now possible to choose: file (default) or database\r\nFixed\r\n - Removed a commented code section, which caused WordFence to trigger a false positive match\r\n== \r\n\r\n==4.13.3==\r\n2018.08.30\r\nNew features\r\n - \"Double Quotation\" option as an exact matching feature for regular engine. When enabled, words in quotation marks are treated as a single term.\r\n - Localization of the back-end. The main .post file is under the /languages folder\r\nChanges\r\n - Regular engine rework: Better cross-field keyword, and the the default search logic changed to AND.\r\n - Some redundant or un-used classes removed: wpdreamsBorderradius, wpdreamsBoxShadowMini, wpdreamsCustomContent, wpdreamsCustomTaxonomyTerm, wpdreamsCustomTaxonomyTermSel, wpdreamsDatePicker, wpdreamsFont, wpdreamsFontMini, wpdreamsImageParser, wpdreamsImageSettings, wpdreamsInfo, wpdreamsLabelPosition, wpdreamsOnOff, wpdreamsPageParents, wpdreamsSelect, wpdreamsSelectCategory, wpdreamsSelectTerm, wpdreamsTextareaIsParam, wpdreamsTextShadow, wpdreamsTextShadowMini, wpdreamsUploadReset\r\n - Removed many unused back-end scripts ~1MB\r\n - API methods - using classes instead of ID selectors for better compatibility\r\nFixes\r\n - The database existence checker method is now case insensitive, as some databases may return a lower case DB via the \'SHOW Tables\' query.\r\n - Right mouse button on results in FireFox - it no longer closes the results list.\r\n - Prevented shortcode to be executed on the back-end taxonomy listing pages (not happening in WordPress by default, only if a custom plugin allows shortcode in listings)\r\n - Single quotes and other special characters fixed on keyword exceptions list\r\n - Inline script and CSS contents are better trimmed from the result contents\r\n - Updater: update checks are less frequent, if the remote servers are not responding\r\n - Index Table: Shortcode extraction improvements\r\n - Results container animation fixes\r\n - Google Fonts are now loading correctly as defined under each font setting\r\n - ACF select/radio etc.. field values are now loading correctly within the custom field filters in all cases, even if the plugin shortcode is not executed in post context\r\n==\r\n\r\n==4.13.2==\r\n2018.08.01\r\nNew features \r\n - Term meta table search option when searching for taxonomy terms\r\n - Advanced title & Content fields now support additional field variables: {__content}, {__title}, {__image}, {__link} - These variables return the corresponding result field\r\n - asp_noajax_results filter added - executed before passing the non-ajax results to the results page\r\nChanged   \r\n - Protocol relative enqueue_scripts & enqueue_styles replaced with the protocol version: Let the Cache/Minify solutions decide to remove/keep the protocol string or not.\r\n - Notification: to enable index table on search instances, when index table first generated\r\nFixed\r\n - The \'inherit\' font option now properly saves and loads\r\n - Taxonomy Term word length query issue fixed\r\n - Elementor plugin compatibility - Fixed an issue with Elementor customized search page, where the override was not working\r\n - Term exclusions now work properly, when a radio/select filter is in use for the same taxonomy\r\n - File attachment thumbnail images are now parsed correctly, if they exist (ex.: PDF images)          \r\n==\r\n\r\n==4.13.1==\r\n2018.06.20\r\nNew features\r\n - Search button as a \'filter\'\r\n - Peepso groups and activities search integration\r\nChanges\r\n - Option: Attachment results can point to the parent (attached) post URL\r\n - Advanced title and content fields recognize The Events Calendar and Events Manager custom fields, and displays the dates in correct date format.\r\n - Some styles/Options reworked on the back-end for easier, more convenient usage\r\n - Font options: entering custom fonts is now possible\r\n - Info message about possible cookies\r\n - \'Load more\' action \'ajax results\' now properly disabled when not available\r\n - Index table - custom fields below length of 50 characters are now indexed with and without space characters as well (ex. field \'001 002 003\' indexed as -> \'001\', \'002\', \'003\' and \'001002003\')\r\n - Index Table Cron option -> Now allows 2, 3, 5, 15, 30 minute cron job intervals\r\nFixed\r\n - WCAG compliance improvements - aria-label and aria-hidden attributes added where possible\r\n - Categories Images plugin support for parsing taxonomy term images\r\n - Default image for non post-type results is now showing (terms, users, attachments) correctly\r\n - Custom field {get_values} syntax now orders the fields alphabetically, in ascending order \r\n - Category Filter -> Display mode -> Placeholder text for multiselect & Search text no longer resets upon saving multiple times\r\n - Category Filter -> Display mode -> \"Select All\" text no longer resets to empty string\r\n - Media Attachment indexing - only one document is indexed per iteration for better performance\r\n - Media attachment indexing (including images) - Cron automatically enables (5 min. interval), when media attachment content indexing is enabled\r\n - Index table generator - detection of long requests, and automatically lowering the limits\r\n - Fixed a bug where empty queries resulted in an error message\r\n - Slider/Range slider width now following the column width\r\n - Fixed a Hidden Search form bug -> When No field selected under General Options -> Sources = No results\r\n - Better index table quote detection\r\n - Synonyms with apostrophes are stored correctly without backslash now\r\n - qTranslate fix for term descriptions\r\n - WPML Query fix for taxonomy term results\r\n - Fixed some search bar Themes\r\n - The term results count is not correct when using Primary + Secondary logic\r\n - Index table - single indexed titles now include an apostropheless version as well\r\n - Featured image source size now affects attachments as well\r\n==\r\n\r\n==4.13==\r\n2018.04.26\r\nNew features\r\n- Synonims for Index table engine\r\n- Indexing media attachment contents: Text, CSV, PDF, RTF, Word, Excel and Powerpoint document files content indexing is now possible\r\n- Live results loader feature for the results page\r\n- Lazy loader jQuery script for results images\r\n- New API method to get the URL to the current filter states: ASP.api(id, \'getStateURL\');\r\nChanged\r\n- Index table post type selector [TEST]\r\nFixed\r\n- Index table index bug - where some singe word post titles might not be indexed individually\r\n- Date filter bug (wrong date inclusion)\r\n- Range slider issue on Mobile devices\r\n- jQuery 3+ compatibility\r\n- Image cache is now working on attachment, tag etc.. results\r\n- Documentation links fix for the new documentation domain\r\n- Custom scrollbar - Mobile scroll difficulties fixed\r\n- More redundant clone detection and fixing method (usually in mobile menus)\r\n- \"Try these\" keywords with apostrophes - properly escaped\r\n- \"No results\" text and apostrophes - properly escaped\r\n==\r\n\r\n==4.12.2==\r\n2018.03.20\r\nNew features\r\n- Plugin scritps/styles can be loaded selectively on certain pages\r\n- Post type and Content type CSS classes to results output\r\n- Index table Pool sizes are now dynamically adjusted by the plugin, but can be adjusted manually as well\r\n- Minimum word length option - to increase perormance\r\nChanged\r\n- Range slider values can be automatically parsed from DB\r\n- On connection failure, the plugin will show a message.\r\nFixed\r\n- Custom field search alignment on Frontend Search settings fixed\r\n- Trigger live search on enter key is now working (if trigger on type is disabled)\r\n- Redirect results to the first on enter/click is now working\r\n- The \'Close\' icon now displays whenever there is a phrase entered and the input is focused\r\n- Major Index table performance issue fixes: the pool size was too big for short phrases | title query optimized and reworked\r\n- Minimum word length introduced for separate keywords - increasing the performance in some cases dramatically\r\n- the_title filter in index table class now applied with the correct number of arguments (2)\r\n==\r\n\r\n==4.12.1==\r\n2018.02.27\r\nFixed\r\n- A result width calculation fix on block layouts\r\n- Result box position calculation fix, when logged in, or the document body is moved\r\n- Result image dimensions calculation fix (integer parsing error)\r\n==\r\n\r\n==4.12==\r\n2018.02.23\r\nAdded\r\n- Priority Groups - Prioritizing results based on taxonomy terms and custom field rules is now possible.\r\n- Max-height attribute to vertical results theme options (default: auto)\r\n- Added \'Empty background\' to isotopic results if no image is found option\r\n- \"Choose any\" option for Post type filters\r\n- Hide completed items Delete…\r\nChanged\r\n- Isotope item width allowing pixels + rows calculation based on that\r\nFixed\r\n- \'keyup\' event handler changed to \'input\' due to better compatibility with IOS mobile devices\r\n- The WP default search query is now definitely cancelled, whenever the override is detected.\r\n- Autocomplete notices no longer appear\r\n- User search, advanced fields - supporting conditional brackets and ACF\r\n- Extended phpDocumentor comments within the code\r\n- Cache & \'load more\' results feature conflict fix\r\n- Term search content HTML stripping and closing added\r\n- Results, settings (cases when search bar is in a fixed header) and compact box scroll delay removed\r\n- Browser back-state and Compact layout conflict fix (no longer triggers auto search)\r\n- Index Table engine bugfix - The selected fields on the regular engine no longer affect the index table engine.\r\n- Index Table and the Generic Selectors (search in title, content and excerpt) now work together as expected\r\n- Backend - Generic Selector option fix - When removing an option, did not update correctly\r\n- Fixed container detection - The results and settings container no longer \'jumps\' when the search bar is placed in a fixed container.\r\n- Mobile fixes - Compact layout now properly fixates to the site, the scrolling is smoother as well\r\n==\r\n\r\n==4.11.10==\r\n2018.01.12\r\nAdded\r\n - Filtering by content type is added\r\n - Filter box header for generic selectors\r\nChanges\r\n - .label and .option classes removed\r\n - Replace old CSS selectors with shorthand classes in style.basic.css\r\n - Some option names changed for better understanding\r\nFixes\r\n - Post format \'standard\' is now present in the filter list.\r\n - WPML, Polylang, qTranslateX current language affects the google keyword suggestions language\r\n - Custom post type filters are no longer in effect when set to invisible\r\n - Search default override does not affect the back-end search anymore (issue in 4.11.9 only)\r\n - Back-end meta box was too wide\r\n - JS Retain popstate (browser back button trigger) option now works properly\r\n - A clipping issue with the chosen script\r\n - When live search is disabled, autocomple will no longer trigger it incorrectly\r\n==\r\n\r\n==4.11.9==\r\n2017.12.12\r\nAdded\r\n - Option to hide settings box on hovering layout, when results display (enabled)\r\n - Browser back-button retain phrase if entered + mobile tests\r\n - Exact matching location option: anywhere, starting with the phrase or ending with the phrase\r\nChanged\r\n - Settings and Results box data-id and data-instance set\r\n - The default style.basic.css file replaced with a minified version (saving ~10% space)\r\n - Minify CSS option added to the Compatibility settings panel\r\n - Better title match relevance calculation (exact matching > starts with matching > random matching)\r\n - \"Search in posts\" and \"Search in pages\" option merged to \"Search in custom post types\"\r\n - Include post by tags is now possible, using the include by taxonomy terms option\r\n - The default search override now applies when the search page is visited via URL: ../?s=phrase\r\n - Results page override is enabled by default\r\n - Frontend search settings disabled by default\r\nFixed\r\n - Checkboxes no longer trigger during scroll on touch devices\r\n - Custom field DatePicker now triggers the search as it supposed to\r\n - The keyword highlighter script now supports Unicode character sets\r\n - ACF5 & MySQL datetime fields are the same, changed to one. ACF4 changed to ACF Date picker, ACF5 to ACF DateTime picker\r\n - Asynch CSS loader - Flash of Unstyled Content elemination, faster & smoother loading Isotopic mobile\r\n - Fixed a mobile focus/scroll issue on multi-instance setups\r\n - Further RTL fixes\r\n - Node Copy detection (especially in menus) and fix in most cases\r\n - When there is a Tax Term inclusion the logic adopts towards more expected results\r\n - Use Index Table for attachments search\r\n - Instance Init overhead reduced - instances table existence check cache\r\n==\r\n\r\n==4.11.8==\r\n2017.11.23\r\nChanged\r\n - $id and $real_id passed with asp_print_search_query filter\r\n - option and .label class deprecated due to non-specific name (will be removed in 4.12). Replaced by asp_option_inner and .asp_option_label classes\r\n - Shorthand class names introduced for global, main box, results, settings and block settings boxes with names: asp_w, asp_m, asp_r, asp_s and asp_sb\r\n - ~10% CSS decrease due to shorthand classes\r\n - Attachments search - ordering by custom fields is in effect now\r\nFixed\r\n - Re-count the results after executing the result filters, so the pagination is correct on the results page\r\n - Date filter not triggering live search\r\n - WPML permalink incorrect, apply wpml_permalink filter\r\n - CSS fixes on labels within the form\r\n==\r\n\r\n==4.11.7==\r\n2017.11.11\r\nAdded\r\n - Search redirect to URL and Show more custom URL: {custom_field} for radio, drop-down and input fields can be passed as argument\r\n - Allowed filtering by custom fields for Media attachments (option, disabled by default)\r\nFixed\r\n - Isotopic layout no longer resets on scrolling the mobile screen\r\n - WPML and Multisite search issue fixed\r\n - Fixed an error where WPML and String translations throws an error on latest WP (WPML String translation plugin issue, please update it)\r\n - Back-end accessibility improvements\r\n - [no-keyword] strings removed from top and last search widgests\r\n - Theme Chooser: An import issue fixed; Minor optimizations\r\n - A duplicate ID removed from the isotopic navigation\r\n==\r\n\r\n==4.11.6==\r\n2017.10.20\r\nAdded\r\n- jQuery choosen implementation\r\n- {_tax_price} variable in advanced fields, displaying the product price with tax in WooCommerce\r\n- Warning message wrapper - Pop up Continue/Cancel that is triggered on certain events.\r\n- Back-end: Only options for the current result layout type will be displayed, instad of all\r\n- EQUAL OR LESS, EQUAL OR MORE operators now available for the custom field filter option\r\n- Better visibility (high contrast) mode on the back-end\r\n- Custom field filter {get_values} variable now can parse ACF multiselect field choices\r\nChanged\r\n- User meta search and select field\r\n- Input select and multiselect CSS fixes\r\n- Settings checkbox background simplifications\r\nFixed\r\n- mb_convert_encoding existence check on image content parser\r\n- create_chmod function will use the File System wrapper in all cases during the activation process\r\n- \'No results text\' apostrophe and escaping fixes\r\n- \'No results text\' - using [ for {phrase}] syntax.\r\n- z-index issues on back-end narrow screens\r\n- \'Try these\' keywords comma after last word\r\n- Empty string checks during Index Table tokenization process\r\n- Text custom field - on keyup trigger a change event, prevent return event as we\r\n==\r\n\r\n==4.11.5==\r\n2017.09.15\r\nAdded\r\n- New themes: Filled red, blue and grey\r\n- Close icon now can be styled\r\nChanged\r\n- The plugin meta box is now adjustable to be displayed on chosen post types only\r\n- Themes are now more organized\r\n- Auto populate can now order randomly\r\nFixed\r\n- The theme loader is now much faster, themes sizes are reduced\r\n- Saving the compatibility options will remain on the active panel\r\n- Excluding and including taxonomy terms on the advanced panel no longer displays the \'Use all\' option\r\n- WP_Filesystem() is no longer initalized upon loading, only on actions and requests\r\n- Custom Field filters now support multiple fields with the same keys\r\n==\r\n\r\n==4.11.4==\r\n2017.08.24\r\nFixed\r\n - Category and term exclusions fix when allowing empty filters is enabled (enabled by default)\r\n - Settings drop down no displays below the magnifier if the settings icon disabled\r\n==\r\n\r\n==4.11.3==\r\n2017.08.21\r\nAdded\r\n - Magnifier, return button, and show more link redirection can be made to a new browser tab\r\n - Custom field selectors now support getting the custom field values from the DB by using the {get_values} variable\r\nChanged\r\n - The minimum width of the search box is now 140px instead of the previous 240px\r\nFixed\r\n - A bug with touch enabled notebooks with mouse, where checkboxes were not working\r\n - Isotopic navigation and item redirection on IOS devices\r\n - ASP_URL constant replaced with ASP_URL_NP to support non-protocol loading of assets\r\n - Some additional problematic shortcodes excluded from index table parser\r\n - Tag and term suggestion count is now respected\r\n - :after and :before attributes from inputs in search and form\r\n==\r\n\r\n==4.11.2==\r\n2017.08.08\r\nAdded\r\n- New option to ignore checkbox filters, if nothing is selected\r\nChanged\r\n- Index table can now exectute shortcodes within post excertp field\r\nFixed\r\n- Missing price tag on variable products is now displayed correctly\r\n- Checkbox bugs on mobile devices\r\n- Built in bfi_thumb library no longer adds actions to wordpress\r\n==\r\n\r\n==4.11.1==\r\n2017.08.02\r\nChanged\r\n- Custom field filter - empty fields - now existing fields with empty string values are considired as \'empty\' as well\r\n- [F1-F12] button triggers disabled\r\n- Faster escaping method\r\n- Last successful search is now better regocnized on empty phrases\r\nFixed\r\n- IOS Safari: Compact box closing fix on document click\r\n- IOS Safari: Zoom prevention on input focus\r\n- Title, term and tag suggestions now respect exclusions\r\n- WPML and product variations - now not displaying from incorrect languages\r\n- WPML redirection to results page - fixed a bug where the query arguments were incorrect\r\n- Generic front-end selectors: If the the front-end options are disabled, their values are no longer in use\r\n- Context find fix - where with an empty phrase the search would return the full description in some cases\r\n==\r\n\r\n==4.11==\r\n2017.07.14\r\nAdded\r\n- Exception filter, so the plugin can be terminated at a certain point\r\n- A \'_keyword_count_limit\' argument, to limit the effective phrase count\r\n- Conditional advanced title and description fields, ex.: [prefix text {field} suffix] will only display if \'field\' custom field is not empty\r\n- Advanced title & description fields: WooCommerce \'_price\', \'_regular_price\', \'_sale_price\' fields are automatically recognized and displayed formatted with currency\r\n- ACF get_field() compatibility switch  to the Compatibility options submenu\r\n- A \'Disable all fonts\' switch to the Compatibility options submenu\r\n- Maintenance submenu: plugin resetting and wiping (full uninstall) features\r\nChanged\r\n- Generic front-end options (Exact matches, Search in title, etc..) can be better modified via a new option layout on the back-end\r\n- The redirection URL has a better structure\r\n- The \'show more\' results opions are simplified\r\n- The commas between \'try these\' phrases are now pseudo selectors\r\n- Isotope script update to 3.0.4 (scoped)\r\n- Search statistics are now only recorded after successful searches, as a separate process\r\nFixed\r\n- Checkboxes bugfix\r\n- Min width of the search set to 140px\r\n- Post status fixes for the index table\r\n- The SVG files are optimized\r\n- The redirection did not work correctly on 0 characters entered\r\n- IE 11 and Edge isotopic results click fix\r\n- Page exclusion option - non-published objects were invisible\r\n- WooCommerce notices and deprecated functions removed\r\n- Isotopic results pagination fix\r\n- RTL layout with isotopic script\r\n==\r\n\r\n==4.10.5==\r\n2017.06.03\r\nAdded\r\n- Empty groups can be displayed\r\n- Old browser compatibility can be disabled\r\nChanged\r\n- HTML Entity decoding on certain events\r\nFixed\r\n- Mobile browser issue, when suggested keywords wouldn\'t work\r\n- Preview mode fix for some rare cases\r\n- min-width attribute to prevent collapsing box in small containers\r\n==\r\n\r\n==4.10.4==\r\n2017.05.23\r\nAdded\r\n- Searching users by email address\r\n- A post parent ID parameter for the asp_query_args filter\r\n- Blueprints for the future feature: instant suggestions\r\nFixed\r\n- RTL layout should be much better now\r\n- A performance issue related to custom field filters\r\n- Isotope layout, middle mouse button click now reacts\r\n- ACF 5 date field filtering updated to mysql datetime\r\n- Issue with index table on default values when saving posts\r\n- An issue with primary and secondary logic execution\r\n- autocomplete missing source fatal error issue\r\n- Mobile redirections corrected\r\n- Auto populate limit fixed\r\n- Group location fixed\r\n- WPML attachment language affection\r\n- Context and content length is now correct for the terms search\r\nChanged\r\n- The default option for Return and Magnifier click is ajax search, not redirection\r\n- Space now does not trigger the search\r\n- Clicking on the input field will display the latest successful results list\r\n- If filter checkboxes are unchecked, the \'Select all\' option is unchecked as well\r\n- Input field z-index is defaulted to 0\r\n==\r\n\r\n==4.10.3==\r\n2017.03.06\r\nAdded\r\n- z-index adjustemt for settings and results box\r\n- z-index fix for close icon on low widths\r\n- asp_results_hide action in JS api\r\nFixed\r\n- Results redirection now memorizes taxonomy drop-down and radio values\r\n- Missing scrolling from custom field selectors\r\nChanged\r\n- Context highlighting now uses the whole term to look for matches\r\n- WPEngine false positive php7 warnings adjusted\r\n- removed unused files\r\n==\r\n\r\n==4.10.2==\r\n2017.02.20\r\nAdded:\r\n- Attachment search by ID\r\nFixed:\r\n- Theme loader fix\r\n==\r\n\r\n==4.10.1==\r\n2017.02.15\r\nFixed:\r\n- Results width caclulation bug fixed\r\n- More results not working fix\r\n- More results count on index table\r\n- Limit modifier variable introduced (as argument)\r\n- z-index related fixes\r\n==\r\n\r\n==4.10==\r\n2017.02.11\r\nAdded\r\n- Pagination can be displayed on both top and bottom of isotopic layout\r\n- Posts per page argument for search override\r\n- The plugin shortcode now works as a menu item (place into the menu title field)\r\n- WooCommerce built in ordering and filtering is now respected on search override\r\n- User search: excluding/including users is now possible\r\n- Visibility Detection option: when enabled the plugin tries to detect if it\'s visisble, if not, closes the results and settings\r\n- asp_it_args filter to allow hooking to index table args\r\n- The index table is now capable of indexing IFRAME contents\r\n- The content context length is now adjustable\r\n- New JS API functions: ASP.api(id, \'searchRedirect\', phrase) and ASP.api(id, \'exists\')\r\nChanged\r\n- Date selectors now support format change\r\n- Trigger and redirect options merged into a more simple one\r\n- Custom field type options now include a search box to select a custom field, instead of listing all\r\n- Term results now correctly display term content if present\r\n- Compact mode scroll propagation prevention for vertical and horizontal layouts\r\n- Optional WP_Session was fully removed, a better, cookie method is used\r\n- Higher priority for exact matching titles on Index table engine\r\n- The page ID is passed during search in the _page_id argument\r\n- Cache now deletes correctly when search options change\r\n- Some data moved to the footer for  better compatibility\r\n- WooCommerce product visibility is now respected\r\n- WooCommerce product variation content is now correctly parsed\r\n- Attachment results now respect custom field filters\r\n- Polaroid layout now more flexible in height\r\nFixed\r\n- A width calculation for hovering results, now properly calculates margin, border and padding as well\r\n- z-index values lowered to 1 on most elements\r\n- Content image parser now correctly gets the first found image instead of second\r\n- Multi-select filter scroll issue fix\r\n- Mobile double-tap issue fix\r\n- Mobile keyboard now properly hides when the virtual keyboard search icon is triggered\r\n- Top and Latest search phrases widget now respects redirection and override\r\n- RequireJS and NodeJS script loaders disabled, the plugin does not use them\r\n- Excerpt translation with qTranslatex\r\n- Checkbox visual bugs from adjacent selectors\r\n- max_input_vars php.ini variable detection and alternate saving method\r\n- Try these keywords visibility fix on static plugin layout\r\n- A trigger fix when only custom field search is enabled\r\n- BuddyPress activity: post processing fix\r\n- User search: role and custom field conflict fix\r\n- min-width attribute on input field for old Android browsers\r\n- Multisite YOAST Seo conflict when saving the SEO fields\r\n- The autocomplete=off attribute added to form to prevent webkit prefill bugs\r\n- Date to filter value fixed\r\n- Double override execution fix\r\n- Flash of Unstyled content fix for async CSS loader\r\n- Override URLs for Genesis Framework\r\n- Box shadow options, reversed vertical and horizontal labels fix\r\n- Instance creation and storage issue, when the initial options were stored incorrectly\r\n- Search Data Optimization: Automatic removal of un-used options, resulting in 50% less data stored\r\n==', 'yes'),
(492, 'widget_ajaxsearchprolastsearcheswidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(493, 'widget_ajaxsearchprotopsearcheswidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(494, 'acf_version', '5.8.2', 'yes'),
(554, 'widget_mc4wp_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(513, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3NjoiYjNKa1pYSmZhV1E5TVRFek5qZzVmSFI1Y0dVOVpHVjJaV3h2Y0dWeWZHUmhkR1U5TWpBeE55MHdPUzB3TVNBeU1UbzBPVG8wTnc9PSI7czozOiJ1cmwiO3M6MzQ6Imh0dHA6Ly9sb2NhbGhvc3Qvd29yZHByZXNzLXN0YXJ0ZXIiO30=', 'yes'),
(514, '_asp_update_usage', 'a:1:{i:0;i:1525900310;}', 'yes'),
(515, 'asp_update_data', 'a:2:{s:3:\"key\";s:36:\"2c75583d-7008-458b-8114-933495eb67d6\";s:4:\"host\";s:9:\"localhost\";}', 'yes'),
(556, 'mc4wp_version', '4.2.1', 'yes'),
(1267, 'wp-optimize-schedule', 'false', 'yes'),
(1268, 'wp-optimize-last-optimized', 'Never', 'yes'),
(559, 'mc4wp_flash_messages', 'a:0:{}', 'no'),
(140, 'theme_mods_twentyseventeen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1525100188;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(141, 'current_theme', 'Divi Child', 'yes'),
(142, 'theme_mods_Divi', 'a:6:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:16:\"et_pb_css_synced\";s:3:\"yes\";s:18:\"nav_menu_locations\";a:3:{s:12:\"primary-menu\";i:2;s:14:\"secondary-menu\";i:0;s:11:\"footer-menu\";i:0;}s:39:\"et_updated_layouts_built_for_post_types\";s:3:\"yes\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1565031081;s:4:\"data\";a:13:{s:19:\"wp_inactive_widgets\";a:0:{}s:19:\"pbe-above-header-wa\";a:1:{i:0;s:13:\"custom_html-2\";}s:19:\"pbe-below-header-wa\";a:0:{}s:13:\"pbe-footer-wa\";a:0:{}s:20:\"pbe-above-content-wa\";a:0:{}s:20:\"pbe-below-content-wa\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:1:{i:0;s:13:\"custom_html-3\";}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:0:{}s:9:\"sidebar-6\";a:0:{}s:9:\"sidebar-7\";a:0:{}}}}', 'yes'),
(143, 'theme_switched', '', 'yes'),
(144, 'et_pb_cache_notice', 'a:1:{s:6:\"3.26.6\";s:6:\"ignore\";}', 'yes'),
(149, 'et_core_version', '3.26.6', 'yes');
INSERT INTO `baum_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(151, 'et_divi', 'a:126:{s:39:\"static_css_custom_css_safety_check_done\";b:1;s:23:\"2_5_flush_rewrite_rules\";s:4:\"done\";s:25:\"3_0_flush_rewrite_rules_2\";s:4:\"done\";s:27:\"divi_skip_font_subset_force\";b:1;s:40:\"divi_email_provider_credentials_migrated\";b:1;s:15:\"divi_1_3_images\";s:7:\"checked\";s:21:\"et_pb_layouts_updated\";b:1;s:30:\"library_removed_legacy_layouts\";b:1;s:30:\"divi_2_4_documentation_message\";s:9:\"triggered\";s:9:\"divi_logo\";s:0:\"\";s:12:\"divi_favicon\";s:0:\"\";s:14:\"divi_fixed_nav\";s:2:\"on\";s:26:\"divi_gallery_layout_enable\";s:5:\"false\";s:18:\"divi_color_palette\";s:63:\"#000000|#ffffff|#e02b20|#e09900|#edf000|#7cda24|#0c71c3|#8300e9\";s:15:\"divi_grab_image\";s:5:\"false\";s:15:\"divi_blog_style\";s:5:\"false\";s:12:\"divi_sidebar\";s:16:\"et_right_sidebar\";s:22:\"divi_shop_page_sidebar\";s:16:\"et_right_sidebar\";s:23:\"divi_show_facebook_icon\";s:2:\"on\";s:22:\"divi_show_twitter_icon\";s:2:\"on\";s:21:\"divi_show_google_icon\";s:2:\"on\";s:18:\"divi_show_rss_icon\";s:2:\"on\";s:17:\"divi_facebook_url\";s:1:\"#\";s:16:\"divi_twitter_url\";s:1:\"#\";s:15:\"divi_google_url\";s:1:\"#\";s:12:\"divi_rss_url\";s:0:\"\";s:34:\"divi_woocommerce_archive_num_posts\";i:9;s:17:\"divi_catnum_posts\";i:6;s:21:\"divi_archivenum_posts\";i:5;s:20:\"divi_searchnum_posts\";i:5;s:17:\"divi_tagnum_posts\";i:5;s:16:\"divi_date_format\";s:6:\"M j, Y\";s:16:\"divi_use_excerpt\";s:5:\"false\";s:26:\"divi_responsive_shortcodes\";s:2:\"on\";s:33:\"divi_gf_enable_all_character_sets\";s:5:\"false\";s:16:\"divi_back_to_top\";s:5:\"false\";s:18:\"divi_smooth_scroll\";s:5:\"false\";s:25:\"divi_disable_translations\";s:5:\"false\";s:27:\"divi_minify_combine_scripts\";s:2:\"on\";s:26:\"divi_minify_combine_styles\";s:2:\"on\";s:15:\"divi_custom_css\";s:0:\"\";s:21:\"divi_enable_dropdowns\";s:2:\"on\";s:14:\"divi_home_link\";s:2:\"on\";s:15:\"divi_sort_pages\";s:10:\"post_title\";s:15:\"divi_order_page\";s:3:\"asc\";s:22:\"divi_tiers_shown_pages\";i:3;s:32:\"divi_enable_dropdowns_categories\";s:2:\"on\";s:21:\"divi_categories_empty\";s:2:\"on\";s:27:\"divi_tiers_shown_categories\";i:3;s:13:\"divi_sort_cat\";s:4:\"name\";s:14:\"divi_order_cat\";s:3:\"asc\";s:20:\"divi_disable_toptier\";s:5:\"false\";s:25:\"divi_scroll_to_anchor_fix\";s:5:\"false\";s:21:\"et_pb_static_css_file\";s:2:\"on\";s:19:\"et_pb_css_in_footer\";s:2:\"on\";s:25:\"et_pb_product_tour_global\";s:3:\"off\";s:14:\"divi_postinfo2\";a:4:{i:0;s:6:\"author\";i:1;s:4:\"date\";i:2;s:10:\"categories\";i:3;s:8:\"comments\";}s:22:\"divi_show_postcomments\";s:2:\"on\";s:15:\"divi_thumbnails\";s:2:\"on\";s:20:\"divi_page_thumbnails\";s:5:\"false\";s:23:\"divi_show_pagescomments\";s:5:\"false\";s:14:\"divi_postinfo1\";a:3:{i:0;s:6:\"author\";i:1;s:4:\"date\";i:2;s:10:\"categories\";}s:21:\"divi_thumbnails_index\";s:2:\"on\";s:19:\"divi_seo_home_title\";s:5:\"false\";s:25:\"divi_seo_home_description\";s:5:\"false\";s:22:\"divi_seo_home_keywords\";s:5:\"false\";s:23:\"divi_seo_home_canonical\";s:5:\"false\";s:23:\"divi_seo_home_titletext\";s:0:\"\";s:29:\"divi_seo_home_descriptiontext\";s:0:\"\";s:26:\"divi_seo_home_keywordstext\";s:0:\"\";s:18:\"divi_seo_home_type\";s:27:\"BlogName | Blog description\";s:22:\"divi_seo_home_separate\";s:3:\" | \";s:21:\"divi_seo_single_title\";s:5:\"false\";s:27:\"divi_seo_single_description\";s:5:\"false\";s:24:\"divi_seo_single_keywords\";s:5:\"false\";s:25:\"divi_seo_single_canonical\";s:5:\"false\";s:27:\"divi_seo_single_field_title\";s:9:\"seo_title\";s:33:\"divi_seo_single_field_description\";s:15:\"seo_description\";s:30:\"divi_seo_single_field_keywords\";s:12:\"seo_keywords\";s:20:\"divi_seo_single_type\";s:21:\"Post title | BlogName\";s:24:\"divi_seo_single_separate\";s:3:\" | \";s:24:\"divi_seo_index_canonical\";s:5:\"false\";s:26:\"divi_seo_index_description\";s:5:\"false\";s:19:\"divi_seo_index_type\";s:24:\"Category name | BlogName\";s:23:\"divi_seo_index_separate\";s:3:\" | \";s:28:\"divi_integrate_header_enable\";s:2:\"on\";s:26:\"divi_integrate_body_enable\";s:2:\"on\";s:31:\"divi_integrate_singletop_enable\";s:2:\"on\";s:34:\"divi_integrate_singlebottom_enable\";s:2:\"on\";s:21:\"divi_integration_head\";s:0:\"\";s:21:\"divi_integration_body\";s:0:\"\";s:27:\"divi_integration_single_top\";s:0:\"\";s:30:\"divi_integration_single_bottom\";s:0:\"\";s:15:\"divi_468_enable\";s:5:\"false\";s:14:\"divi_468_image\";s:0:\"\";s:12:\"divi_468_url\";s:0:\"\";s:16:\"divi_468_adsense\";s:0:\"\";s:31:\"divi_previous_installed_version\";s:6:\"3.26.5\";s:29:\"divi_latest_installed_version\";s:6:\"3.26.6\";s:16:\"show_search_icon\";b:0;s:24:\"footer_widget_text_color\";s:7:\"#ffffff\";s:24:\"footer_widget_link_color\";s:7:\"#ffffff\";s:30:\"3_0_flush_rewrite_rules_3.19.3\";s:4:\"done\";s:30:\"3_0_flush_rewrite_rules_3.19.5\";s:4:\"done\";s:30:\"3_0_flush_rewrite_rules_3.19.9\";s:4:\"done\";s:30:\"et_flush_rewrite_rules_library\";s:6:\"3.26.6\";s:27:\"et_pb_post_type_integration\";a:3:{s:4:\"post\";s:2:\"on\";s:4:\"page\";s:2:\"on\";s:7:\"project\";s:2:\"on\";}s:24:\"et_enable_classic_editor\";s:2:\"on\";s:19:\"product_tour_status\";a:1:{i:1;s:3:\"off\";}s:26:\"divi_bfb_optin_modal_shown\";s:3:\"yes\";s:23:\"builder_custom_defaults\";O:8:\"stdClass\":0:{}s:33:\"customizer_settings_migrated_flag\";b:1;s:34:\"builder_custom_defaults_unmigrated\";b:0;s:24:\"divi_show_instagram_icon\";s:5:\"false\";s:18:\"divi_instagram_url\";s:1:\"#\";s:13:\"content_width\";i:1160;s:17:\"use_sidebar_width\";b:1;s:13:\"sidebar_width\";i:30;s:10:\"link_color\";s:7:\"#43ac7c\";s:12:\"accent_color\";s:7:\"#43ac7c\";s:16:\"menu_link_active\";s:7:\"#3b9968\";s:21:\"bottom_bar_text_color\";s:7:\"#666666\";s:21:\"custom_footer_credits\";s:71:\"<strong>Baum</strong> Copyright © 2019. Todos los derechos reservados.\";s:24:\"show_footer_social_icons\";b:1;s:16:\"all_buttons_icon\";s:2:\"no\";s:27:\"et_pb_clear_templates_cache\";b:1;}', 'yes'),
(152, 'widget_aboutmewidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(153, 'widget_adsensewidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(154, 'widget_advwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(155, 'shop_catalog_image_size', 'a:3:{s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";s:4:\"crop\";i:1;}', 'yes'),
(156, 'shop_single_image_size', 'a:3:{s:5:\"width\";s:3:\"510\";s:6:\"height\";s:4:\"9999\";s:4:\"crop\";i:0;}', 'yes'),
(157, 'shop_thumbnail_image_size', 'a:3:{s:5:\"width\";s:3:\"157\";s:6:\"height\";s:3:\"157\";s:4:\"crop\";i:1;}', 'yes'),
(158, 'et_images_temp_folder', 'C:\\wamp64\\www\\wordpress-starter/wp-content/uploads/et_temp', 'yes'),
(159, 'et_schedule_clean_images_last_time', '1565017336', 'yes'),
(160, 'et_pb_builder_options', 'a:2:{i:0;b:0;s:35:\"email_provider_credentials_migrated\";b:1;}', 'yes'),
(2393, 'category_children', 'a:0:{}', 'yes'),
(303, 'w3tc_nr_application_id', '{\"d41d8cd98f00b204e9800998ecf8427e\":0}', 'yes'),
(316, 'polylang_wpml_strings', 'a:0:{}', 'yes'),
(161, '_site_transient_et_update_all_plugins', 'O:8:\"stdClass\":1:{s:12:\"last_checked\";i:1565026538;}', 'no'),
(189, 'w3tc_state', '{\"common.install\":1525101249,\"common.show_note.flush_statics_needed\":false,\"common.show_note.flush_statics_needed.timestamp\":1525898951,\"common.show_note.flush_posts_needed\":false,\"common.show_note.flush_posts_needed.timestamp\":1525898951,\"common.show_note.plugins_updated\":false,\"common.show_note.plugins_updated.timestamp\":1525898951,\"minify.show_note.need_flush\":false,\"minify.show_note.need_flush.timestamp\":1525101368,\"objectcache.show_note.flush_needed\":false,\"objectcache.show_note.flush_needed.timestamp\":1525101368,\"common.next_support_us_invitation\":1544033422,\"common.support_us_invitations\":2,\"license.status\":\"no_key\",\"license.next_check\":1565454179,\"license.terms\":\"\"}', 'no'),
(166, '_site_transient_et_update_themes', 'O:8:\"stdClass\":1:{s:12:\"last_checked\";i:1565030704;}', 'no'),
(176, 'autoptimize_version', '2.5.1', 'yes'),
(164, 'recently_activated', 'a:6:{s:37:\"plugin-organizer/plugin-organizer.php\";i:1565024160;s:21:\"polylang/polylang.php\";i:1565024160;s:28:\"wp-disable/wpperformance.php\";i:1565024160;s:43:\"change-table-prefix/change-table-prefix.php\";i:1565024140;s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";i:1565023795;s:33:\"classic-editor/classic-editor.php\";i:1565023756;}', 'yes'),
(217, 'autoptimize_html', 'on', 'yes'),
(218, 'autoptimize_html_keepcomments', '', 'yes'),
(219, 'autoptimize_js', 'on', 'yes'),
(220, 'autoptimize_js_exclude', 'seal.js, js/jquery/jquery.js', 'yes'),
(221, 'autoptimize_js_trycatch', '', 'yes'),
(222, 'autoptimize_js_justhead', '', 'yes'),
(223, 'autoptimize_js_forcehead', '', 'yes'),
(224, 'autoptimize_js_include_inline', '', 'yes'),
(225, 'autoptimize_css', '', 'yes'),
(226, 'autoptimize_css_exclude', 'wp-content/cache/, wp-content/uploads/, admin-bar.min.css, dashicons.min.css', 'yes'),
(227, 'autoptimize_css_justhead', '', 'yes'),
(228, 'autoptimize_css_datauris', '', 'yes'),
(229, 'autoptimize_css_defer', '', 'yes'),
(230, 'autoptimize_css_defer_inline', '', 'yes'),
(231, 'autoptimize_css_inline', 'on', 'yes'),
(232, 'autoptimize_css_include_inline', '', 'yes'),
(233, 'autoptimize_cdn_url', '', 'yes'),
(234, 'autoptimize_cache_clean', '0', 'yes'),
(235, 'autoptimize_cache_nogzip', 'on', 'yes'),
(236, 'autoptimize_show_adv', '1', 'yes'),
(237, 'autoptimize_optimize_logged', 'on', 'yes'),
(238, 'autoptimize_optimize_checkout', '', 'yes'),
(241, 'autoptimize_extra_settings', 'a:5:{s:34:\"autoptimize_extra_checkbox_field_1\";s:1:\"1\";s:34:\"autoptimize_extra_checkbox_field_0\";s:1:\"1\";s:31:\"autoptimize_extra_radio_field_4\";s:1:\"1\";s:30:\"autoptimize_extra_text_field_2\";s:0:\"\";s:30:\"autoptimize_extra_text_field_3\";s:0:\"\";}', 'yes'),
(253, 'jedi_apprentice_settings', 'a:33:{s:19:\"jedi_master_version\";s:3:\"1.1\";s:13:\"include_posts\";s:1:\"1\";s:13:\"include_media\";s:1:\"1\";s:15:\"include_options\";s:1:\"1\";s:11:\"include_css\";i:0;s:13:\"include_menus\";s:1:\"1\";s:15:\"include_widgets\";s:1:\"1\";s:16:\"include_homepage\";s:1:\"1\";s:17:\"include_posttypes\";a:10:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:7:\"project\";i:3;s:7:\"product\";i:4;s:17:\"product_variation\";i:5;s:10:\"shop_order\";i:6;s:17:\"shop_order_refund\";i:7;s:11:\"shop_coupon\";i:8;s:12:\"shop_webhook\";i:9;s:12:\"et_pb_layout\";}s:18:\"include_categories\";s:1:\"1\";s:16:\"include_featured\";s:1:\"1\";s:11:\"homepage_ID\";s:2:\"41\";s:15:\"installer_style\";s:1:\"1\";s:14:\"installer_name\";s:7:\"josefin\";s:14:\"installer_slug\";s:7:\"josefin\";s:17:\"installer_message\";s:89:\"Select your options to import the demo content. It is always recommended to backup first!\";s:15:\"support_message\";s:364:\"<p><strong>Documentation</strong><br>Visit the <a href=\\\"http://josefin.madebysuperfly.com/extras/\\\" target=\\\"_blank\\\">Josefin extras page</a> for documentation and video tutorials!</p><p>Check out the <a href=\\\"https://besuperfly.zendesk.com/hc/en-us/sections/115002176108-Josefin\\\" target=\\\"_blank\\\">Josefin knowledgebase</a> for answers to common questions.</p>\";s:18:\"recommended_plugin\";s:11:\"Plugin Name\";s:22:\"recommended_plugin_url\";s:3:\"url\";s:14:\"enable_logging\";i:0;s:20:\"custom_documentation\";a:8:{s:5:\"intro\";a:2:{s:7:\"heading\";s:12:\"Introduction\";s:4:\"text\";s:193:\"<p>Imports work best on a fresh installation of WordPress with the Divi theme installed. It is always recommended that you verify that you have a good backup prior to importing new content.</p>\";}s:13:\"include_posts\";a:2:{s:7:\"heading\";s:21:\"Include Posts & Pages\";s:4:\"text\";s:220:\"<p>This option enables the importing of posts and pages as well as custom post types such as Projects and the layouts from the Divi Library. Current posts, pages and custom post types will not be modified or removed.</p>\";}s:13:\"include_media\";a:2:{s:7:\"heading\";s:21:\"Include Media Library\";s:4:\"text\";s:125:\"<p>Check this to import the demo images into your WordPress Media Library. Current items will not be modified or removed.</p>\";}s:15:\"include_options\";a:2:{s:7:\"heading\";s:53:\"Include Divi Theme Options & Divi Customizer Settings\";s:4:\"text\";s:106:\"<p>Notice: This option will overwrite all the current Divi Theme Options and Divi Customizer Settings.</p>\";}s:11:\"include_css\";a:2:{s:7:\"heading\";s:33:\"Include Customizer Additional CSS\";s:4:\"text\";s:156:\"<p>This option will add any extra CSS from the demo content to the Customizer\\\'s Additional CSS. This is typically used when a child theme is not setup.</p>\";}s:13:\"include_menus\";a:2:{s:7:\"heading\";s:22:\"Include Menu Structure\";s:4:\"text\";s:135:\"<p>Check this to recreate the menu structure from the demo content. Your current menus may be modified depending on their settings.</p>\";}s:15:\"include_widgets\";a:2:{s:7:\"heading\";s:23:\"Include Widget Settings\";s:4:\"text\";s:63:\"<p>Notice: This will overwrite all current widget settings.</p>\";}s:16:\"include_homepage\";a:2:{s:7:\"heading\";s:26:\"Automatically Set Homepage\";s:4:\"text\";s:84:\"<p>This option will update the WordPress setting to display a static front page.</p>\";}}s:18:\"selected_posttypes\";a:11:{s:4:\"post\";s:1:\"1\";s:4:\"page\";s:1:\"1\";s:7:\"project\";s:1:\"1\";s:7:\"product\";s:1:\"1\";s:17:\"product_variation\";s:1:\"1\";s:10:\"shop_order\";s:1:\"1\";s:17:\"shop_order_refund\";s:1:\"1\";s:11:\"shop_coupon\";s:1:\"1\";s:12:\"shop_webhook\";s:1:\"1\";s:12:\"et_pb_layout\";s:1:\"1\";s:12:\"oembed_cache\";i:0;}s:15:\"alternate_media\";i:0;s:12:\"beta_updates\";s:1:\"1\";s:16:\"selected_plugins\";a:4:{s:14:\"rotatingtweets\";s:14:\"rotatingtweets\";s:11:\"woocommerce\";s:11:\"woocommerce\";s:14:\"worth-the-read\";s:14:\"worth-the-read\";s:19:\"wp-instagram-widget\";s:19:\"wp-instagram-widget\";}s:18:\"include_thumbnails\";s:1:\"1\";s:18:\"prevent_thumbnails\";i:0;s:10:\"extensions\";a:2:{s:18:\"jedi_pro_extension\";a:5:{s:14:\"extension_name\";s:12:\"J.E.D.I. Pro\";s:17:\"extension_version\";s:3:\"1.1\";s:14:\"extension_path\";s:64:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/\";s:21:\"extension_plugin_file\";s:79:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/jedi-master.php\";s:22:\"extension_license_slug\";s:20:\"jedi_edd_license_key\";}s:24:\"jedi_anyplugin_extension\";a:5:{s:14:\"extension_name\";s:38:\"J.E.D.I. Extension | Export Any Plugin\";s:17:\"extension_version\";s:3:\"1.0\";s:14:\"extension_path\";s:67:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-anyplugin/\";s:21:\"extension_plugin_file\";s:85:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-anyplugin/jedi-anyplugin.php\";s:22:\"extension_license_slug\";s:26:\"jedi_anyplugin_license_key\";}}s:24:\"selected_nonrepo_plugins\";a:1:{s:31:\"nonrepo_page-builder-everywhere\";s:23:\"page-builder-everywhere\";}s:17:\"jedi_master_paths\";a:1:{s:18:\"export_data_folder\";s:98:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/josefin/jedi-apprentice/demo-data/\";}s:19:\"exclude_posts_by_id\";s:34:\"202551,203488,203467,203468,203469\";s:29:\"jedi_exported_nonrepo_plugins\";a:1:{s:23:\"page-builder-everywhere\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:23:\"Page Builder Everywhere\";s:9:\"PluginURI\";s:80:\"https://divi.space/product/page-builder-everywhere-header-footer-sidebar-editor/\";s:7:\"Version\";s:3:\"2.0\";s:11:\"Description\";s:137:\"Use the Divi Page Builder to create custom headers, footers and sidebars. <cite>By <a href=\"https://divi.space\">Stephen James</a>.</cite>\";s:6:\"Author\";s:46:\"<a href=\"https://divi.space\">Stephen James</a>\";s:9:\"AuthorURI\";s:18:\"https://divi.space\";s:10:\"TextDomain\";s:23:\"page-builder-everywhere\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:118:\"<a href=\"https://divi.space/product/page-builder-everywhere-header-footer-sidebar-editor/\">Page Builder Everywhere</a>\";s:10:\"AuthorName\";s:13:\"Stephen James\";}}s:19:\"jedi_import_sysinfo\";a:12:{s:6:\"php_os\";s:5:\"WINNT\";s:11:\"php_version\";s:6:\"5.6.31\";s:15:\"allow_url_fopen\";s:1:\"1\";s:18:\"max_execution_time\";s:3:\"120\";s:16:\"max_file_uploads\";s:2:\"20\";s:12:\"memory_limit\";s:4:\"128M\";s:10:\"wp_version\";s:5:\"4.9.5\";s:6:\"wp_url\";s:34:\"http://localhost/wordpress-starter\";s:13:\"wp_theme_name\";s:4:\"Divi\";s:16:\"wp_theme_version\";s:3:\"3.2\";s:18:\"wp_childtheme_name\";s:7:\"Josefin\";s:21:\"wp_childtheme_version\";s:3:\"4.1\";}}', 'yes'),
(254, 'jedi_import_options', 'a:33:{s:19:\"jedi_master_version\";s:3:\"1.1\";s:13:\"include_posts\";s:1:\"1\";s:13:\"include_media\";s:1:\"1\";s:15:\"include_options\";s:1:\"1\";s:11:\"include_css\";i:0;s:13:\"include_menus\";s:1:\"1\";s:15:\"include_widgets\";s:1:\"1\";s:16:\"include_homepage\";s:1:\"1\";s:17:\"include_posttypes\";a:10:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:7:\"project\";i:3;s:7:\"product\";i:4;s:17:\"product_variation\";i:5;s:10:\"shop_order\";i:6;s:17:\"shop_order_refund\";i:7;s:11:\"shop_coupon\";i:8;s:12:\"shop_webhook\";i:9;s:12:\"et_pb_layout\";}s:18:\"include_categories\";s:1:\"1\";s:16:\"include_featured\";s:1:\"1\";s:11:\"homepage_ID\";s:2:\"41\";s:15:\"installer_style\";s:1:\"1\";s:14:\"installer_name\";s:7:\"josefin\";s:14:\"installer_slug\";s:7:\"josefin\";s:17:\"installer_message\";s:89:\"Select your options to import the demo content. It is always recommended to backup first!\";s:15:\"support_message\";s:364:\"<p><strong>Documentation</strong><br>Visit the <a href=\\\"http://josefin.madebysuperfly.com/extras/\\\" target=\\\"_blank\\\">Josefin extras page</a> for documentation and video tutorials!</p><p>Check out the <a href=\\\"https://besuperfly.zendesk.com/hc/en-us/sections/115002176108-Josefin\\\" target=\\\"_blank\\\">Josefin knowledgebase</a> for answers to common questions.</p>\";s:18:\"recommended_plugin\";s:11:\"Plugin Name\";s:22:\"recommended_plugin_url\";s:3:\"url\";s:14:\"enable_logging\";i:0;s:20:\"custom_documentation\";a:8:{s:5:\"intro\";a:2:{s:7:\"heading\";s:12:\"Introduction\";s:4:\"text\";s:193:\"<p>Imports work best on a fresh installation of WordPress with the Divi theme installed. It is always recommended that you verify that you have a good backup prior to importing new content.</p>\";}s:13:\"include_posts\";a:2:{s:7:\"heading\";s:21:\"Include Posts & Pages\";s:4:\"text\";s:220:\"<p>This option enables the importing of posts and pages as well as custom post types such as Projects and the layouts from the Divi Library. Current posts, pages and custom post types will not be modified or removed.</p>\";}s:13:\"include_media\";a:2:{s:7:\"heading\";s:21:\"Include Media Library\";s:4:\"text\";s:125:\"<p>Check this to import the demo images into your WordPress Media Library. Current items will not be modified or removed.</p>\";}s:15:\"include_options\";a:2:{s:7:\"heading\";s:53:\"Include Divi Theme Options & Divi Customizer Settings\";s:4:\"text\";s:106:\"<p>Notice: This option will overwrite all the current Divi Theme Options and Divi Customizer Settings.</p>\";}s:11:\"include_css\";a:2:{s:7:\"heading\";s:33:\"Include Customizer Additional CSS\";s:4:\"text\";s:156:\"<p>This option will add any extra CSS from the demo content to the Customizer\\\'s Additional CSS. This is typically used when a child theme is not setup.</p>\";}s:13:\"include_menus\";a:2:{s:7:\"heading\";s:22:\"Include Menu Structure\";s:4:\"text\";s:135:\"<p>Check this to recreate the menu structure from the demo content. Your current menus may be modified depending on their settings.</p>\";}s:15:\"include_widgets\";a:2:{s:7:\"heading\";s:23:\"Include Widget Settings\";s:4:\"text\";s:63:\"<p>Notice: This will overwrite all current widget settings.</p>\";}s:16:\"include_homepage\";a:2:{s:7:\"heading\";s:26:\"Automatically Set Homepage\";s:4:\"text\";s:84:\"<p>This option will update the WordPress setting to display a static front page.</p>\";}}s:18:\"selected_posttypes\";a:11:{s:4:\"post\";s:1:\"1\";s:4:\"page\";s:1:\"1\";s:7:\"project\";s:1:\"1\";s:7:\"product\";s:1:\"1\";s:17:\"product_variation\";s:1:\"1\";s:10:\"shop_order\";s:1:\"1\";s:17:\"shop_order_refund\";s:1:\"1\";s:11:\"shop_coupon\";s:1:\"1\";s:12:\"shop_webhook\";s:1:\"1\";s:12:\"et_pb_layout\";s:1:\"1\";s:12:\"oembed_cache\";i:0;}s:15:\"alternate_media\";i:0;s:12:\"beta_updates\";s:1:\"1\";s:16:\"selected_plugins\";a:4:{s:14:\"rotatingtweets\";s:14:\"rotatingtweets\";s:11:\"woocommerce\";s:11:\"woocommerce\";s:14:\"worth-the-read\";s:14:\"worth-the-read\";s:19:\"wp-instagram-widget\";s:19:\"wp-instagram-widget\";}s:18:\"include_thumbnails\";s:1:\"1\";s:18:\"prevent_thumbnails\";i:0;s:10:\"extensions\";a:2:{s:18:\"jedi_pro_extension\";a:5:{s:14:\"extension_name\";s:12:\"J.E.D.I. Pro\";s:17:\"extension_version\";s:3:\"1.1\";s:14:\"extension_path\";s:64:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/\";s:21:\"extension_plugin_file\";s:79:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/jedi-master.php\";s:22:\"extension_license_slug\";s:20:\"jedi_edd_license_key\";}s:24:\"jedi_anyplugin_extension\";a:5:{s:14:\"extension_name\";s:38:\"J.E.D.I. Extension | Export Any Plugin\";s:17:\"extension_version\";s:3:\"1.0\";s:14:\"extension_path\";s:67:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-anyplugin/\";s:21:\"extension_plugin_file\";s:85:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-anyplugin/jedi-anyplugin.php\";s:22:\"extension_license_slug\";s:26:\"jedi_anyplugin_license_key\";}}s:24:\"selected_nonrepo_plugins\";a:1:{s:31:\"nonrepo_page-builder-everywhere\";s:23:\"page-builder-everywhere\";}s:17:\"jedi_master_paths\";a:1:{s:18:\"export_data_folder\";s:98:\"/nas/content/live/madebysuperfly/wp-content/plugins/jedi-master/josefin/jedi-apprentice/demo-data/\";}s:19:\"exclude_posts_by_id\";s:34:\"202551,203488,203467,203468,203469\";s:29:\"jedi_exported_nonrepo_plugins\";a:1:{s:23:\"page-builder-everywhere\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:23:\"Page Builder Everywhere\";s:9:\"PluginURI\";s:80:\"https://divi.space/product/page-builder-everywhere-header-footer-sidebar-editor/\";s:7:\"Version\";s:3:\"2.0\";s:11:\"Description\";s:137:\"Use the Divi Page Builder to create custom headers, footers and sidebars. <cite>By <a href=\"https://divi.space\">Stephen James</a>.</cite>\";s:6:\"Author\";s:46:\"<a href=\"https://divi.space\">Stephen James</a>\";s:9:\"AuthorURI\";s:18:\"https://divi.space\";s:10:\"TextDomain\";s:23:\"page-builder-everywhere\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:118:\"<a href=\"https://divi.space/product/page-builder-everywhere-header-footer-sidebar-editor/\">Page Builder Everywhere</a>\";s:10:\"AuthorName\";s:13:\"Stephen James\";}}s:19:\"jedi_import_sysinfo\";a:12:{s:6:\"php_os\";s:5:\"WINNT\";s:11:\"php_version\";s:6:\"5.6.31\";s:15:\"allow_url_fopen\";s:1:\"1\";s:18:\"max_execution_time\";s:3:\"120\";s:16:\"max_file_uploads\";s:2:\"20\";s:12:\"memory_limit\";s:4:\"128M\";s:10:\"wp_version\";s:5:\"4.9.5\";s:6:\"wp_url\";s:34:\"http://localhost/wordpress-starter\";s:13:\"wp_theme_name\";s:4:\"Divi\";s:16:\"wp_theme_version\";s:3:\"3.2\";s:18:\"wp_childtheme_name\";s:7:\"Josefin\";s:21:\"wp_childtheme_version\";s:3:\"4.1\";}}', 'yes'),
(255, 'jedi_apprentice_path', 'C:\\wamp64\\www\\wordpress-starter\\wp-content\\themes\\josefin\\jedi-apprentice/', 'yes'),
(256, 'theme_mods_josefin', 'a:2:{s:16:\"et_pb_css_synced\";s:3:\"yes\";s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(262, 'ewww_image_optimizer_background_optimization', '1', 'yes'),
(980, 'ewww_image_optimizer_bulk_attachments', '', 'no'),
(981, 'ewww_image_optimizer_flag_attachments', '', 'no'),
(982, 'ewww_image_optimizer_ngg_attachments', '', 'no'),
(266, 'ewww_image_optimizer_disable_pngout', '1', 'no'),
(267, 'ewww_image_optimizer_optipng_level', '2', 'no'),
(268, 'ewww_image_optimizer_pngout_level', '2', 'no'),
(269, 'ewww_image_optimizer_jpegtran_copy', '1', 'no'),
(270, 'ewww_image_optimizer_jpg_level', '10', 'no'),
(271, 'ewww_image_optimizer_png_level', '10', 'no'),
(272, 'ewww_image_optimizer_gif_level', '10', 'no'),
(273, 'ewww_image_optimizer_pdf_level', '0', 'no'),
(274, 'exactdn_all_the_things', '1', 'no'),
(275, 'ewww_image_optimizer_version', '432.0', 'yes'),
(276, 'ewww_image_optimizer_tracking_notice', '1', 'yes'),
(528, 'asp_recently_updated', '1', 'yes'),
(531, 'PO_ignore_protocol', '1', 'yes'),
(530, '_asp_tables', 'a:1:{s:4:\"main\";i:1;}', 'yes'),
(532, 'PO_ignore_arguments', '1', 'yes'),
(533, 'PO_order_access_net_admin', '1', 'yes'),
(534, 'PO_auto_trailing_slash', '1', 'yes'),
(535, 'PO_disable_plugins_frontend', '2', 'yes'),
(536, 'PO_disable_plugins_mobile', '0', 'yes'),
(537, 'PO_disable_plugins_admin', '0', 'yes'),
(315, 'polylang', 'a:15:{s:7:\"browser\";b:0;s:7:\"rewrite\";i:1;s:12:\"hide_default\";i:1;s:10:\"force_lang\";i:1;s:13:\"redirect_lang\";i:0;s:13:\"media_support\";b:0;s:9:\"uninstall\";i:0;s:4:\"sync\";a:0:{}s:10:\"post_types\";a:0:{}s:10:\"taxonomies\";a:0:{}s:7:\"domains\";a:0:{}s:7:\"version\";s:5:\"2.6.2\";s:12:\"default_lang\";s:2:\"es\";s:9:\"nav_menus\";a:2:{s:4:\"Divi\";a:3:{s:12:\"primary-menu\";a:2:{s:2:\"es\";i:2;s:2:\"en\";i:0;}s:14:\"secondary-menu\";a:2:{s:2:\"es\";i:0;s:2:\"en\";i:0;}s:11:\"footer-menu\";a:2:{s:2:\"es\";i:0;s:2:\"en\";i:0;}}s:10:\"Divi-child\";a:3:{s:12:\"primary-menu\";a:2:{s:2:\"es\";i:2;s:2:\"en\";i:0;}s:14:\"secondary-menu\";a:2:{s:2:\"es\";i:0;s:2:\"en\";i:0;}s:11:\"footer-menu\";a:2:{s:2:\"es\";i:0;s:2:\"en\";i:0;}}}s:16:\"previous_version\";s:5:\"2.5.4\";}', 'yes'),
(290, 'instantclick', 'a:4:{s:11:\"before_init\";s:0:\"\";s:10:\"after_init\";s:0:\"\";s:10:\"preload_on\";s:5:\"hover\";s:18:\"no_instant_scripts\";a:1:{i:0;s:0:\"\";}}', 'yes'),
(298, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(322, 'widget_polylang', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(388, 'et_account_status', 'active', 'no'),
(617, 'et_google_api_settings', 'a:3:{s:7:\"api_key\";s:0:\"\";s:26:\"enqueue_google_maps_script\";s:2:\"on\";s:16:\"use_google_fonts\";s:2:\"on\";}', 'yes'),
(618, 'et_automatic_updates_options', 'a:2:{s:8:\"username\";s:11:\"baumdigital\";s:7:\"api_key\";s:40:\"142ab98a638f77316d25dcc9041c26f9a126636e\";}', 'no'),
(619, 'et_account_status_last_checked', '1565022427', 'no'),
(438, 'w3tc_browsercache_flush_timestamp', '73308', 'yes'),
(491, 'widget_ajaxsearchprowidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2221, '_site_transient_timeout_PO_delete_missing_posts', '1565622135', 'no'),
(2222, '_site_transient_PO_delete_missing_posts', '1', 'no'),
(911, 'rml_db_version', '3.4.2', 'yes'),
(962, 'widget_divi_pb_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2198, 'recovery_keys', 'a:0:{}', 'yes'),
(2037, 'wp-optimize-dismiss_dash_notice_until', '1585762688', 'yes'),
(1265, 'wpperformance_rev3a_combined_google_fonts_requests_number', '0', 'yes'),
(923, 'ure_tasks_queue', 'a:0:{}', 'yes'),
(2297, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1565034298', 'no'),
(915, 'matthiasweb_advert_next_ts', '1542046542', 'no'),
(1231, 'autoptimize_imgopt_launched', 'on', 'yes'),
(1556, 'pand-d83e5fe4823f8f867dc30d3bb0b6c7de', '1559313336', 'no'),
(1764, 'matthiasweb_advert_dialogs_off', '', 'yes'),
(1765, 'whl_page', 'login', 'yes'),
(1766, 'whl_redirect_admin', '404', 'yes'),
(922, 'wp_backup_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:87:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"edit_plugin_filter\";b:1;s:19:\"edit_plugin_filters\";b:1;s:27:\"edit_private_plugin_filters\";b:1;s:20:\"delete_plugin_filter\";b:1;s:21:\"delete_plugin_filters\";b:1;s:26:\"edit_others_plugin_filters\";b:1;s:19:\"read_plugin_filters\";b:1;s:27:\"read_private_plugin_filters\";b:1;s:22:\"publish_plugin_filters\";b:1;s:28:\"delete_others_plugin_filters\";b:1;s:31:\"delete_published_plugin_filters\";b:1;s:29:\"delete_private_plugin_filters\";b:1;s:17:\"edit_filter_group\";b:1;s:20:\"manage_filter_groups\";b:1;s:17:\"edit_plugin_group\";b:1;s:18:\"edit_plugin_groups\";b:1;s:26:\"edit_private_plugin_groups\";b:1;s:19:\"delete_plugin_group\";b:1;s:20:\"delete_plugin_groups\";b:1;s:25:\"edit_others_plugin_groups\";b:1;s:18:\"read_plugin_groups\";b:1;s:26:\"read_private_plugin_groups\";b:1;s:21:\"publish_plugin_groups\";b:1;s:27:\"delete_others_plugin_groups\";b:1;s:30:\"delete_published_plugin_groups\";b:1;s:28:\"delete_private_plugin_groups\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'no'),
(979, 'matthiasweb_advert_rtg-regenerate', '1', 'no'),
(767, 'wrm_b6922947dd4141382fff', '1531426915', 'yes'),
(850, 'dnh_dismissed_notices', 'a:2:{i:0;s:24:\"wrm_b6922947dd4141382fff\";i:1;s:24:\"wrm_1e278f4992d8bb3f1f0b\";}', 'yes'),
(1304, 'show_comments_cookies_opt_in', '', 'yes'),
(1233, 'asp_glob', 'a:1:{s:20:\"additional_tag_posts\";a:0:{}}', 'no'),
(1269, 'wp-optimize-schedule-type', 'wpo_weekly', 'yes'),
(1270, 'wp-optimize-retention-enabled', 'false', 'yes'),
(1271, 'wp-optimize-retention-period', '2', 'yes'),
(1272, 'wp-optimize-enable-admin-menu', 'false', 'yes'),
(1273, 'wp-optimize-total-cleaned', '0', 'yes'),
(1274, 'wp-optimize-auto', 'a:7:{s:6:\"drafts\";s:4:\"true\";s:8:\"optimize\";s:5:\"false\";s:9:\"revisions\";s:4:\"true\";s:5:\"spams\";s:4:\"true\";s:9:\"transient\";s:5:\"false\";s:5:\"trash\";s:4:\"true\";s:10:\"unapproved\";s:5:\"false\";}', 'yes'),
(1275, 'wp-optimize-settings', 'a:13:{s:11:\"user-drafts\";s:4:\"true\";s:16:\"user-commentmeta\";s:5:\"false\";s:13:\"user-optimize\";s:4:\"true\";s:15:\"user-orphandata\";s:5:\"false\";s:14:\"user-pingbacks\";s:5:\"false\";s:13:\"user-postmeta\";s:5:\"false\";s:14:\"user-revisions\";s:4:\"true\";s:10:\"user-spams\";s:4:\"true\";s:15:\"user-trackbacks\";s:5:\"false\";s:14:\"user-transient\";s:4:\"true\";s:10:\"user-trash\";s:4:\"true\";s:15:\"user-unapproved\";s:4:\"true\";s:13:\"last_saved_in\";s:6:\"3.0.10\";}', 'yes'),
(1312, 'wp-optimize-enable-auto-backup', 'false', 'yes'),
(1281, 'wp-optimize-corrupted-tables-count', '0', 'yes'),
(1285, 'matthiasweb_advert_rcl-order', '1', 'no'),
(1301, 'wpperformance_rev3a_settings', 'a:57:{s:17:\"disable_gravatars\";i:0;s:31:\"disable_gravatars_only_comments\";i:0;s:21:\"disable_referral_spam\";i:1;s:21:\"remove_jquery_migrate\";i:0;s:12:\"dns_prefetch\";i:0;s:22:\"dns_prefetch_host_list\";s:0:\"\";s:13:\"disable_emoji\";i:1;s:14:\"disable_embeds\";i:0;s:19:\"remove_querystrings\";i:0;s:22:\"lazy_load_google_fonts\";i:0;s:22:\"lazy_load_font_awesome\";i:0;s:20:\"remove_yoast_comment\";i:0;s:35:\"remove_yoast_breadcrumbs_duplicates\";i:1;s:19:\"default_ping_status\";i:0;s:20:\"disable_all_comments\";i:1;s:20:\"disable_author_pages\";i:1;s:38:\"disable_comments_on_certain_post_types\";i:0;s:30:\"disable_comments_on_post_types\";a:0:{}s:14:\"close_comments\";i:0;s:17:\"paginate_comments\";i:0;s:21:\"remove_comments_links\";i:0;s:19:\"heartbeat_frequency\";s:2:\"60\";s:18:\"heartbeat_location\";s:7:\"default\";s:10:\"remove_rsd\";i:0;s:26:\"remove_windows_live_writer\";i:0;s:30:\"remove_wordpress_generator_tag\";i:0;s:20:\"remove_shortlink_tag\";i:0;s:32:\"remove_wordpress_api_from_header\";i:0;s:11:\"disable_rss\";i:0;s:24:\"not_disable_global_feeds\";i:0;s:23:\"disabled_feed_behaviour\";s:8:\"redirect\";s:14:\"disable_xmlrpc\";i:1;s:21:\"spam_comments_cleaner\";i:0;s:20:\"delete_spam_comments\";s:5:\"daily\";s:16:\"disable_autosave\";i:1;s:21:\"disable_admin_notices\";i:0;s:17:\"disable_revisions\";s:7:\"default\";s:29:\"disable_woocommerce_non_pages\";i:0;s:34:\"disable_woocommerce_cart_fragments\";i:0;s:27:\"disable_woocommerce_reviews\";i:0;s:34:\"disable_woocommerce_password_meter\";i:0;s:32:\"disable_wordpress_password_meter\";i:1;s:45:\"disable_front_dashicons_when_disabled_toolbar\";i:0;s:19:\"disable_google_maps\";i:0;s:32:\"exclude_from_disable_google_maps\";s:0:\"\";s:19:\"asynch_urls_to_load\";s:0:\"\";s:30:\"enable_external_scripts_asynch\";s:0:\"\";s:28:\"enable_external_fonts_asynch\";s:0:\"\";s:25:\"fonts_asynch_urls_to_load\";s:0:\"\";s:14:\"ds_tracking_id\";N;s:23:\"ds_adjusted_bounce_rate\";i:0;s:16:\"ds_enqueue_order\";i:0;s:15:\"ds_anonymize_ip\";N;s:18:\"ds_script_position\";N;s:29:\"caos_disable_display_features\";N;s:14:\"ds_track_admin\";N;s:19:\"caos_remove_wp_cron\";N;}', 'yes'),
(1225, 'autoptimize_service_availablity', 'a:2:{s:12:\"extra_imgopt\";a:3:{s:6:\"status\";s:2:\"up\";s:5:\"hosts\";a:1:{i:1;s:26:\"https://cdn.shortpixel.ai/\";}s:16:\"launch-threshold\";s:4:\"4096\";}s:7:\"critcss\";a:2:{s:6:\"status\";s:2:\"up\";s:5:\"hosts\";a:1:{i:1;s:24:\"https://criticalcss.com/\";}}}', 'yes'),
(1480, 'wp_page_for_privacy_policy', '0', 'yes'),
(1266, 'wpperformance_rev3a_combined_font_awesome_requests_number', '0', 'yes'),
(1117, 'wp-smush-skip-redirect', '1', 'no'),
(1112, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:23:\"wp-smushit/wp-smush.php\";i:1536949111;}s:5:\"queue\";a:0:{}s:4:\"done\";a:2:{i:0;a:6:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1536949111;s:5:\"state\";s:6:\"ignore\";s:4:\"hash\";s:32:\"fc50097023d0d34c5a66f6cddcf77694\";s:10:\"handled_at\";i:1537220987;}i:1;a:6:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1537553911;s:5:\"state\";s:6:\"ignore\";s:4:\"hash\";s:32:\"fc50097023d0d34c5a66f6cddcf77694\";s:10:\"handled_at\";i:1543520801;}}}', 'no'),
(1120, 'wp-smush-install-type', 'existing', 'no'),
(1121, 'wp-smush-version', '3.2.2.1', 'no'),
(1122, 'smush_global_stats', 'a:9:{s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:7:\"percent\";d:0;s:5:\"human\";s:5:\"0,0 B\";s:5:\"bytes\";i:0;s:12:\"total_images\";i:0;s:12:\"resize_count\";i:0;s:14:\"resize_savings\";i:0;s:18:\"conversion_savings\";i:0;}', 'no'),
(1127, 'dir_smush_stats', 'a:2:{s:9:\"dir_smush\";a:2:{s:5:\"total\";s:1:\"0\";s:9:\"optimised\";i:0;}s:14:\"combined_stats\";a:0:{}}', 'no'),
(1128, 'wp-smush-resize_sizes', 'a:2:{s:5:\"width\";i:2048;s:6:\"height\";i:2048;}', 'no'),
(1129, 'skip-smush-setup', '1', 'no'),
(1694, 'can_compress_scripts', '1', 'no'),
(1397, 'wp-smush-settings', 'a:15:{s:11:\"networkwide\";b:0;s:4:\"auto\";i:1;s:5:\"lossy\";b:0;s:10:\"strip_exif\";i:1;s:6:\"resize\";i:0;s:9:\"detection\";b:0;s:8:\"original\";b:0;s:6:\"backup\";b:0;s:10:\"png_to_jpg\";b:0;s:7:\"nextgen\";b:0;s:2:\"s3\";b:0;s:9:\"gutenberg\";i:0;s:3:\"cdn\";b:0;s:11:\"auto_resize\";b:0;s:4:\"webp\";b:1;}', 'no'),
(1747, 'et_bfb_settings', 'a:1:{s:10:\"enable_bfb\";s:3:\"off\";}', 'yes'),
(1854, 'et_support_site_id', '45*i1WU3nku68OQ0oSNF', 'yes'),
(1855, 'et_safe_mode_plugins_whitelist', 'a:6:{i:0;s:27:\"ari-adminer/ari-adminer.php\";i:1;s:15:\"etdev/etdev.php\";i:2;s:29:\"divi-builder/divi-builder.php\";i:3;s:31:\"query-monitor/query-monitor.php\";i:4;s:27:\"woocommerce/woocommerce.php\";i:5;s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";}', 'yes'),
(1856, 'et_support_center_installed', 'true', 'yes');
INSERT INTO `baum_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2365, 'rewrite_rules', 'a:165:{s:10:\"project/?$\";s:27:\"index.php?post_type=project\";s:40:\"project/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=project&feed=$matches[1]\";s:35:\"project/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=project&feed=$matches[1]\";s:27:\"project/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=project&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:56:\"layout_category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?layout_category=$matches[1]&feed=$matches[2]\";s:51:\"layout_category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?layout_category=$matches[1]&feed=$matches[2]\";s:32:\"layout_category/([^/]+)/embed/?$\";s:48:\"index.php?layout_category=$matches[1]&embed=true\";s:44:\"layout_category/([^/]+)/page/?([0-9]{1,})/?$\";s:55:\"index.php?layout_category=$matches[1]&paged=$matches[2]\";s:26:\"layout_category/([^/]+)/?$\";s:37:\"index.php?layout_category=$matches[1]\";s:52:\"layout_pack/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?layout_pack=$matches[1]&feed=$matches[2]\";s:47:\"layout_pack/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?layout_pack=$matches[1]&feed=$matches[2]\";s:28:\"layout_pack/([^/]+)/embed/?$\";s:44:\"index.php?layout_pack=$matches[1]&embed=true\";s:40:\"layout_pack/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?layout_pack=$matches[1]&paged=$matches[2]\";s:22:\"layout_pack/([^/]+)/?$\";s:33:\"index.php?layout_pack=$matches[1]\";s:52:\"layout_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?layout_type=$matches[1]&feed=$matches[2]\";s:47:\"layout_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?layout_type=$matches[1]&feed=$matches[2]\";s:28:\"layout_type/([^/]+)/embed/?$\";s:44:\"index.php?layout_type=$matches[1]&embed=true\";s:40:\"layout_type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?layout_type=$matches[1]&paged=$matches[2]\";s:22:\"layout_type/([^/]+)/?$\";s:33:\"index.php?layout_type=$matches[1]\";s:46:\"scope/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?scope=$matches[1]&feed=$matches[2]\";s:41:\"scope/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?scope=$matches[1]&feed=$matches[2]\";s:22:\"scope/([^/]+)/embed/?$\";s:38:\"index.php?scope=$matches[1]&embed=true\";s:34:\"scope/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?scope=$matches[1]&paged=$matches[2]\";s:16:\"scope/([^/]+)/?$\";s:27:\"index.php?scope=$matches[1]\";s:53:\"module_width/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?module_width=$matches[1]&feed=$matches[2]\";s:48:\"module_width/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?module_width=$matches[1]&feed=$matches[2]\";s:29:\"module_width/([^/]+)/embed/?$\";s:45:\"index.php?module_width=$matches[1]&embed=true\";s:41:\"module_width/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?module_width=$matches[1]&paged=$matches[2]\";s:23:\"module_width/([^/]+)/?$\";s:34:\"index.php?module_width=$matches[1]\";s:40:\"et_pb_layout/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"et_pb_layout/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"et_pb_layout/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"et_pb_layout/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"et_pb_layout/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"et_pb_layout/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"et_pb_layout/([^/]+)/embed/?$\";s:60:\"index.php?post_type=et_pb_layout&name=$matches[1]&embed=true\";s:33:\"et_pb_layout/([^/]+)/trackback/?$\";s:54:\"index.php?post_type=et_pb_layout&name=$matches[1]&tb=1\";s:41:\"et_pb_layout/([^/]+)/page/?([0-9]{1,})/?$\";s:67:\"index.php?post_type=et_pb_layout&name=$matches[1]&paged=$matches[2]\";s:48:\"et_pb_layout/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?post_type=et_pb_layout&name=$matches[1]&cpage=$matches[2]\";s:37:\"et_pb_layout/([^/]+)(?:/([0-9]+))?/?$\";s:66:\"index.php?post_type=et_pb_layout&name=$matches[1]&page=$matches[2]\";s:29:\"et_pb_layout/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"et_pb_layout/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"et_pb_layout/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"et_pb_layout/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"et_pb_layout/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"et_pb_layout/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"project/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"project/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"project/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"project/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"project/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"project/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"project/([^/]+)/embed/?$\";s:40:\"index.php?project=$matches[1]&embed=true\";s:28:\"project/([^/]+)/trackback/?$\";s:34:\"index.php?project=$matches[1]&tb=1\";s:48:\"project/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:43:\"project/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:36:\"project/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&paged=$matches[2]\";s:43:\"project/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&cpage=$matches[2]\";s:32:\"project/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?project=$matches[1]&page=$matches[2]\";s:24:\"project/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"project/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"project/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"project/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"project/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"project/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:57:\"project_category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?project_category=$matches[1]&feed=$matches[2]\";s:52:\"project_category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?project_category=$matches[1]&feed=$matches[2]\";s:33:\"project_category/([^/]+)/embed/?$\";s:49:\"index.php?project_category=$matches[1]&embed=true\";s:45:\"project_category/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?project_category=$matches[1]&paged=$matches[2]\";s:27:\"project_category/([^/]+)/?$\";s:38:\"index.php?project_category=$matches[1]\";s:52:\"project_tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?project_tag=$matches[1]&feed=$matches[2]\";s:47:\"project_tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?project_tag=$matches[1]&feed=$matches[2]\";s:28:\"project_tag/([^/]+)/embed/?$\";s:44:\"index.php?project_tag=$matches[1]&embed=true\";s:40:\"project_tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?project_tag=$matches[1]&paged=$matches[2]\";s:22:\"project_tag/([^/]+)/?$\";s:33:\"index.php?project_tag=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=4&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(2286, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"es_CR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1565026474;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(2384, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1565026536;s:7:\"checked\";a:17:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.2\";s:35:\"ajax-search-pro/ajax-search-pro.php\";s:6:\"4.16.3\";s:27:\"autoptimize/autoptimize.php\";s:5:\"2.5.1\";s:47:\"better-search-replace/better-search-replace.php\";s:5:\"1.3.3\";s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";s:5:\"2.0.6\";s:31:\"page-builder-everywhere/pbe.php\";s:3:\"2.0\";s:37:\"plugin-organizer/plugin-organizer.php\";s:4:\"10.1\";s:21:\"polylang/polylang.php\";s:5:\"2.6.2\";s:23:\"wp-smushit/wp-smush.php\";s:7:\"3.2.2.1\";s:25:\"sucuri-scanner/sucuri.php\";s:6:\"1.8.21\";s:33:\"w3-total-cache/w3-total-cache.php\";s:7:\"0.9.7.5\";s:41:\"wordpress-importer/wordpress-importer.php\";s:5:\"0.6.4\";s:27:\"wp-optimize/wp-optimize.php\";s:6:\"3.0.10\";s:28:\"wp-disable/wpperformance.php\";s:6:\"1.5.21\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"1.5.2\";s:41:\"real-media-library/real-media-library.php\";s:5:\"3.4.2\";s:33:\"wps-hide-login/wps-hide-login.php\";s:5:\"1.5.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:13:{s:27:\"autoptimize/autoptimize.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/autoptimize\";s:4:\"slug\";s:11:\"autoptimize\";s:6:\"plugin\";s:27:\"autoptimize/autoptimize.php\";s:11:\"new_version\";s:5:\"2.5.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/autoptimize/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/autoptimize.2.5.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:64:\"https://ps.w.org/autoptimize/assets/icon-128x128.png?rev=1864142\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/autoptimize/assets/banner-772x250.jpg?rev=1315920\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"better-search-replace/better-search-replace.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/better-search-replace\";s:4:\"slug\";s:21:\"better-search-replace\";s:6:\"plugin\";s:47:\"better-search-replace/better-search-replace.php\";s:11:\"new_version\";s:5:\"1.3.3\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/better-search-replace/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/better-search-replace.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/better-search-replace/assets/icon-256x256.png?rev=1238934\";s:2:\"1x\";s:74:\"https://ps.w.org/better-search-replace/assets/icon-128x128.png?rev=1238934\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/better-search-replace/assets/banner-1544x500.png?rev=1238934\";s:2:\"1x\";s:76:\"https://ps.w.org/better-search-replace/assets/banner-772x250.png?rev=1238934\";}s:11:\"banners_rtl\";a:0:{}}s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/force-regenerate-thumbnails\";s:4:\"slug\";s:27:\"force-regenerate-thumbnails\";s:6:\"plugin\";s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";s:11:\"new_version\";s:5:\"2.0.6\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/force-regenerate-thumbnails/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/force-regenerate-thumbnails.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:71:\"https://s.w.org/plugins/geopattern-icon/force-regenerate-thumbnails.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:37:\"plugin-organizer/plugin-organizer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/plugin-organizer\";s:4:\"slug\";s:16:\"plugin-organizer\";s:6:\"plugin\";s:37:\"plugin-organizer/plugin-organizer.php\";s:11:\"new_version\";s:4:\"10.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/plugin-organizer/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/plugin-organizer.10.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/plugin-organizer/assets/icon-256x256.png?rev=1786554\";s:2:\"1x\";s:69:\"https://ps.w.org/plugin-organizer/assets/icon-128x128.png?rev=1786554\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/plugin-organizer/assets/banner-1544x500.jpg?rev=1789638\";s:2:\"1x\";s:71:\"https://ps.w.org/plugin-organizer/assets/banner-772x250.jpg?rev=1789638\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"polylang/polylang.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/polylang\";s:4:\"slug\";s:8:\"polylang\";s:6:\"plugin\";s:21:\"polylang/polylang.php\";s:11:\"new_version\";s:5:\"2.6.2\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/polylang/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/polylang.2.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/polylang/assets/icon-256x256.png?rev=1331499\";s:2:\"1x\";s:61:\"https://ps.w.org/polylang/assets/icon-128x128.png?rev=1331499\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/polylang/assets/banner-1544x500.png?rev=1405299\";s:2:\"1x\";s:63:\"https://ps.w.org/polylang/assets/banner-772x250.png?rev=1405299\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:7:\"3.2.2.1\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-smushit.3.2.2.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.jpg?rev=2132251\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.jpg?rev=2132250\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"sucuri-scanner/sucuri.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/sucuri-scanner\";s:4:\"slug\";s:14:\"sucuri-scanner\";s:6:\"plugin\";s:25:\"sucuri-scanner/sucuri.php\";s:11:\"new_version\";s:6:\"1.8.21\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/sucuri-scanner/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/sucuri-scanner.1.8.21.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/sucuri-scanner/assets/icon-256x256.png?rev=1235419\";s:2:\"1x\";s:67:\"https://ps.w.org/sucuri-scanner/assets/icon-128x128.png?rev=1235419\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/sucuri-scanner/assets/banner-772x250.png?rev=1235419\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"w3-total-cache/w3-total-cache.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/w3-total-cache\";s:4:\"slug\";s:14:\"w3-total-cache\";s:6:\"plugin\";s:33:\"w3-total-cache/w3-total-cache.php\";s:11:\"new_version\";s:7:\"0.9.7.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/w3-total-cache/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/w3-total-cache.0.9.7.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-256x256.png?rev=1041806\";s:2:\"1x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-128x128.png?rev=1041806\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/w3-total-cache/assets/banner-772x250.jpg?rev=1041806\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:5:\"0.6.4\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wordpress-importer/assets/icon-256x256.png?rev=1908375\";s:2:\"1x\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";s:3:\"svg\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"wp-optimize/wp-optimize.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/wp-optimize\";s:4:\"slug\";s:11:\"wp-optimize\";s:6:\"plugin\";s:27:\"wp-optimize/wp-optimize.php\";s:11:\"new_version\";s:6:\"3.0.10\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/wp-optimize/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-optimize.3.0.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/wp-optimize/assets/icon-256x256.png?rev=1552899\";s:2:\"1x\";s:64:\"https://ps.w.org/wp-optimize/assets/icon-128x128.png?rev=1552899\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-optimize/assets/banner-1544x500.png?rev=2125385\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-optimize/assets/banner-772x250.png?rev=2125385\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wp-disable/wpperformance.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-disable\";s:4:\"slug\";s:10:\"wp-disable\";s:6:\"plugin\";s:28:\"wp-disable/wpperformance.php\";s:11:\"new_version\";s:6:\"1.5.21\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-disable/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wp-disable.1.5.21.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/wp-disable/assets/icon-128x128.jpg?rev=1616150\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"1.5.2\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.1.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2120094\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2120094\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"wps-hide-login/wps-hide-login.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wps-hide-login\";s:4:\"slug\";s:14:\"wps-hide-login\";s:6:\"plugin\";s:33:\"wps-hide-login/wps-hide-login.php\";s:11:\"new_version\";s:5:\"1.5.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wps-hide-login/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wps-hide-login.1.5.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-256x256.png?rev=1820667\";s:2:\"1x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-128x128.png?rev=1820667\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wps-hide-login/assets/banner-1544x500.jpg?rev=1820667\";s:2:\"1x\";s:69:\"https://ps.w.org/wps-hide-login/assets/banner-772x250.jpg?rev=1820667\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(2288, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1565030703;s:7:\"checked\";a:3:{s:10:\"Divi-child\";s:14:\"3.2.1525354353\";s:4:\"Divi\";s:6:\"3.26.6\";s:7:\"josefin\";s:3:\"4.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(2298, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4602;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3567;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2635;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2503;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1929;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1746;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1740;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1470;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1444;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1443;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1434;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1378;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1334;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1295;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1148;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1129;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1101;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1070;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1027;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:943;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:855;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:845;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:842;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:807;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:748;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:738;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:732;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:722;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:716;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:702;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:700;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:686;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:680;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:664;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:664;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:654;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:634;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:625;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:624;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:620;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:596;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:593;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:573;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:572;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:560;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:558;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:550;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:544;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:535;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:527;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:527;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:520;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:516;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:513;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:509;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:503;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:496;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:478;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:477;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:477;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:474;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:473;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:458;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:453;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:446;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:431;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:431;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:422;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:416;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:416;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:413;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:410;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:402;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:400;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:394;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:393;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:386;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:384;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:384;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:378;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:370;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:369;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:365;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:362;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:357;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:356;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:356;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:351;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:350;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:347;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:341;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:332;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:325;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:324;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:321;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:320;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:319;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:315;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:311;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:310;}}', 'no'),
(2372, '_site_transient_timeout_popular_importers_105da04f1baa3dbe94bb00f5b1d00859', '1565199255', 'no'),
(2373, '_site_transient_popular_importers_105da04f1baa3dbe94bb00f5b1d00859', 'a:2:{s:9:\"importers\";a:8:{s:7:\"blogger\";a:4:{s:4:\"name\";s:7:\"Blogger\";s:11:\"description\";s:54:\"Import posts, comments, and users from a Blogger blog.\";s:11:\"plugin-slug\";s:16:\"blogger-importer\";s:11:\"importer-id\";s:7:\"blogger\";}s:9:\"wpcat2tag\";a:4:{s:4:\"name\";s:29:\"Categories and Tags Converter\";s:11:\"description\";s:71:\"Convert existing categories to tags or tags to categories, selectively.\";s:11:\"plugin-slug\";s:18:\"wpcat2tag-importer\";s:11:\"importer-id\";s:10:\"wp-cat2tag\";}s:11:\"livejournal\";a:4:{s:4:\"name\";s:11:\"LiveJournal\";s:11:\"description\";s:46:\"Import posts from LiveJournal using their API.\";s:11:\"plugin-slug\";s:20:\"livejournal-importer\";s:11:\"importer-id\";s:11:\"livejournal\";}s:11:\"movabletype\";a:4:{s:4:\"name\";s:24:\"Movable Type and TypePad\";s:11:\"description\";s:62:\"Import posts and comments from a Movable Type or TypePad blog.\";s:11:\"plugin-slug\";s:20:\"movabletype-importer\";s:11:\"importer-id\";s:2:\"mt\";}s:4:\"opml\";a:4:{s:4:\"name\";s:8:\"Blogroll\";s:11:\"description\";s:28:\"Import links in OPML format.\";s:11:\"plugin-slug\";s:13:\"opml-importer\";s:11:\"importer-id\";s:4:\"opml\";}s:3:\"rss\";a:4:{s:4:\"name\";s:3:\"RSS\";s:11:\"description\";s:30:\"Import posts from an RSS feed.\";s:11:\"plugin-slug\";s:12:\"rss-importer\";s:11:\"importer-id\";s:3:\"rss\";}s:6:\"tumblr\";a:4:{s:4:\"name\";s:6:\"Tumblr\";s:11:\"description\";s:53:\"Import posts &amp; media from Tumblr using their API.\";s:11:\"plugin-slug\";s:15:\"tumblr-importer\";s:11:\"importer-id\";s:6:\"tumblr\";}s:9:\"wordpress\";a:4:{s:4:\"name\";s:9:\"WordPress\";s:11:\"description\";s:96:\"Import posts, pages, comments, custom fields, categories, and tags from a WordPress export file.\";s:11:\"plugin-slug\";s:18:\"wordpress-importer\";s:11:\"importer-id\";s:9:\"wordpress\";}}s:10:\"translated\";b:0;}', 'no'),
(2366, 'new_admin_email', 'keylor@baum.digital', 'yes'),
(2426, '_transient_timeout_et_core_path', '1565118133', 'no'),
(2394, 'layout_category_children', 'a:0:{}', 'yes'),
(2395, 'project_category_children', 'a:0:{}', 'yes'),
(2428, '_transient_timeout_et_core_version', '1565118133', 'no'),
(2425, 'wp-optimize-install-or-update-notice-version', '1.0', 'yes'),
(2244, '_site_transient_timeout_community-events-d41d8cd98f00b204e9800998ecf8427e', '1565065395', 'no'),
(2245, '_site_transient_community-events-d41d8cd98f00b204e9800998ecf8427e', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";b:0;}s:6:\"events\";a:5:{i:0;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:69:\"Conformación De Cooperativa De Impulso Para Proyectos Emprendedores \";s:3:\"url\";s:80:\"https://www.meetup.com/WordPress-Meetup-de-San-Carlos-Alajuela/events/263306725/\";s:6:\"meetup\";s:27:\"WordPress Meetup San Carlos\";s:10:\"meetup_url\";s:63:\"https://www.meetup.com/WordPress-Meetup-de-San-Carlos-Alajuela/\";s:4:\"date\";s:19:\"2019-08-06 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:22:\"Cd Quesada, Costa Rica\";s:7:\"country\";s:2:\"cr\";s:8:\"latitude\";d:10.316226;s:9:\"longitude\";d:-84.42046;}}i:1;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:58:\"Ingenierí­a de Requerimientos / Monetización de la Web.\";s:3:\"url\";s:52:\"https://www.meetup.com/wpcostarica/events/263401649/\";s:6:\"meetup\";s:38:\"San José, Costa Rica WordPress Meetup\";s:10:\"meetup_url\";s:35:\"https://www.meetup.com/wpcostarica/\";s:4:\"date\";s:19:\"2019-08-07 19:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:32:\"San José, San José, Costa Rica\";s:7:\"country\";s:2:\"CR\";s:8:\"latitude\";d:9.9300003051758;s:9:\"longitude\";d:-84.080001831055;}}i:2;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:61:\"Taller Herramientas de Accesibilidad en WordPress AMP / ANDI \";s:3:\"url\";s:52:\"https://www.meetup.com/wpcostarica/events/263172037/\";s:6:\"meetup\";s:38:\"San José, Costa Rica WordPress Meetup\";s:10:\"meetup_url\";s:35:\"https://www.meetup.com/wpcostarica/\";s:4:\"date\";s:19:\"2019-08-08 18:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:22:\"Curridabat, Costa Rica\";s:7:\"country\";s:2:\"cr\";s:8:\"latitude\";d:9.923833;s:9:\"longitude\";d:-84.04291;}}i:3;a:7:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:30:\"WordCamp San José/ Costa Rica\";s:3:\"url\";s:33:\"https://2019.sanjose.wordcamp.org\";s:6:\"meetup\";N;s:10:\"meetup_url\";N;s:4:\"date\";s:19:\"2019-09-07 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:21:\"San José/ Costa Rica\";s:7:\"country\";s:2:\"CR\";s:8:\"latitude\";d:9.932826;s:9:\"longitude\";d:-84.043475;}}i:4;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:35:\"WordCamp San José, Costa Rica 2019\";s:3:\"url\";s:52:\"https://www.meetup.com/wpcostarica/events/258963543/\";s:6:\"meetup\";s:38:\"San José, Costa Rica WordPress Meetup\";s:10:\"meetup_url\";s:35:\"https://www.meetup.com/wpcostarica/\";s:4:\"date\";s:19:\"2019-09-07 08:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:33:\"San José, Costa Rica, Costa Rica\";s:7:\"country\";s:2:\"cr\";s:8:\"latitude\";d:9.932826;s:9:\"longitude\";d:-84.04347;}}}}', 'no'),
(2252, '_site_transient_timeout_available_translations', '1565033137', 'no');
INSERT INTO `baum_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2253, '_site_transient_available_translations', 'a:117:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.0.4\";s:7:\"updated\";s:19:\"2019-05-16 12:52:45\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.4/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-15 14:27:16\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.10\";s:7:\"updated\";s:19:\"2019-05-14 14:59:20\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.10/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-10 20:13:25\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-16 03:25:39\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-14 07:58:27\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-14 11:54:05\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-14 05:36:50\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-11 08:51:16\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-25 20:13:18\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-10 16:02:58\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:30:49\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.2.1/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:30:07\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-07 21:15:55\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-10 16:03:04\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/5.2.1/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-06 05:44:03\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-18 10:42:19\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:25:10\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:33:22\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-06-06 15:48:01\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-18 10:42:08\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-05-19 17:09:08\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 03:26:22\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-11 15:51:57\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-06 15:53:48\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-12-06 21:26:01\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.0/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-12-07 18:38:30\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.0/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:6:\"4.9.10\";s:7:\"updated\";s:19:\"2019-05-23 02:23:28\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.10/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-18 12:06:18\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:9:\"5.0-beta3\";s:7:\"updated\";s:19:\"2018-11-28 16:04:33\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0-beta3/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-05-29 05:00:30\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-12 08:08:56\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 01:38:53\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-01-31 11:16:06\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:32:08\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-15 09:41:31\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-05-28 10:07:08\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-11 14:05:23\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-19 14:36:40\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 13:16:13\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-14 07:31:31\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-18 06:50:11\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-02-21 08:17:32\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-21 14:15:57\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 07:34:10\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.3/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.10\";s:7:\"updated\";s:19:\"2019-05-08 04:00:57\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.10/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 14:27:41\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.9.9\";s:7:\"updated\";s:19:\"2018-12-18 14:32:44\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.9/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-07-28 18:49:10\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:6:\"4.7.13\";s:7:\"updated\";s:19:\"2019-05-10 10:24:08\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.13/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.6/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-30 20:27:25\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-10 17:45:28\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 06:02:07\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-23 09:40:21\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.1.1/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:34:52\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-09 16:36:42\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:8:\"5.2-beta\";s:7:\"updated\";s:19:\"2019-04-09 16:46:27\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/translation/core/5.2-beta/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 08:23:12\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 09:24:27\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.2.1/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-08 11:59:02\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-11 13:56:15\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-18 13:30:06\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-15 12:19:22\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 15:22:52\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-14 13:39:13\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2.1/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-04-02 15:10:17\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.1/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-05-21 18:58:08\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-13 17:16:16\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-16 03:13:05\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-12 23:35:59\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 19:34:53\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-03-31 10:39:40\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.1/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-23 12:32:40\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.0.3/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-05-24 10:49:36\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.1/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"5.2.1\";s:7:\"updated\";s:19:\"2019-06-17 05:35:37\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.1/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-09 17:07:08\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.1.1\";s:7:\"updated\";s:19:\"2019-07-29 00:19:11\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.1/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(2421, 'wp-optimize-compression_server', 'resmushit', 'yes'),
(2422, 'wp-optimize-image_quality', 'very_good', 'yes'),
(2423, 'wp-optimize-back_up_original', '1', 'yes'),
(2305, 'wpo_cache_config', 'a:15:{s:19:\"enable_page_caching\";b:0;s:23:\"page_cache_length_value\";i:24;s:22:\"page_cache_length_unit\";s:5:\"hours\";s:17:\"page_cache_length\";i:86400;s:20:\"cache_exception_urls\";a:0:{}s:23:\"cache_exception_cookies\";a:0:{}s:30:\"cache_exception_browser_agents\";a:0:{}s:22:\"enable_sitemap_preload\";b:0;s:23:\"enable_schedule_preload\";b:0;s:21:\"preload_schedule_type\";s:0:\"\";s:21:\"enable_mobile_caching\";b:0;s:19:\"enable_user_caching\";b:0;s:8:\"site_url\";s:28:\"http://localhost/wp-starter/\";s:17:\"wpo_cache_cookies\";a:0:{}s:25:\"wpo_cache_query_variables\";a:0:{}}', 'yes'),
(2429, '_transient_et_core_version', '3.26.6', 'no'),
(2427, '_transient_et_core_path', 'C:/wamp/www/wp-starter/wp-content/themes/Divi/core', 'no'),
(2095, 'updraft_task_manager_dbversion', '1.1', 'yes'),
(2096, 'autoptimize_imgopt_settings', 'a:5:{s:35:\"autoptimize_imgopt_checkbox_field_1\";N;s:33:\"autoptimize_imgopt_select_field_2\";N;s:35:\"autoptimize_imgopt_checkbox_field_3\";s:1:\"0\";s:35:\"autoptimize_imgopt_checkbox_field_4\";s:1:\"0\";s:31:\"autoptimize_imgopt_text_field_5\";s:0:\"\";}', 'yes'),
(2431, '_transient_timeout_et_divi_customizer_option_set', '1565129737', 'no'),
(2432, '_transient_et_divi_customizer_option_set', 'theme', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_postmeta`
--

DROP TABLE IF EXISTS `baum_postmeta`;
CREATE TABLE IF NOT EXISTS `baum_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_postmeta`
--

INSERT INTO `baum_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 2, '_edit_lock', '1525101731:1'),
(3, 1, '_edit_lock', '1525105124:1'),
(4, 4, '_edit_last', '1'),
(5, 4, '_wp_page_template', 'default'),
(6, 4, '_et_pb_post_hide_nav', 'default'),
(7, 4, '_et_pb_page_layout', 'et_no_sidebar'),
(8, 4, '_et_pb_side_nav', 'off'),
(9, 4, '_et_pb_use_builder', 'on'),
(10, 4, '_et_builder_version', 'BB|Divi|3.26.6'),
(11, 4, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(12, 4, '_et_pb_old_content', 'Este es el home'),
(13, 4, '_et_pb_enable_shortcode_tracking', ''),
(14, 4, '_et_pb_custom_css', ''),
(15, 4, '_edit_lock', '1565031372:1'),
(16, 6, '_edit_last', '1'),
(17, 6, '_edit_lock', '1552344699:1'),
(18, 6, '_wp_page_template', 'default'),
(19, 6, '_et_pb_post_hide_nav', 'default'),
(20, 6, '_et_pb_page_layout', 'et_right_sidebar'),
(21, 6, '_et_pb_side_nav', 'off'),
(22, 6, '_et_pb_use_builder', 'off'),
(23, 6, '_et_builder_version', 'BB|Divi|3.20.1'),
(24, 6, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(25, 6, '_et_pb_old_content', 'Esta es la página 1'),
(26, 6, '_et_pb_enable_shortcode_tracking', ''),
(27, 6, '_et_pb_custom_css', ''),
(28, 8, '_edit_last', '1'),
(29, 8, '_edit_lock', '1541442465:1'),
(30, 8, '_wp_page_template', 'default'),
(31, 8, '_et_pb_post_hide_nav', 'default'),
(32, 8, '_et_pb_page_layout', 'et_no_sidebar'),
(33, 8, '_et_pb_side_nav', 'off'),
(34, 8, '_et_pb_use_builder', ''),
(35, 8, '_et_builder_version', 'BB|Divi|3.14'),
(36, 8, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(37, 8, '_et_pb_old_content', ''),
(38, 8, '_et_pb_enable_shortcode_tracking', ''),
(39, 8, '_et_pb_custom_css', ''),
(40, 10, '_menu_item_type', 'post_type'),
(41, 10, '_menu_item_menu_item_parent', '0'),
(42, 10, '_menu_item_object_id', '8'),
(43, 10, '_menu_item_object', 'page'),
(44, 10, '_menu_item_target', ''),
(45, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(46, 10, '_menu_item_xfn', ''),
(47, 10, '_menu_item_url', ''),
(76, 14, '_pll_strings_translations', 'a:0:{}'),
(49, 11, '_menu_item_type', 'post_type'),
(50, 11, '_menu_item_menu_item_parent', '0'),
(51, 11, '_menu_item_object_id', '6'),
(52, 11, '_menu_item_object', 'page'),
(53, 11, '_menu_item_target', ''),
(54, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(55, 11, '_menu_item_xfn', ''),
(56, 11, '_menu_item_url', ''),
(58, 12, '_menu_item_type', 'post_type'),
(59, 12, '_menu_item_menu_item_parent', '0'),
(60, 12, '_menu_item_object_id', '4'),
(61, 12, '_menu_item_object', 'page'),
(62, 12, '_menu_item_target', ''),
(63, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(64, 12, '_menu_item_xfn', ''),
(65, 12, '_menu_item_url', ''),
(77, 15, '_pll_strings_translations', 'a:0:{}'),
(78, 16, '_wp_page_template', 'default'),
(79, 16, '_edit_last', '1'),
(80, 16, '_edit_lock', '1552340277:1'),
(81, 16, '_et_pb_post_hide_nav', 'default'),
(82, 16, '_et_pb_page_layout', 'et_right_sidebar'),
(83, 16, '_et_pb_side_nav', 'off'),
(84, 16, '_et_pb_use_builder', ''),
(85, 16, '_et_builder_version', 'BB|Divi|3.2'),
(86, 16, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(87, 16, '_et_pb_old_content', ''),
(88, 16, '_et_pb_enable_shortcode_tracking', ''),
(89, 16, '_et_pb_custom_css', ''),
(90, 18, '_menu_item_type', 'custom'),
(91, 18, '_menu_item_menu_item_parent', '0'),
(92, 18, '_menu_item_object_id', '18'),
(93, 18, '_menu_item_object', 'custom'),
(94, 18, '_menu_item_target', ''),
(95, 18, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 18, '_menu_item_xfn', ''),
(97, 18, '_menu_item_url', '#pll_switcher'),
(99, 18, '_pll_menu_item', 'a:6:{s:22:\"hide_if_no_translation\";i:0;s:12:\"hide_current\";i:1;s:10:\"force_home\";i:0;s:10:\"show_flags\";i:0;s:10:\"show_names\";i:1;s:8:\"dropdown\";i:0;}'),
(105, 4, '_et_pb_first_image', ''),
(106, 4, '_et_pb_truncate_post', ''),
(107, 41, '_wp_attached_file', '2019/03/limes.jpg'),
(108, 41, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.4616280065914466;s:5:\"bytes\";i:10838;s:11:\"size_before\";i:198439;s:10:\"size_after\";i:187601;s:4:\"time\";d:0.13999999999999999;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:8:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.2300000000000004;s:5:\"bytes\";i:239;s:11:\"size_before\";i:4572;s:10:\"size_after\";i:4333;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.4299999999999997;s:5:\"bytes\";i:548;s:11:\"size_before\";i:8526;s:10:\"size_after\";i:7978;s:4:\"time\";d:0.01;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.1399999999999997;s:5:\"bytes\";i:3853;s:11:\"size_before\";i:62751;s:10:\"size_after\";i:58898;s:4:\"time\";d:0.040000000000000001;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.8799999999999999;s:5:\"bytes\";i:611;s:11:\"size_before\";i:12532;s:10:\"size_after\";i:11921;s:4:\"time\";d:0.01;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.3099999999999996;s:5:\"bytes\";i:2517;s:11:\"size_before\";i:47433;s:10:\"size_after\";i:44916;s:4:\"time\";d:0.029999999999999999;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.5199999999999996;s:5:\"bytes\";i:642;s:11:\"size_before\";i:14198;s:10:\"size_after\";i:13556;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.7599999999999998;s:5:\"bytes\";i:1046;s:11:\"size_before\";i:21971;s:10:\"size_after\";i:20925;s:4:\"time\";d:0.01;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.2199999999999998;s:5:\"bytes\";i:1382;s:11:\"size_before\";i:26456;s:10:\"size_after\";i:25074;s:4:\"time\";d:0.02;}}}'),
(109, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:1200;s:4:\"file\";s:17:\"2019/03/limes.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"limes-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"limes-150x300.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"limes-512x1024.jpg\";s:5:\"width\";i:512;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:17:\"limes-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:17:\"limes-600x675.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:17:\"limes-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:17:\"limes-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:17:\"limes-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(110, 42, '_wp_attached_file', '2019/03/springrolls.jpg'),
(111, 42, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:4.1261793492637331;s:5:\"bytes\";i:9359;s:11:\"size_before\";i:226820;s:10:\"size_after\";i:217461;s:4:\"time\";d:0.17000000000000001;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:8:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.3700000000000001;s:5:\"bytes\";i:314;s:11:\"size_before\";i:5851;s:10:\"size_after\";i:5537;s:4:\"time\";d:0.02;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.9400000000000004;s:5:\"bytes\";i:541;s:11:\"size_before\";i:9110;s:10:\"size_after\";i:8569;s:4:\"time\";d:0.01;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.5800000000000001;s:5:\"bytes\";i:2839;s:11:\"size_before\";i:61988;s:10:\"size_after\";i:59149;s:4:\"time\";d:0.029999999999999999;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.8300000000000001;s:5:\"bytes\";i:661;s:11:\"size_before\";i:17243;s:10:\"size_after\";i:16582;s:4:\"time\";d:0.02;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.4500000000000002;s:5:\"bytes\";i:1908;s:11:\"size_before\";i:55231;s:10:\"size_after\";i:53323;s:4:\"time\";d:0.02;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.8900000000000001;s:5:\"bytes\";i:734;s:11:\"size_before\";i:18888;s:10:\"size_after\";i:18154;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.5699999999999998;s:5:\"bytes\";i:1003;s:11:\"size_before\";i:28082;s:10:\"size_after\";i:27079;s:4:\"time\";d:0.02;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.4699999999999998;s:5:\"bytes\";i:1359;s:11:\"size_before\";i:30427;s:10:\"size_after\";i:29068;s:4:\"time\";d:0.040000000000000001;}}}'),
(112, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:1200;s:4:\"file\";s:23:\"2019/03/springrolls.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"springrolls-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"springrolls-150x300.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"springrolls-512x1024.jpg\";s:5:\"width\";i:512;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:23:\"springrolls-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:23:\"springrolls-600x675.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:23:\"springrolls-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:23:\"springrolls-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:23:\"springrolls-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(113, 43, '_wp_attached_file', '2019/03/eggs.jpg'),
(114, 43, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:6.9352901934623086;s:5:\"bytes\";i:7797;s:11:\"size_before\";i:112425;s:10:\"size_after\";i:104628;s:4:\"time\";d:0.12000000000000001;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:8:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.4800000000000004;s:5:\"bytes\";i:171;s:11:\"size_before\";i:3118;s:10:\"size_after\";i:2947;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.7599999999999998;s:5:\"bytes\";i:331;s:11:\"size_before\";i:5751;s:10:\"size_after\";i:5420;s:4:\"time\";d:0.01;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.5300000000000002;s:5:\"bytes\";i:2706;s:11:\"size_before\";i:35956;s:10:\"size_after\";i:33250;s:4:\"time\";d:0.02;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.0099999999999998;s:5:\"bytes\";i:361;s:11:\"size_before\";i:7199;s:10:\"size_after\";i:6838;s:4:\"time\";d:0.01;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:8.1999999999999993;s:5:\"bytes\";i:2035;s:11:\"size_before\";i:24802;s:10:\"size_after\";i:22767;s:4:\"time\";d:0.02;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.1900000000000004;s:5:\"bytes\";i:423;s:11:\"size_before\";i:8143;s:10:\"size_after\";i:7720;s:4:\"time\";d:0.02;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.2999999999999998;s:5:\"bytes\";i:770;s:11:\"size_before\";i:12228;s:10:\"size_after\";i:11458;s:4:\"time\";d:0.02;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.5700000000000003;s:5:\"bytes\";i:1000;s:11:\"size_before\";i:15228;s:10:\"size_after\";i:14228;s:4:\"time\";d:0.01;}}}'),
(115, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:1200;s:4:\"file\";s:16:\"2019/03/eggs.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"eggs-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"eggs-150x300.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"eggs-512x1024.jpg\";s:5:\"width\";i:512;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:16:\"eggs-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:16:\"eggs-600x675.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:16:\"eggs-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:16:\"eggs-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:16:\"eggs-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(116, 44, '_wp_attached_file', '2019/03/lemons.jpg'),
(117, 44, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:4.8247479311750219;s:5:\"bytes\";i:10891;s:11:\"size_before\";i:225732;s:10:\"size_after\";i:214841;s:4:\"time\";d:0.97000000000000008;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:8:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.0099999999999998;s:5:\"bytes\";i:336;s:11:\"size_before\";i:5593;s:10:\"size_after\";i:5257;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.8399999999999999;s:5:\"bytes\";i:647;s:11:\"size_before\";i:9462;s:10:\"size_after\";i:8815;s:4:\"time\";d:0.82999999999999996;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.7599999999999998;s:5:\"bytes\";i:3592;s:11:\"size_before\";i:62319;s:10:\"size_after\";i:58727;s:4:\"time\";d:0.050000000000000003;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.8199999999999998;s:5:\"bytes\";i:696;s:11:\"size_before\";i:18207;s:10:\"size_after\";i:17511;s:4:\"time\";d:0.02;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.3799999999999999;s:5:\"bytes\";i:2290;s:11:\"size_before\";i:52288;s:10:\"size_after\";i:49998;s:4:\"time\";d:0.02;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.8900000000000001;s:5:\"bytes\";i:756;s:11:\"size_before\";i:19457;s:10:\"size_after\";i:18701;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.5899999999999999;s:5:\"bytes\";i:1040;s:11:\"size_before\";i:28970;s:10:\"size_after\";i:27930;s:4:\"time\";d:0.01;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.21;s:5:\"bytes\";i:1534;s:11:\"size_before\";i:29436;s:10:\"size_after\";i:27902;s:4:\"time\";d:0.02;}}}'),
(118, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:1200;s:4:\"file\";s:18:\"2019/03/lemons.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"lemons-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"lemons-150x300.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"lemons-512x1024.jpg\";s:5:\"width\";i:512;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:18:\"lemons-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:18:\"lemons-600x675.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:18:\"lemons-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:18:\"lemons-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:18:\"lemons-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 45, '_wp_attached_file', '2019/03/jar.jpg'),
(120, 45, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.0329538645895751;s:5:\"bytes\";i:4032;s:11:\"size_before\";i:80112;s:10:\"size_after\";i:76080;s:4:\"time\";d:0.080000000000000002;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.46;s:5:\"bytes\";i:122;s:11:\"size_before\";i:3526;s:10:\"size_after\";i:3404;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.3600000000000003;s:5:\"bytes\";i:343;s:11:\"size_before\";i:7871;s:10:\"size_after\";i:7528;s:4:\"time\";d:0.01;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.8399999999999999;s:5:\"bytes\";i:583;s:11:\"size_before\";i:12054;s:10:\"size_after\";i:11471;s:4:\"time\";d:0.02;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.6399999999999997;s:5:\"bytes\";i:580;s:11:\"size_before\";i:12499;s:10:\"size_after\";i:11919;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.8399999999999999;s:5:\"bytes\";i:1154;s:11:\"size_before\";i:19762;s:10:\"size_after\";i:18608;s:4:\"time\";d:0.01;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.1200000000000001;s:5:\"bytes\";i:1250;s:11:\"size_before\";i:24400;s:10:\"size_after\";i:23150;s:4:\"time\";d:0.02;}}}'),
(121, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:764;s:6:\"height\";i:638;s:4:\"file\";s:15:\"2019/03/jar.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"jar-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"jar-300x251.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:251;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:15:\"jar-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:15:\"jar-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:15:\"jar-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:15:\"jar-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(122, 46, '_wp_attached_file', '2019/03/cake.jpg'),
(123, 46, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:4.4920812505773551;s:5:\"bytes\";i:8753;s:11:\"size_before\";i:194854;s:10:\"size_after\";i:186101;s:4:\"time\";d:0.099999999999999992;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.2000000000000002;s:5:\"bytes\";i:252;s:11:\"size_before\";i:4848;s:10:\"size_after\";i:4596;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.0800000000000001;s:5:\"bytes\";i:749;s:11:\"size_before\";i:14753;s:10:\"size_after\";i:14004;s:4:\"time\";d:0.01;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.96;s:5:\"bytes\";i:1028;s:11:\"size_before\";i:20737;s:10:\"size_after\";i:19709;s:4:\"time\";d:0.01;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.4100000000000001;s:5:\"bytes\";i:2842;s:11:\"size_before\";i:64452;s:10:\"size_after\";i:61610;s:4:\"time\";d:0.029999999999999999;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.6100000000000003;s:5:\"bytes\";i:1016;s:11:\"size_before\";i:22047;s:10:\"size_after\";i:21031;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.1500000000000004;s:5:\"bytes\";i:1372;s:11:\"size_before\";i:33074;s:10:\"size_after\";i:31702;s:4:\"time\";d:0.01;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.2800000000000002;s:5:\"bytes\";i:1494;s:11:\"size_before\";i:34943;s:10:\"size_after\";i:33449;s:4:\"time\";d:0.02;}}}'),
(124, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:735;s:6:\"height\";i:719;s:4:\"file\";s:16:\"2019/03/cake.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"cake-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"cake-300x293.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:293;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:16:\"cake-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:16:\"cake-735x675.jpg\";s:5:\"width\";i:735;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:16:\"cake-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:16:\"cake-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:16:\"cake-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(125, 47, '_wp_attached_file', '2019/03/cheese.jpg'),
(126, 47, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.1906980179963895;s:5:\"bytes\";i:22486;s:11:\"size_before\";i:433198;s:10:\"size_after\";i:410712;s:4:\"time\";d:0.18999999999999997;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:10:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.9800000000000004;s:5:\"bytes\";i:393;s:11:\"size_before\";i:6574;s:10:\"size_after\";i:6181;s:4:\"time\";d:0.01;}s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.9500000000000002;s:5:\"bytes\";i:415;s:11:\"size_before\";i:8381;s:10:\"size_after\";i:7966;s:4:\"time\";d:0.01;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.46;s:5:\"bytes\";i:2304;s:11:\"size_before\";i:42229;s:10:\"size_after\";i:39925;s:4:\"time\";d:0.02;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.3600000000000003;s:5:\"bytes\";i:3664;s:11:\"size_before\";i:68373;s:10:\"size_after\";i:64709;s:4:\"time\";d:0.02;}s:21:\"et-pb-post-main-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.46;s:5:\"bytes\";i:1202;s:11:\"size_before\";i:22024;s:10:\"size_after\";i:20822;s:4:\"time\";d:0.02;}s:31:\"et-pb-post-main-image-fullwidth\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.1100000000000003;s:5:\"bytes\";i:5174;s:11:\"size_before\";i:101204;s:10:\"size_after\";i:96030;s:4:\"time\";d:0.029999999999999999;}s:21:\"et-pb-portfolio-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.4299999999999997;s:5:\"bytes\";i:1367;s:11:\"size_before\";i:25172;s:10:\"size_after\";i:23805;s:4:\"time\";d:0.01;}s:28:\"et-pb-portfolio-module-image\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.0499999999999998;s:5:\"bytes\";i:2035;s:11:\"size_before\";i:40297;s:10:\"size_after\";i:38262;s:4:\"time\";d:0.02;}s:28:\"et-pb-portfolio-image-single\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.21;s:5:\"bytes\";i:3890;s:11:\"size_before\";i:74614;s:10:\"size_after\";i:70724;s:4:\"time\";d:0.029999999999999999;}s:35:\"et-pb-gallery-module-image-portrait\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.6100000000000003;s:5:\"bytes\";i:2042;s:11:\"size_before\";i:44330;s:10:\"size_after\";i:42288;s:4:\"time\";d:0.02;}}}'),
(127, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:556;s:4:\"file\";s:18:\"2019/03/cheese.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"cheese-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"cheese-300x130.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:130;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"cheese-768x334.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:334;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"cheese-1024x445.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:445;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-post-main-image\";a:4:{s:4:\"file\";s:18:\"cheese-400x250.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:31:\"et-pb-post-main-image-fullwidth\";a:4:{s:4:\"file\";s:19:\"cheese-1080x556.jpg\";s:5:\"width\";i:1080;s:6:\"height\";i:556;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"et-pb-portfolio-image\";a:4:{s:4:\"file\";s:18:\"cheese-400x284.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:284;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-module-image\";a:4:{s:4:\"file\";s:18:\"cheese-510x382.jpg\";s:5:\"width\";i:510;s:6:\"height\";i:382;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"et-pb-portfolio-image-single\";a:4:{s:4:\"file\";s:19:\"cheese-1080x469.jpg\";s:5:\"width\";i:1080;s:6:\"height\";i:469;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:35:\"et-pb-gallery-module-image-portrait\";a:4:{s:4:\"file\";s:18:\"cheese-400x516.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:516;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(128, 6, '_et_pb_first_image', ''),
(129, 6, '_et_pb_truncate_post', ''),
(243, 4, '_et_pb_gutter_width', '3'),
(132, 2943, '_edit_last', '1'),
(133, 2943, '_et_post_bg_color', '#ffffff'),
(134, 2943, '_et_post_bg_layout', 'light'),
(135, 2943, '_et_pb_show_title', 'on'),
(136, 2943, '_et_pb_post_hide_nav', 'default'),
(137, 2943, '_et_pb_page_layout', 'et_right_sidebar'),
(138, 2943, '_et_pb_side_nav', 'off'),
(139, 2943, '_et_pb_use_builder', ''),
(140, 2943, '_et_builder_version', 'BB|Divi|3.24.1'),
(141, 2943, '_et_pb_first_image', ''),
(142, 2943, '_et_pb_truncate_post', ''),
(143, 2943, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(144, 2943, '_et_pb_old_content', ''),
(145, 2943, '_et_pb_enable_shortcode_tracking', ''),
(146, 2943, '_et_pb_custom_css', ''),
(147, 2945, '_et_post_bg_color', '#ffffff'),
(148, 2945, '_et_post_bg_layout', 'light'),
(149, 2945, '_et_pb_show_title', 'on'),
(150, 2945, '_et_pb_post_hide_nav', 'default'),
(151, 2945, '_et_pb_page_layout', 'et_right_sidebar'),
(152, 2945, '_et_pb_side_nav', 'off'),
(153, 2945, '_et_pb_use_builder', ''),
(154, 2945, '_et_builder_version', 'BB|Divi|3.24.1'),
(155, 2945, '_et_pb_first_image', ''),
(156, 2945, '_et_pb_truncate_post', ''),
(157, 2945, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(158, 2945, '_et_pb_old_content', ''),
(159, 2945, '_et_pb_enable_shortcode_tracking', ''),
(160, 2945, '_et_pb_custom_css', ''),
(161, 2945, '_dp_original', '2943'),
(162, 2945, '_edit_last', '1'),
(163, 2946, '_et_post_bg_color', '#ffffff'),
(164, 2946, '_et_post_bg_layout', 'light'),
(165, 2946, '_et_pb_show_title', 'on'),
(166, 2946, '_et_pb_post_hide_nav', 'default'),
(167, 2946, '_et_pb_page_layout', 'et_right_sidebar'),
(168, 2946, '_et_pb_side_nav', 'off'),
(169, 2946, '_et_pb_use_builder', ''),
(170, 2946, '_et_builder_version', 'BB|Divi|3.24.1'),
(171, 2946, '_et_pb_first_image', ''),
(172, 2946, '_et_pb_truncate_post', ''),
(173, 2946, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(174, 2946, '_et_pb_old_content', ''),
(175, 2946, '_et_pb_enable_shortcode_tracking', ''),
(176, 2946, '_et_pb_custom_css', ''),
(177, 2946, '_dp_original', '2943'),
(178, 2946, '_edit_last', '1'),
(179, 2947, '_et_post_bg_color', '#ffffff'),
(180, 2947, '_et_post_bg_layout', 'light'),
(181, 2947, '_et_pb_show_title', 'on'),
(182, 2947, '_et_pb_post_hide_nav', 'default'),
(183, 2947, '_et_pb_page_layout', 'et_right_sidebar'),
(184, 2947, '_et_pb_side_nav', 'off'),
(185, 2947, '_et_pb_use_builder', ''),
(186, 2947, '_et_builder_version', 'BB|Divi|3.24.1'),
(187, 2947, '_et_pb_first_image', ''),
(188, 2947, '_et_pb_truncate_post', ''),
(189, 2947, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(190, 2947, '_et_pb_old_content', ''),
(191, 2947, '_et_pb_enable_shortcode_tracking', ''),
(192, 2947, '_et_pb_custom_css', ''),
(193, 2947, '_dp_original', '2943'),
(194, 2947, '_edit_last', '1'),
(195, 2948, '_et_post_bg_color', '#ffffff'),
(196, 2948, '_et_post_bg_layout', 'light'),
(197, 2948, '_et_pb_show_title', 'on'),
(198, 2948, '_et_pb_post_hide_nav', 'default'),
(199, 2948, '_et_pb_page_layout', 'et_right_sidebar'),
(200, 2948, '_et_pb_side_nav', 'off'),
(201, 2948, '_et_pb_use_builder', ''),
(202, 2948, '_et_builder_version', 'BB|Divi|3.24.1'),
(203, 2948, '_et_pb_first_image', ''),
(204, 2948, '_et_pb_truncate_post', ''),
(205, 2948, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(206, 2948, '_et_pb_old_content', ''),
(207, 2948, '_et_pb_enable_shortcode_tracking', ''),
(208, 2948, '_et_pb_custom_css', ''),
(209, 2948, '_dp_original', '2943'),
(210, 2948, '_edit_last', '1'),
(211, 2949, '_et_post_bg_color', '#ffffff'),
(212, 2949, '_et_post_bg_layout', 'light'),
(213, 2949, '_et_pb_show_title', 'on'),
(214, 2949, '_et_pb_post_hide_nav', 'default'),
(215, 2949, '_et_pb_page_layout', 'et_right_sidebar'),
(216, 2949, '_et_pb_side_nav', 'off'),
(217, 2949, '_et_pb_use_builder', ''),
(218, 2949, '_et_builder_version', 'BB|Divi|3.24.1'),
(219, 2949, '_et_pb_first_image', ''),
(220, 2949, '_et_pb_truncate_post', ''),
(221, 2949, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(222, 2949, '_et_pb_old_content', ''),
(223, 2949, '_et_pb_enable_shortcode_tracking', ''),
(224, 2949, '_et_pb_custom_css', ''),
(225, 2949, '_dp_original', '2943'),
(226, 2949, '_edit_last', '1'),
(227, 2950, '_et_post_bg_color', '#ffffff'),
(228, 2950, '_et_post_bg_layout', 'light'),
(229, 2950, '_et_pb_show_title', 'on'),
(230, 2950, '_et_pb_post_hide_nav', 'default'),
(231, 2950, '_et_pb_page_layout', 'et_right_sidebar'),
(232, 2950, '_et_pb_side_nav', 'off'),
(233, 2950, '_et_pb_use_builder', ''),
(234, 2950, '_et_builder_version', 'BB|Divi|3.24.1'),
(235, 2950, '_et_pb_first_image', ''),
(236, 2950, '_et_pb_truncate_post', ''),
(237, 2950, '_et_pb_ab_stats_refresh_interval', 'hourly'),
(238, 2950, '_et_pb_old_content', ''),
(239, 2950, '_et_pb_enable_shortcode_tracking', ''),
(240, 2950, '_et_pb_custom_css', ''),
(241, 2950, '_dp_original', '2943'),
(242, 2950, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_posts`
--

DROP TABLE IF EXISTS `baum_posts`;
CREATE TABLE IF NOT EXISTS `baum_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=2961 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_posts`
--

INSERT INTO `baum_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-04-30 14:55:36', '2018-04-30 14:55:36', 'Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡y comienza a escribir!', '¡Hola mundo!', '', 'publish', 'open', 'open', '', 'hola-mundo', '', '', '2018-04-30 14:55:36', '2018-04-30 14:55:36', '', 0, 'http://localhost/wordpress-starter/?p=1', 0, 'post', '', 0),
(2, 1, '2018-04-30 14:55:36', '2018-04-30 14:55:36', 'Esta es una página de ejemplo, Es diferente a una entrada de un blog porque se mantiene estática y se mostrará en la barra de navegación (en la mayoría de temas). Casi todo el mundo comienza con una página Sobre mí para presentarse a los potenciales visitantes. Podría ser algo así: <blockquote>¡Hola!: Soy mensajero por el día, aspirante a actor por la noche y esta es mi web. Vivo en Madrid, tengo perrazo llamado Juan y me encantan las piñas coladas (y que me pille un chaparrón)</blockquote> … o algo así: <blockquote>La empresa Calcetines XYZ se fundó en 1971, y ha estado produciendo calcetines de calidad para sus clientes desde entonces. Ubicada en Gothan, la empresa XYZ tiene más de 2.000 empleados e intenta ayudar en lo que puede para mejorar la vida en Gothan</blockquote> Si eres nuevo en WordPress deberías ir a <a href=\"http://localhost/wordpress-starter/wp-admin/\">tu escritorio</a> para borrar esta página y crear algunas nuevas con tu contenido. ¡Pásalo bien!', 'Página de ejemplo', '', 'publish', 'closed', 'open', '', 'pagina-ejemplo', '', '', '2018-04-30 14:55:36', '2018-04-30 14:55:36', '', 0, 'http://localhost/wordpress-starter/?page_id=2', 0, 'page', '', 0),
(20, 1, '2018-05-21 15:35:20', '2018-05-21 15:35:20', '', 'Divi-child', '', 'publish', 'closed', 'closed', '', 'divi-child', '', '', '2019-08-05 11:42:42', '2019-08-05 17:42:42', '', 0, 'http://localhost/wordpress-starter/2018/05/21/divi-child/', 0, 'custom_css', '', 0),
(4, 1, '2018-04-30 16:23:22', '2018-04-30 16:23:22', '[et_pb_section bb_built=\"1\" inner_width=\"auto\" inner_max_width=\"1080px\"][et_pb_row _builder_version=\"3.26.6\" use_custom_gutter=\"on\" gutter_width=\"1\" make_equal=\"on\" box_shadow_horizontal_tablet=\"0px\" box_shadow_vertical_tablet=\"0px\" box_shadow_blur_tablet=\"40px\" box_shadow_spread_tablet=\"0px\" module_class=\"row-image-to-left\" z_index_tablet=\"500\" width=\"100%\" max_width=\"100%\" custom_padding=\"0px|0px|0px|0px\"][et_pb_column type=\"1_2\" custom_padding__hover=\"|||\" custom_padding=\"|||\"][et_pb_text _builder_version=\"3.26.6\" text_text_shadow_horizontal_length=\"text_text_shadow_style,%91object Object%93\" text_text_shadow_horizontal_length_tablet=\"0px\" text_text_shadow_vertical_length=\"text_text_shadow_style,%91object Object%93\" text_text_shadow_vertical_length_tablet=\"0px\" text_text_shadow_blur_strength=\"text_text_shadow_style,%91object Object%93\" text_text_shadow_blur_strength_tablet=\"1px\" link_text_shadow_horizontal_length=\"link_text_shadow_style,%91object Object%93\" link_text_shadow_horizontal_length_tablet=\"0px\" link_text_shadow_vertical_length=\"link_text_shadow_style,%91object Object%93\" link_text_shadow_vertical_length_tablet=\"0px\" link_text_shadow_blur_strength=\"link_text_shadow_style,%91object Object%93\" link_text_shadow_blur_strength_tablet=\"1px\" ul_text_shadow_horizontal_length=\"ul_text_shadow_style,%91object Object%93\" ul_text_shadow_horizontal_length_tablet=\"0px\" ul_text_shadow_vertical_length=\"ul_text_shadow_style,%91object Object%93\" ul_text_shadow_vertical_length_tablet=\"0px\" ul_text_shadow_blur_strength=\"ul_text_shadow_style,%91object Object%93\" ul_text_shadow_blur_strength_tablet=\"1px\" ol_text_shadow_horizontal_length=\"ol_text_shadow_style,%91object Object%93\" ol_text_shadow_horizontal_length_tablet=\"0px\" ol_text_shadow_vertical_length=\"ol_text_shadow_style,%91object Object%93\" ol_text_shadow_vertical_length_tablet=\"0px\" ol_text_shadow_blur_strength=\"ol_text_shadow_style,%91object Object%93\" ol_text_shadow_blur_strength_tablet=\"1px\" quote_text_shadow_horizontal_length=\"quote_text_shadow_style,%91object Object%93\" quote_text_shadow_horizontal_length_tablet=\"0px\" quote_text_shadow_vertical_length=\"quote_text_shadow_style,%91object Object%93\" quote_text_shadow_vertical_length_tablet=\"0px\" quote_text_shadow_blur_strength=\"quote_text_shadow_style,%91object Object%93\" quote_text_shadow_blur_strength_tablet=\"1px\" header_text_shadow_horizontal_length=\"header_text_shadow_style,%91object Object%93\" header_text_shadow_horizontal_length_tablet=\"0px\" header_text_shadow_vertical_length=\"header_text_shadow_style,%91object Object%93\" header_text_shadow_vertical_length_tablet=\"0px\" header_text_shadow_blur_strength=\"header_text_shadow_style,%91object Object%93\" header_text_shadow_blur_strength_tablet=\"1px\" header_2_text_shadow_horizontal_length=\"header_2_text_shadow_style,%91object Object%93\" header_2_text_shadow_horizontal_length_tablet=\"0px\" header_2_text_shadow_vertical_length=\"header_2_text_shadow_style,%91object Object%93\" header_2_text_shadow_vertical_length_tablet=\"0px\" header_2_text_shadow_blur_strength=\"header_2_text_shadow_style,%91object Object%93\" header_2_text_shadow_blur_strength_tablet=\"1px\" header_3_text_shadow_horizontal_length=\"header_3_text_shadow_style,%91object Object%93\" header_3_text_shadow_horizontal_length_tablet=\"0px\" header_3_text_shadow_vertical_length=\"header_3_text_shadow_style,%91object Object%93\" header_3_text_shadow_vertical_length_tablet=\"0px\" header_3_text_shadow_blur_strength=\"header_3_text_shadow_style,%91object Object%93\" header_3_text_shadow_blur_strength_tablet=\"1px\" header_4_text_shadow_horizontal_length=\"header_4_text_shadow_style,%91object Object%93\" header_4_text_shadow_horizontal_length_tablet=\"0px\" header_4_text_shadow_vertical_length=\"header_4_text_shadow_style,%91object Object%93\" header_4_text_shadow_vertical_length_tablet=\"0px\" header_4_text_shadow_blur_strength=\"header_4_text_shadow_style,%91object Object%93\" header_4_text_shadow_blur_strength_tablet=\"1px\" header_5_text_shadow_horizontal_length=\"header_5_text_shadow_style,%91object Object%93\" header_5_text_shadow_horizontal_length_tablet=\"0px\" header_5_text_shadow_vertical_length=\"header_5_text_shadow_style,%91object Object%93\" header_5_text_shadow_vertical_length_tablet=\"0px\" header_5_text_shadow_blur_strength=\"header_5_text_shadow_style,%91object Object%93\" header_5_text_shadow_blur_strength_tablet=\"1px\" header_6_text_shadow_horizontal_length=\"header_6_text_shadow_style,%91object Object%93\" header_6_text_shadow_horizontal_length_tablet=\"0px\" header_6_text_shadow_vertical_length=\"header_6_text_shadow_style,%91object Object%93\" header_6_text_shadow_vertical_length_tablet=\"0px\" header_6_text_shadow_blur_strength=\"header_6_text_shadow_style,%91object Object%93\" header_6_text_shadow_blur_strength_tablet=\"1px\" box_shadow_horizontal_tablet=\"0px\" box_shadow_vertical_tablet=\"0px\" box_shadow_blur_tablet=\"40px\" box_shadow_spread_tablet=\"0px\" z_index_tablet=\"500\"]\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis congue ex.</p>\r\n<p>Mauris nec sapien a quam pulvinar auctor sed vitae orci. Aliquam commodo, neque eget auctor mollis, neque magna pharetra ligula, vel tempor dui urna et tellus. Proin mollis velit sed tellus posuere, et aliquet magna efficitur.</p>\r\n<p>Vivamus ullamcorper tellus nisi, eget hendrerit felis mollis nec.</p>\r\n\r\n[/et_pb_text][et_pb_divider _builder_version=\"3.26.6\" show_divider=\"off\" height=\"25px\" box_shadow_horizontal_tablet=\"0px\" box_shadow_vertical_tablet=\"0px\" box_shadow_blur_tablet=\"40px\" box_shadow_spread_tablet=\"0px\" z_index_tablet=\"500\" /][et_pb_button _builder_version=\"3.26.6\" button_text=\"Ver más\" button_url=\"#cv\" button_text_shadow_horizontal_length=\"button_text_shadow_style,%91object Object%93\" button_text_shadow_horizontal_length_tablet=\"0px\" button_text_shadow_vertical_length=\"button_text_shadow_style,%91object Object%93\" button_text_shadow_vertical_length_tablet=\"0px\" button_text_shadow_blur_strength=\"button_text_shadow_style,%91object Object%93\" button_text_shadow_blur_strength_tablet=\"1px\" box_shadow_horizontal_tablet=\"0px\" box_shadow_vertical_tablet=\"0px\" box_shadow_blur_tablet=\"40px\" box_shadow_spread_tablet=\"0px\" z_index_tablet=\"500\" /][/et_pb_column][et_pb_column type=\"1_2\" custom_padding__hover=\"|||\" custom_padding=\"|||\"][et_pb_image _builder_version=\"3.26.6\" src=\"http://localhost/wp-starter/wp-content/uploads/2019/03/jar.jpg\" box_shadow_horizontal_tablet=\"0px\" box_shadow_vertical_tablet=\"0px\" box_shadow_blur_tablet=\"40px\" box_shadow_spread_tablet=\"0px\" z_index_tablet=\"500\" module_class=\"bg-image\" /][/et_pb_column][/et_pb_row][/et_pb_section]', 'Inicio', '', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2019-08-05 12:34:31', '2019-08-05 18:34:31', '', 0, 'http://localhost/wordpress-starter/?page_id=4', 0, 'page', '', 0),
(6, 1, '2018-04-30 16:27:12', '2018-04-30 16:27:12', 'Esta es la página 1', 'Página 1', '', 'publish', 'closed', 'closed', '', 'pagina-1', '', '', '2019-03-11 15:41:57', '2019-03-11 21:41:57', '', 0, 'http://localhost/wordpress-starter/?page_id=6', 0, 'page', '', 0),
(8, 1, '2018-04-30 16:27:54', '2018-04-30 16:27:54', 'Lorem ipsum dolor sit amet, <a href=\"#\">consectetur</a> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\r\n[stumble]. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n<hr />\r\n\r\nButtons:\r\n<a class=\"button\" href=\"#\">.button</a>\r\n<a class=\"button alt\" href=\"#\">.button.alt</a>\r\n\r\nBootstrap\r\n<a class=\"btn btn-primary\" href=\"#\">.btn-primary</a> <a class=\"btn btn-default\" href=\"#\">.btn-default</a> <a class=\"btn btn-success\" href=\"#\">.btn-success</a> <a class=\"btn btn-warning\" href=\"#\">.btn-warning</a> <a class=\"btn btn-danger\" href=\"#\">.btn-danger</a> <a class=\"btn btn-light\" href=\"#\">.btn-light</a> <a class=\"btn btn-dark\" href=\"#\">.btn-dark</a>\r\n\r\n<hr />\r\n\r\nThe <a href=\"#\">a element</a> example\r\n\r\nThe <abbr>abbr element</abbr> and an <abbr title=\"Abbreviation\">abbr</abbr> element with title examples\r\n\r\nThe <b>b element</b> example\r\n\r\nThe <cite>cite element</cite> example\r\n\r\nThe <code>code element</code> example\r\n\r\nThe <em>em element</em> example\r\n\r\nThe <del>del element</del> example\r\n\r\nThe <dfn>dfn element</dfn> and <dfn title=\"Title text\">dfn element with title</dfn> examples\r\n\r\nThe <i>i element</i> example\r\n\r\nThe <ins>ins element</ins> example\r\n\r\nThe <kbd>kbd element</kbd> example\r\n\r\nThe <mark>mark element</mark> example\r\n\r\nThe <q>q element</q> example\r\n\r\nThe <q>q element <q>inside</q> a q element</q> example\r\n\r\nThe <s>s element</s> example\r\n\r\nThe <samp>samp element</samp> example\r\n\r\nThe <small>small element</small> example\r\n\r\nThe span element example\r\n\r\nThe <strong>strong element</strong> example\r\n\r\nThe <sub>sub element</sub> example\r\n\r\nThe <sup>sup element</sup> example\r\n\r\nThe <u>u element</u> example\r\n\r\nThe <var>var element</var> example\r\n\r\n<hr />\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<ul>\r\n<li>In voluptate velit esse cillum</li>\r\n<li>In voluptate velit esse cillum</li>\r\n<li>In voluptate velit esse cillum</li>\r\n</ul>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n<h3>Default Image</h3>\r\n<img src=\"http://placehold.it/240x240\" alt=\"Image alt text 3\" />\r\n<h3>Linked Image</h3>\r\n<a href=\"#\"><img src=\"http://placehold.it/240x240\" alt=\"Image alt text 4\" /></a>\r\n<h3>Missing Image</h3>\r\n<img alt=\"This is an example of a missing image\" />\r\n<h3>Picture</h3>\r\n<picture><source srcset=\"http://placehold.it/600x600\" media=\"(min-width: 60em)\" /></picture> <picture><source srcset=\"http://placehold.it/300x300\" media=\"(min-width: 40em)\" /></picture> <picture><source srcset=\"http://placehold.it/150x150\" /></picture> <picture> <img src=\"http://placehold.it/150x150\" alt=\"Image alt text 5\" /></picture>\r\n<h3>Svg</h3>\r\n&nbsp;\r\n<h3>Video</h3>\r\n<div class=\"sg-video\">\r\n\r\n<video id=\"video1\" poster=\"http://sandbox.thewikies.com/vfe-generator/images/big-buck-bunny_poster.jpg\" preload=\"\" controls=\"controls\" width=\"640\" height=\"360\"><source id=\"mp4\" src=\"http://media.w3.org/2010/05/bunny/trailer.mp4\" type=\"video/mp4\" /><source id=\"ogv\" src=\"http://media.w3.org/2010/05/bunny/trailer.ogv\" type=\"video/ogg\" />Your user agent does not support the HTML5 Video element.&nbsp;\r\n\r\n</video>\r\n<label class=\"sg-visually-hidden\" for=\"video1\">Video of Big Buck Bunny</label>\r\n\r\n</div>\r\n<h3>Missing Video</h3>\r\n<video id=\"video2\" controls=\"controls\" width=\"300\" height=\"150\"></video>\r\n<label class=\"sg-visually-hidden\" for=\"video2\">Missing video</label>\r\n<ul>\r\n<li>list item 1</li>\r\n<li>list item 1\r\n<ul>\r\n<li>list item 2</li>\r\n<li>list item 2\r\n<ul>\r\n<li>list item 3</li>\r\n<li>list item 3</li>\r\n</ul>\r\n</li>\r\n<li>list item 2</li>\r\n<li>list item 2</li>\r\n</ul>\r\n</li>\r\n<li>list item 1</li>\r\n<li>list item 1</li>\r\n</ul>\r\n<ol>\r\n<li>list item 1</li>\r\n<li>list item 1\r\n<ol>\r\n<li>list item 2</li>\r\n<li>list item 2\r\n<ol>\r\n<li>list item 3</li>\r\n<li>list item 3</li>\r\n</ol>\r\n</li>\r\n<li>list item 2</li>\r\n<li>list item 2</li>\r\n</ol>\r\n</li>\r\n<li>list item 1</li>\r\n<li>list item 1</li>\r\n</ol>\r\n<dl>\r\n<dt>Description name</dt>\r\n<dd>Description value</dd>\r\n<dt>Description name</dt>\r\n<dd>Description value</dd>\r\n<dd>Description value</dd>\r\n<dt>Description name</dt>\r\n<dt>Description name</dt>\r\n<dd>Description value</dd>\r\n</dl>\r\n\r\n<hr />\r\n\r\n<h1>Heading 1 with <small>small text</small> and a <a href=\"#\">link</a></h1>\r\n<h2>Heading 2 with <small>small text</small> and a <a href=\"#\">link</a></h2>\r\n<h3>Heading 3 with <small>small text</small> and a <a href=\"#\">link</a></h3>\r\n<h4>Heading 4 with <small>small text</small> and a <a href=\"#\">link</a></h4>\r\n<h5>Heading 5 with <small>small text</small> and a <a href=\"#\">link</a></h5>\r\n<h6>Heading 6 with <small>small text</small> and a <a href=\"#\">link</a></h6>\r\n\r\n<hr />\r\n\r\n<blockquote>Some sort of famous witty quote marked up with a &lt;blockquote&gt; and a child &lt;p&gt; element.</blockquote>\r\n<blockquote>Even better philosophical quote marked up with just a &lt;blockquote&gt; element.</blockquote>\r\n<address>Company Name, Inc.\r\n811 7th Ave, Suite 700\r\nManhattan, NY 10019\r\nUSA</address>', 'Contenido', '', 'publish', 'closed', 'closed', '', 'contenido', '', '', '2018-11-05 12:29:26', '2018-11-05 18:29:26', '', 0, 'http://localhost/wordpress-starter/?page_id=8', 0, 'page', '', 0),
(10, 1, '2018-04-30 16:29:06', '2018-04-30 16:29:06', ' ', '', '', 'publish', 'closed', 'closed', '', '10', '', '', '2018-11-05 12:27:12', '2018-11-05 18:27:12', '', 0, 'http://localhost/wordpress-starter/?p=10', 2, 'nav_menu_item', '', 0),
(11, 1, '2018-04-30 16:29:06', '2018-04-30 16:29:06', ' ', '', '', 'publish', 'closed', 'closed', '', '11', '', '', '2018-11-05 12:27:12', '2018-11-05 18:27:12', '', 0, 'http://localhost/wordpress-starter/?p=11', 3, 'nav_menu_item', '', 0),
(12, 1, '2018-04-30 16:29:05', '2018-04-30 16:29:05', ' ', '', '', 'publish', 'closed', 'closed', '', '12', '', '', '2018-11-05 12:27:12', '2018-11-05 18:27:12', '', 0, 'http://localhost/wordpress-starter/?p=12', 1, 'nav_menu_item', '', 0),
(14, 1, '2018-04-30 16:43:04', '2018-04-30 16:43:04', '', 'polylang_mo_3', '', 'private', 'closed', 'closed', '', 'polylang_mo_3', '', '', '2018-04-30 16:43:04', '2018-04-30 16:43:04', '', 0, 'http://localhost/wordpress-starter/?post_type=polylang_mo&p=14', 0, 'polylang_mo', '', 0),
(15, 1, '2018-04-30 16:43:25', '2018-04-30 16:43:25', '', 'polylang_mo_6', '', 'private', 'closed', 'closed', '', 'polylang_mo_6', '', '', '2018-04-30 16:43:25', '2018-04-30 16:43:25', '', 0, 'http://localhost/wordpress-starter/?post_type=polylang_mo&p=15', 0, 'polylang_mo', '', 0),
(16, 1, '2018-04-30 16:44:34', '2018-04-30 16:44:34', 'This is page 1 content', 'Page 1', '', 'publish', 'closed', 'closed', '', 'page-1', '', '', '2018-04-30 16:44:34', '2018-04-30 16:44:34', '', 0, 'http://localhost/wordpress-starter/?page_id=16', 0, 'page', '', 0),
(18, 1, '2018-04-30 16:45:25', '2018-04-30 16:45:25', '', 'Language switcher', '', 'publish', 'closed', 'closed', '', 'language-switcher', '', '', '2018-11-05 12:27:12', '2018-11-05 18:27:12', '', 0, 'http://localhost/wordpress-starter/?p=18', 4, 'nav_menu_item', '', 0),
(41, 1, '2019-03-11 15:29:51', '2019-03-11 21:29:51', '', 'limes', '', 'inherit', 'closed', 'closed', '', 'limes', '', '', '2019-03-11 15:29:51', '2019-03-11 21:29:51', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/limes.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2019-03-11 15:30:00', '2019-03-11 21:30:00', '', 'springrolls', '', 'inherit', 'closed', 'closed', '', 'springrolls', '', '', '2019-03-11 15:30:00', '2019-03-11 21:30:00', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/springrolls.jpg', 0, 'attachment', 'image/jpeg', 0),
(43, 1, '2019-03-11 15:30:09', '2019-03-11 21:30:09', '', 'eggs', '', 'inherit', 'closed', 'closed', '', 'eggs', '', '', '2019-03-11 15:30:09', '2019-03-11 21:30:09', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/eggs.jpg', 0, 'attachment', 'image/jpeg', 0),
(44, 1, '2019-03-11 15:30:17', '2019-03-11 21:30:17', '', 'lemons', '', 'inherit', 'closed', 'closed', '', 'lemons', '', '', '2019-03-11 15:30:17', '2019-03-11 21:30:17', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/lemons.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2019-03-11 15:30:27', '2019-03-11 21:30:27', '', 'jar', '', 'inherit', 'closed', 'closed', '', 'jar', '', '', '2019-03-11 15:30:27', '2019-03-11 21:30:27', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/jar.jpg', 0, 'attachment', 'image/jpeg', 0),
(46, 1, '2019-03-11 15:30:36', '2019-03-11 21:30:36', '', 'cake', '', 'inherit', 'closed', 'closed', '', 'cake', '', '', '2019-03-11 15:30:36', '2019-03-11 21:30:36', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/cake.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2019-03-11 15:30:45', '2019-03-11 21:30:45', '', 'cheese', '', 'inherit', 'closed', 'closed', '', 'cheese', '', '', '2019-03-11 15:30:45', '2019-03-11 21:30:45', '', 0, 'http://localhost/wordpress-starter/wp-content/uploads/2019/03/cheese.jpg', 0, 'attachment', 'image/jpeg', 0),
(2943, 1, '2019-06-16 19:34:30', '2019-06-17 01:34:30', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 1. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 1', '', 'publish', 'closed', 'open', '', 'vacante-1', '', '', '2019-06-16 19:34:30', '2019-06-17 01:34:30', '', 0, 'https://edica.co.cr/nuevo/?p=2943', 0, 'post', '', 0),
(2945, 1, '2019-06-16 19:36:29', '2019-06-17 01:36:29', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 2. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 2', '', 'publish', 'closed', 'open', '', 'vacante-2', '', '', '2019-06-16 19:36:29', '2019-06-17 01:36:29', '', 0, 'https://edica.co.cr/nuevo/?p=2945', 0, 'post', '', 0),
(2946, 1, '2019-06-16 19:37:35', '2019-06-17 01:37:35', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 3. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 3', '', 'publish', 'closed', 'open', '', 'vacante-3', '', '', '2019-06-16 19:37:35', '2019-06-17 01:37:35', '', 0, 'https://edica.co.cr/nuevo/?p=2946', 0, 'post', '', 0),
(2947, 1, '2019-06-16 19:38:57', '2019-06-17 01:38:57', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 4. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 4', '', 'publish', 'closed', 'open', '', 'vacante-4', '', '', '2019-06-16 19:38:57', '2019-06-17 01:38:57', '', 0, 'https://edica.co.cr/nuevo/?p=2947', 0, 'post', '', 0),
(2948, 1, '2019-06-16 19:39:15', '2019-06-17 01:39:15', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 5. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 5', '', 'publish', 'closed', 'open', '', 'vacante-5', '', '', '2019-06-16 19:39:15', '2019-06-17 01:39:15', '', 0, 'https://edica.co.cr/nuevo/?p=2948', 0, 'post', '', 0),
(2949, 1, '2019-06-16 19:39:27', '2019-06-17 01:39:27', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 6. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 6', '', 'publish', 'closed', 'open', '', 'vacante-6', '', '', '2019-06-16 19:39:27', '2019-06-17 01:39:27', '', 0, 'https://edica.co.cr/nuevo/?p=2949', 0, 'post', '', 0),
(2950, 1, '2019-06-16 19:39:46', '2019-06-17 01:39:46', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit 7. Mauris diam eros, dapibus eget turpis et, tempor laoreet ex.', 'Vacante 7', '', 'publish', 'closed', 'open', '', 'vacante-7', '', '', '2019-06-16 19:39:46', '2019-06-17 01:39:46', '', 0, 'https://edica.co.cr/nuevo/?p=2950', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_po_plugins`
--

DROP TABLE IF EXISTS `baum_po_plugins`;
CREATE TABLE IF NOT EXISTS `baum_po_plugins` (
  `pl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `permalink` longtext NOT NULL,
  `permalink_hash` varchar(32) NOT NULL DEFAULT '',
  `permalink_hash_args` varchar(32) NOT NULL DEFAULT '',
  `post_type` varchar(50) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT 'publish',
  `secure` int(1) NOT NULL DEFAULT '0',
  `children` int(1) NOT NULL DEFAULT '0',
  `pt_override` int(1) NOT NULL DEFAULT '0',
  `disabled_plugins` longtext NOT NULL,
  `enabled_plugins` longtext NOT NULL,
  `disabled_mobile_plugins` longtext NOT NULL,
  `enabled_mobile_plugins` longtext NOT NULL,
  `disabled_groups` longtext NOT NULL,
  `enabled_groups` longtext NOT NULL,
  `disabled_mobile_groups` longtext NOT NULL,
  `enabled_mobile_groups` longtext NOT NULL,
  `post_priority` int(3) NOT NULL DEFAULT '0',
  `dir_count` int(3) NOT NULL DEFAULT '0',
  `user_role` varchar(100) NOT NULL DEFAULT '_',
  PRIMARY KEY (`pl_id`),
  KEY `PO_post_id` (`post_id`),
  KEY `PO_permalink_idx` (`permalink_hash`,`status`,`secure`,`post_type`,`user_role`),
  KEY `PO_permalink_args_idx` (`permalink_hash_args`,`status`,`secure`,`post_type`,`user_role`),
  KEY `PO_page_lists` (`post_id`,`post_type`,`user_role`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_realmedialibrary`
--

DROP TABLE IF EXISTS `baum_realmedialibrary`;
CREATE TABLE IF NOT EXISTS `baum_realmedialibrary` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `parent` mediumint(9) NOT NULL DEFAULT '-1',
  `name` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `absolute` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `owner` bigint(20) NOT NULL,
  `ord` mediumint(10) NOT NULL DEFAULT '0',
  `contentCustomOrder` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `restrictions` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cnt` mediumint(10) DEFAULT NULL,
  `importId` bigint(20) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_realmedialibrary_debug`
--

DROP TABLE IF EXISTS `baum_realmedialibrary_debug`;
CREATE TABLE IF NOT EXISTS `baum_realmedialibrary_debug` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_520_ci,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_realmedialibrary_meta`
--

DROP TABLE IF EXISTS `baum_realmedialibrary_meta`;
CREATE TABLE IF NOT EXISTS `baum_realmedialibrary_meta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `realmedialibrary_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `realmedialibrary_id` (`realmedialibrary_id`),
  KEY `meta_key` (`meta_key`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_realmedialibrary_posts`
--

DROP TABLE IF EXISTS `baum_realmedialibrary_posts`;
CREATE TABLE IF NOT EXISTS `baum_realmedialibrary_posts` (
  `attachment` bigint(20) NOT NULL,
  `fid` mediumint(9) NOT NULL DEFAULT '-1',
  `isShortcut` bigint(20) NOT NULL DEFAULT '0',
  `nr` bigint(20) DEFAULT NULL,
  `oldCustomNr` bigint(20) DEFAULT NULL,
  `importData` text COLLATE utf8mb4_unicode_520_ci,
  UNIQUE KEY `rmlorder` (`attachment`,`isShortcut`),
  KEY `rmljoin` (`attachment`,`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_realmedialibrary_posts`
--

INSERT INTO `baum_realmedialibrary_posts` (`attachment`, `fid`, `isShortcut`, `nr`, `oldCustomNr`, `importData`) VALUES
(41, -1, 0, NULL, NULL, NULL),
(42, -1, 0, NULL, NULL, NULL),
(43, -1, 0, NULL, NULL, NULL),
(44, -1, 0, NULL, NULL, NULL),
(45, -1, 0, NULL, NULL, NULL),
(46, -1, 0, NULL, NULL, NULL),
(47, -1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_smush_dir_images`
--

DROP TABLE IF EXISTS `baum_smush_dir_images`;
CREATE TABLE IF NOT EXISTS `baum_smush_dir_images` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `path` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `path_hash` char(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `resize` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lossy` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `error` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `image_size` int(10) UNSIGNED DEFAULT NULL,
  `orig_size` int(10) UNSIGNED DEFAULT NULL,
  `file_time` int(10) UNSIGNED DEFAULT NULL,
  `last_scan` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `meta` text COLLATE utf8mb4_unicode_520_ci,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `path_hash` (`path_hash`),
  KEY `image_size` (`image_size`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_termmeta`
--

DROP TABLE IF EXISTS `baum_termmeta`;
CREATE TABLE IF NOT EXISTS `baum_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_terms`
--

DROP TABLE IF EXISTS `baum_terms`;
CREATE TABLE IF NOT EXISTS `baum_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_terms`
--

INSERT INTO `baum_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sin categoría', 'sin-categoria', 0),
(2, 'Menú Principal', 'menu-principal', 0),
(3, 'Español', 'es', 0),
(4, 'Español', 'pll_es', 0),
(5, 'pll_5ae74798413d7', 'pll_5ae74798413d7', 0),
(6, 'English', 'en', 1),
(7, 'English', 'pll_en', 0),
(8, 'No Category', 'no-category', 0),
(10, 'pll_5ae747f2a52e6', 'pll_5ae747f2a52e6', 0),
(11, 'Empleo', 'empleo', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_term_relationships`
--

DROP TABLE IF EXISTS `baum_term_relationships`;
CREATE TABLE IF NOT EXISTS `baum_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_term_relationships`
--

INSERT INTO `baum_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 2, 0),
(11, 2, 0),
(10, 2, 0),
(1, 4, 0),
(1, 5, 0),
(8, 7, 0),
(8, 3, 0),
(8, 5, 0),
(6, 3, 0),
(4, 3, 0),
(1, 3, 0),
(2, 3, 0),
(16, 6, 0),
(16, 10, 0),
(6, 10, 0),
(18, 2, 0),
(27, 3, 0),
(35, 3, 0),
(56, 3, 0),
(2943, 11, 0),
(2945, 11, 0),
(2946, 11, 0),
(2947, 11, 0),
(2948, 11, 0),
(2949, 11, 0),
(2950, 11, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_term_taxonomy`
--

DROP TABLE IF EXISTS `baum_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `baum_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_term_taxonomy`
--

INSERT INTO `baum_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'language', 'a:3:{s:6:\"locale\";s:5:\"es_CR\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"cr\";}', 0, 5),
(4, 4, 'term_language', '', 0, 1),
(5, 5, 'term_translations', 'a:2:{s:2:\"es\";i:1;s:2:\"en\";i:8;}', 0, 2),
(6, 6, 'language', 'a:3:{s:6:\"locale\";s:5:\"en_US\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"us\";}', 0, 1),
(7, 7, 'term_language', '', 0, 1),
(8, 8, 'category', '', 0, 0),
(10, 10, 'post_translations', 'a:2:{s:2:\"en\";i:16;s:2:\"es\";i:6;}', 0, 2),
(11, 11, 'category', '', 0, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_tm_taskmeta`
--

DROP TABLE IF EXISTS `baum_tm_taskmeta`;
CREATE TABLE IF NOT EXISTS `baum_tm_taskmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `meta_key` (`meta_key`(191)),
  KEY `task_id` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_tm_tasks`
--

DROP TABLE IF EXISTS `baum_tm_tasks`;
CREATE TABLE IF NOT EXISTS `baum_tm_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `type` varchar(300) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `class_identifier` varchar(300) COLLATE utf8mb4_unicode_520_ci DEFAULT '0',
  `attempts` int(11) DEFAULT '0',
  `description` varchar(300) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_locked_at` bigint(20) DEFAULT '0',
  `status` varchar(300) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_usermeta`
--

DROP TABLE IF EXISTS `baum_usermeta`;
CREATE TABLE IF NOT EXISTS `baum_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_usermeta`
--

INSERT INTO `baum_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin_wpstarter'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'baum_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'baum_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'pll_lgt,wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(48, 1, 'session_tokens', 'a:2:{s:64:\"ad862d2c6340f66f84f0d44841909e563d87dbdf7843f91c7e1c9f4103421597\";a:4:{s:10:\"expiration\";i:1565194977;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565022177;}s:64:\"358a7ded558394e46f4816f5397167775dbcb49b638e26c4bc266d238dd6b4cc\";a:4:{s:10:\"expiration\";i:1565195181;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565022381;}}'),
(17, 1, 'baum_dashboard_quick_press_last_post_id', '56'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, 'chld_thm_cfg_upgrade_notice', '2.2.9'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:11:\"css-classes\";i:3;s:3:\"xfn\";i:4;s:11:\"description\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:21:\"add-post-type-project\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:20:\"add-project_category\";i:4;s:15:\"add-project_tag\";}'),
(22, 1, 'nav_menu_recently_edited', '2'),
(23, 1, 'pll_filter_content', ''),
(24, 1, 'closedpostboxes_page', 'a:0:{}'),
(25, 1, 'metaboxhidden_page', 'a:5:{i:0;s:12:\"et_pb_layout\";i:1;s:16:\"commentstatusdiv\";i:2;s:11:\"commentsdiv\";i:3;s:7:\"slugdiv\";i:4;s:9:\"authordiv\";}'),
(37, 1, 'baum_user-settings', 'editor=tinymce&libraryContent=browse'),
(38, 1, 'baum_user-settings-time', '1565027186'),
(49, 1, 'description_en', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baum_users`
--

DROP TABLE IF EXISTS `baum_users`;
CREATE TABLE IF NOT EXISTS `baum_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `baum_users`
--

INSERT INTO `baum_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin_wpstarter', '$P$BuCJodd2Ojk8q4mgpLA07lMrIFpqwI/', 'admin_wpstarter', 'keylor@baum.digital', '', '2018-04-30 14:55:36', '', 0, 'admin_wpstarter');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
