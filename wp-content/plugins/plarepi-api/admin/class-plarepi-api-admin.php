<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://baum.digital
 * @since      1.0.0
 *
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/admin
 * @author     River Martínez <river@baum.digital>
 */
class Plarepi_Api_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $base_url = 'https://api.plarepi.com/';
	private $x_api_key = 'tSXTsQ2BG73nTNgb1G7X35pI2ykDpHqtzYMPwLub';
	private $token = 'tSXTsQ2BG73nTNgb1G7X35pI2ykDpHqtzYMPwLub';
	public $name = '';
	public $lastname = '';
	public $credits = 0;
	public $email = '';
	public $password = '';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Send request to Plarepi APP after the order is marked how complete in WooCommerce
	 *
	 * @see Plarepi_Api, \Baum\Plarepi\PlarepiGuzzleHttpClient
	 * @param int $order_id Order id of WooCommerce
	 */
	public function send_user_data_to_plarepi( $order_id ) {
		$order    = wc_get_order( $order_id );
		$credits  = $this->get_total_credits( $order );
		$customer = $this->get_customer_data( $order );
		$is_previous_user = $this->user_has_previous_purchase( $customer );

		// Set data to json body
		$this->name     = $customer['first_name'];
		$this->lastname = $customer['last_name'];
		$this->credits  = $credits;
		$this->email    = $customer['email'];
		$this->password = $customer['password'];
		$body           = $this->formatBody();

		if ( $is_previous_user ) {
			$response = $this->request( 'usuarios', $body, 'PUT' );
		} else {
			$response = $this->request( 'usuarios', $body, 'POST' );
		}

		if ( $response ) {
			delete_transient( 'p_api_user_' . $customer['user_login'] );
		}
	}

	// Make formatted request
	private function request( $path, $body, $method = 'GET' ) {
		$args = [
			'headers' => [
				'x-api-key' => $this->x_api_key,
				'token'     => $this->token,
				'Content-Type'   => 'application/json'
			],
			'timeout' => 60
		];

		if ( $method ) {
			$args['method'] = $method;
		}

		if ( $body ) {
			$args['body'] = $body;
		}

		$response = wp_remote_request( $this->base_url . $path, $args );

		// Check for success
		if ( ! is_wp_error( $response ) && ( $response['response']['code'] == 200 || $response['response']['code'] == 201 ) ) {
			return $response['body'];
		}

		return false;
	}

	private function formatBody() {
		$body = json_encode( [
			'nombre'    => $this->name,
			'apellidos' => $this->lastname,
			'creditos'  => $this->credits,
			'email'     => $this->email,
			'password'  => $this->password
		] );

		return $body;
	}

	/**
	 * Calculates the total credits of the products in the order.
	 *
	 * @param $order
	 *
	 * @return float|int
	 */
	private function get_total_credits( $order ) {
		$credits = 0;

		// Loop all products items
		foreach ( $order->get_items() as $item_key => $item ) {
			$item_data_qty = (int) $item->get_quantity();
			$variation_id  = $item->get_variation_id();
			$variations    = wc_get_product_variation_attributes( $variation_id );
			// $item_data     = $item->get_data();
			// $product       = $item->get_product();

			if ( isset( $variations['attribute_pa_credito'] ) ) {
				$variation_value = (int) $variations['attribute_pa_credito'];
				$credits         += $item_data_qty * $variation_value;
			}
		}

		return $credits;
	}

	/**
	 * Get all user data of the order
	 *
	 * @param $order
	 *
	 * @return array
	 */
	private function get_customer_data( $order ) {
		$customer_data = [];
		$user          = $order->get_user();
		$user_pass     = $this->get_user_password_transient( $user->user_login );

		$customer_data['user_id']    = $user->id;
		$customer_data['user_login'] = $user->user_login;
		$customer_data['first_name'] = empty( $order->get_billing_first_name() ) ? $user->first_name : $order->get_billing_first_name();
		$customer_data['last_name']  = empty( $order->get_billing_last_name() ) ? $user->last_name : $order->get_billing_last_name();
		$customer_data['email']      = empty( $order->get_billing_email() ) ? $user->user_email : $order->get_billing_email();
		$customer_data['password']   = $user_pass ? $user_pass : '';

		return $customer_data;
	}

	/**
	 * Set trasient when new user a created in Woocommerce
	 *
	 * @param $args
	 */
	public function set_user_password_transient( $args ) {
		if ( isset ( $args['user_login'] ) && isset( $args['user_pass'] ) ) {
			set_transient( 'p_api_user_' . $args['user_login'], $args['user_pass'], DAY_IN_SECONDS );
		}
	}

	/**
	 * Get password for new user from transient
	 * @param $user_login
	 *
	 * @return mixed
	 */
	private function get_user_password_transient( $user_login ) {
		return get_transient( 'p_api_user_' . $user_login );
	}

	/**
	 * Validate if user has previous purchase "Complete", if is True the customer have at least one purchase in state
	 * complete, if it is False the user may be registered on WooCommerce but not in Plarepi APP and has not made any purchase.
	 *
	 * @param array $customer Array data of Customer.
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function user_has_previous_purchase( $customer ) {
		$bought = false;

		$args = [
			'limit'       => 2, // limit two result
			'return'      => 'ids', // return only ids, default is Order object
			'customer_id' => $customer['user_id'],
			'status'      => 'completed'
		];

		$query  = new WC_Order_Query( $args );
		$orders = $query->get_orders();

		// Validate if order is greater than one because if it is a single purchase that is changing status it is the
		// first time that data will be sent to the Plarepi APP
		if ( count( $orders ) > 1 ) {
			$bought = true; //customer has more than one order
		}

		return $bought;
	}
}
