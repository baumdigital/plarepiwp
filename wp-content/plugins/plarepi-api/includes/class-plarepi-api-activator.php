<?php

/**
 * Fired during plugin activation
 *
 * @link       https://baum.digital
 * @since      1.0.0
 *
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/includes
 * @author     River Martínez <river@baum.digital>
 */
class Plarepi_Api_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
