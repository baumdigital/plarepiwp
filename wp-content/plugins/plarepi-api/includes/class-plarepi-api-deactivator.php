<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://baum.digital
 * @since      1.0.0
 *
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Plarepi_Api
 * @subpackage Plarepi_Api/includes
 * @author     River Martínez <river@baum.digital>
 */
class Plarepi_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
