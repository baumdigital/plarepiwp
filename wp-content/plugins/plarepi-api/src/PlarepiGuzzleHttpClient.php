<?php
namespace Baum\Plarepi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;

class PlarepiGuzzleHttpClient {
	private $client = null;
	private $base_url = 'https://api.plarepi.com/';
	private $x_api_key = 'tSXTsQ2BG73nTNgb1G7X35pI2ykDpHqtzYMPwLub';
	private $token = 'tSXTsQ2BG73nTNgb1G7X35pI2ykDpHqtzYMPwLub';
	public $name = '';
	public $lastname = '';
	public $credits = 0;
	public $email = '';
	public $password = '';

	/**
	 * PlarepiGuzzleHttpClient constructor.
	 *
	 * @param string $name
	 * @param string $lastname
	 * @param int $credits
	 * @param string $email
	 * @param string $password
	 */
	public function __construct( $name, $lastname, $credits, $email, $password ) {
		// Monolog
		$stack = HandlerStack::create();
		$stack->push( $this->getLoggerConfig() );

		$this->name = $name;
		$this->lastname = $lastname;
		$this->credits = $credits;
		$this->email = $email;
		$this->password = $password;

		$this->client = new Client( [
			'base_uri' => $this->base_url,
			'handler'  => $stack,
		] );
	}

	public function request( $method, $path_url, array $options = [] ) {
		$body    = $this->formatBody();
		$options = $this->formatHeaders( $options, $body );
		$user_login = isset( $options['user_login'] ) ? $options['user_login'] : '';

		try {

			$response = $this->client->request( $method, $path_url, $options );

			if ( $response->getStatusCode() === 200 ) {
				$this->writeLoggerResponse( (string) $response->getBody(), 'info' );
				delete_transient( 'p_api_user_' . $user_login );
			}

		} catch ( RequestException $e ) {

			// echo 'Exception ' . Psr7\str( $e->getRequest() );
			$this->writeLoggerResponse( 'Exception for request: ' . (string) Psr7\str( $e->getRequest() ), 'error' );

			if ( $e->hasResponse() ) {
				// echo 'Exception ' . Psr7\str( $e->getResponse() );
				$this->writeLoggerResponse( 'Exception response: ' . (string) Psr7\str( $e->getResponse() ), 'error' );
			}

		}
	}

	private function formatBody() {
		$body = json_encode( [
			'nombre'    => $this->name,
			'apellidos' => $this->lastname,
			'creditos'  => $this->credits,
			'email'     => $this->email,
			'password'  => $this->password
		] );

		return $body;
	}

	private function formatHeaders( $options, $body ) {
		if ( $body ) {
			$options['body'] = $body;
		}

		if ( ! isset( $options['x-api-key'] ) ) {
			$options['headers']['x-api-key'] = $this->x_api_key;
		}

		if ( ! isset( $options['token'] ) ) {
			$options['headers']['token'] = $this->token;
		}

		if ( ! isset( $options['Content-Type'] ) ) {
			$options['headers']['Content-Type'] = 'application/json';
		}

		return $options;
	}

	private function getLoggerConfig() {
		return Middleware::log(
			new Logger( 'PlarepiGuzzle', [
				new StreamHandler( __DIR__ . '/../logs/' . date( 'Ymd' ). '.log', Logger::DEBUG ),
				new FirePHPHandler()
			] ),
			new MessageFormatter( '{req_body} - {res_body}' )
		);
	}

	private function writeLoggerResponse( $response, $type ) {
		$logger = new Logger( 'PlarepiAPPResponse' );
		$logger->pushHandler( new StreamHandler( __DIR__ . '/../logs/' . date( 'Ymd' ) . '.log', Logger::DEBUG ) );

		if ( $type === 'info' ) {
			$logger->info( $response );
		}

		if ( $type === 'error' ) {
			$logger->error( $response);
		}
	}
}