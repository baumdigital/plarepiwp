(function($) {
    /**
     ** Custom checked checkboxes
     **/
    $(document).on('click', 'input[type="checkbox"]', function () {
        if ($(this).is(':checked')) {
            $(this).parent('label[class*="checkbox"]').addClass('checked');
        } else {
            $(this).parent('label[class*="checkbox"]').removeClass('checked');
        }
    });

    /**
     ** Allow only numbers in input type text
     */
    numbersOnly('.input-container input.input-text');

    function numbersOnly(s) {
        $(s).on('keydown', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    }
    
    /**
    ** Ajax login on Checkout
    **/
    $('#woo-form-login input').focus(function () {
        $('form#woo-form-login .alert').fadeOut(1000);
    });

    // Perform AJAX login on form submit
    $('form#woo-form-login').on('submit', function (e) {
        $('form#woo-form-login .alert').addClass('alert-info').fadeIn(1000);
        $('form#woo-form-login .alert p').html(baumchild_ajax.loadingmessage + ' <i class="fas fa-circle-notch fa-spin"></i>');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baumchild_ajax.ajax_url,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#woo-form-login #username').val(),
                'password': $('form#woo-form-login #password').val(),
                'security': $('form#woo-form-login #security').val()
            },
            beforeSend: function () {
                $('form#woo-form-login .alert').removeClass('alert-danger alert-success');
            },
            success: function (data) {
                if (data.loggedin == true) {
                    $('form#woo-form-login .alert').removeClass('alert-info').addClass('alert-success');
                    document.location.href = baumchild_ajax.redirecturl;
                } else {
                    $('form#woo-form-login .alert').removeClass('alert-info').addClass('alert-danger');
                }

                $('form#woo-form-login .alert p').text(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('form#woo-form-login .alert').addClass('alert-danger');
                $('form#woo-form-login .alert p').text('Un error ha ocurrido, por favor inténtelo nuevamente.');
                console.log('jqXHR: ' + jqXHR.responseText);
                console.log('textStatus: ' + textStatus);
                console.log('error: ' + errorThrown);
            }
        });

        e.preventDefault();
    });
    
    /**
    ** Update cart in review order table in Checkout
    **/
    $('.woocommerce-checkout').on('change', 'input.qty', function() {
        var _this = $(this);
        var data = {
            action: 'baumchild_ajax_update_to_cart',
            cart_key: _this.closest('tr').data('cart_item_key'),
            product_sku: _this.closest('tr').find('.product-sku').text(),
            quantity: _this.val(),
        };

        if (!$('#order_review').find('.shop_table.bm-loader').length) {
            ajax_update_cart(data, false);
        }
    });

    $(document).on('click', '.remove-product', function() {
        var _this = $(this);
        var data = {
            action: 'baumchild_ajax_update_to_cart',
            cart_key: _this.closest('tr').data('cart_item_key'),
            quantity: 0,
        };
        ajax_update_cart(data, true);

        return false;
    });

    function ajax_update_cart(data, update_checkout) {
        $('#order_review').find('.shop_table').addClass('bm-loader');
        $.post(wc_add_to_cart_params.ajax_url, data)
        .done(function (response) {
            if (response.error & response.product_url) {
                window.location = response.product_url;
                return;
            } else {
                $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
            }

            $(document.body).trigger('update_cart_from_checkout');

            if(update_checkout) {
                setTimeout(function() {
                    $('body').trigger('update_checkout');
                }, 250);
            }
        })
        .fail(function (error) {
            console.log(JSON.stringify(error));
        })
        .always(function() {
            $('#order_review').find('.shop_table').removeClass('bm-loader');
        });
    }

	/**
	** Add to Cart loader
	** Removed, now it is using the loader from Added to Cart popup WC plugin
	** 28-11-2019 - KMA
	**/

	/**
	** Toggle checkout actions
	**/
	$('#woocommerce-form-login-toggle-container a').click(function() {
		var parent = $(this).parent().parent();
		remove_coupon_message(0);

		$('[class*="woocommerce-form-"]').not(parent).removeClass('open');

		if(parent.hasClass('open')) {
			parent.removeClass('open');
		} else {
			parent.addClass('open');
		}
		
		if(parent.hasClass('open')) {
			$('body').addClass('overlay');
		} else {
			$('body').removeClass('overlay');
		}
	});
	
	/**
	** Toggle overlay
	**/
	$(document).on('click', 'body.overlay', function(e) {
		if(!$(e.target).closest('.open + form').length) {
			remove_checkout_info_overlay();
		}
	});

	$(document).on('click', '.woocommerce-checkout-notifications .close', function(e) {
		remove_checkout_info_overlay();
	});
	
	function remove_checkout_info_overlay() {
		$('body').removeClass('overlay');
		$('[class*="woocommerce-form-"]').removeClass('open');
	}

	function remove_coupon_message(time) {
		// var elements = '[class*="woocommerce-form"] .woocommerce-error, .woocommerce-checkout .woocommerce-message';
		var elements = '.woocommerce-checkout-notifications .woocommerce-error, .woocommerce-checkout .woocommerce-message';
		setTimeout(function() {
			$(elements).fadeOut('slow', function() {
				setTimeout(function() {
					$(elements).remove();
				}, 60);
			});
		}, time);
	}

	/**
	** Habilitar el boton en formulario cupones
	**/
	$(document).on('change keyup keypress blur', '#coupon_code', function() {
		if($(this).val() != "") {
			$(this).parent().find('.button').removeAttr('disabled');
		} else {
			$(this).parent().find('.button').attr('disabled', 'disabled');
		}
	});

	/**
	** Activar check para crear cuenta en checkout
	**/
	$("#account_password, #account_confirm_password").on("blur", function() {
		var _account = $("#account_password");
		var _account_conf = $("#account_confirm_password");

		if(_account.val() != "" || _account_conf.val() != "") {
			$("#createaccount").prop("checked", true);
		} else {
			$("#createaccount").prop("checked", false);
		}
	});

	/**
	** jQuery checkout fields validation
	**/
	$(document).on('change', '#payment_methods', function() {
		$('.payment_box').not('.payment_method_' + $(this).val()).slideUp();
		$('.payment_box.payment_method_' + $(this).val()).slideDown();
	});

		// placeholder: '8888-8888'
	$("#billing_phone, #billing_celular").mask("0000-0000");

	$('#account_email, #billing_email').blur(function() {
		$(this).val( $(this).val().split(' ').join('') );
	});

	/**
	** Shipping calculator as dropdown
	**/
	$(document).on("change", ".shipping_methods", function () {
		var _this = $(this).val();
		if (_this == "calculate_shipping") {
			$(".shipping-calculator-button").trigger("click");
		} else {
			if ($(".shipping-calculator-form").is(':visible')) {
				$(".shipping-calculator-button").trigger("click");
			}
			$('select.shipping_method option[value="' + _this + '"]').prop('selected', true);
			$('select.shipping_method').trigger("change");
		}
	});

	/**
	** After update checkout
	**/
	$(document.body).on('update_checkout, updated_checkout', function () {
		validate_checkout_required_fields();
		remove_checkout_info_overlay();
		remove_coupon_message(1500);
	});

	function validate_checkout_required_fields() {
		if(jQuery('select.shipping_method').val() == "wc_pickup_store") {
			$('#billing_state_field, #billing_city_field, #billing_address_1_field').find(".required").hide();
			$('#billing_state_field, #billing_city_field, #billing_address_1_field').removeClass("validate-required woocommerce-validated woocommerce-invalid woocommerce-invalid-required-field");
		} else {
			if($('#billing_state_field .required, #billing_city_field .required, #billing_address_1_field .required').length < 1) {
				$('#billing_state_field label, #billing_city_field label, #billing_address_1_field label').append('<abbr class="required" title="obligatorio">*</abbr>');
			} else {
				$('#billing_state_field, #billing_city_field, #billing_address_1_field').find(".required").show();
			}
			$('#billing_state_field, #billing_city_field, #billing_address_1_field').addClass("validate-required");
		}
	}
	
	/**
	** Input quantity
	**/
	$(window).on('load', function() {		
		if($('.woocommerce-cart-form input.qty').length) {
			check_input_qty();
		}

		if($('.single-product').length) {
			check_stock($(".quantity input.qty"));
			check_max_attr($('.single-product input.qty'));
		}
	});

	$(document).on('click', '.quantity-up', function (e) {
		input_qty = $(this).parents('.input-container').find('.qty');

		if (check_stock(input_qty)) {
			input_qty.val(function (i, cur_val) {
				return ++cur_val;
			});

			input_qty.change();
		}

		return false;
	});

	$(document).on('click', '.quantity-down', function (e) {
		input_qty = $(this).parents('.input-container').find('.qty');
		input_qty.val(function (i, cur_val) {
			return cur_val > 1 ? --cur_val : 0;
		});

		input_qty.change();

		return false;
	});

	$(document).on('focus', 'input.qty', function() {
		$(this).select();
	});

	$(document).on('change blur keyup', 'input.qty', function() {
		var _this = $(this);
		setTimeout(function() {
			check_stock(_this);
		}, 50);
	});

	/* Update order-review table */
	$(document).on('update_cart_from_checkout', function() {
		if($('#order_review tr.cart_item input.qty').length) {
			$('#order_review tr.cart_item').each(function() {
				setTimeout(function() {
					check_stock($(this).find('input.qty'));
				}, 50);
			});
		}
	});

	function enable_qty_input(pos, input) {
		input.parent().find('.quantity-' + pos).removeClass('quantity-disabled');
	}

	function disable_qty_input(pos, input) {
		input.parent().find('.quantity-' + pos).addClass('quantity-disabled');
	}

	function check_stock(_this) {
		var val = parseFloat(_this.val());
		var max_stock = parseFloat(_this.attr('max'));
		var min_stock = (_this.attr('min') > 0) ? parseFloat(_this.attr('min')) : 1;
		var current = parseFloat(_this.data('qty'));

		if(val <= max_stock || val > min_stock) {
			enable_qty_input('down', _this);
			enable_qty_input('up', _this);
		}		

		if(val == max_stock || isNaN(max_stock)) {
			disable_qty_input('up', _this);
		}

		if(val == 1) {
			disable_qty_input('down', _this);
		} else if(val > max_stock) {
			_this.val(min_stock);
			_this.trigger('change');
			_this.select();
		}

		return true;
	}

	/**
	** Update cart amount
	**/
	$(document.body).on('updated_cart_totals', function () {
		check_input_qty();
	});
	check_input_qty();

	$(document).on("woocommerce_variation_select_change", ".variations_form", function () {
		setTimeout(function() {
			check_stock($(".quantity input.qty"));
		}, 80);
	});

	$(document).on('success_modal', function () {
		check_stock($(".xoo-qv-summary .quantity input.qty"));
	});

	function check_input_qty() {
		$(".woocommerce-cart-form input.qty").each(function () {
			var _this = $(this);

			_this.data("qty", _this.val());
			check_stock(_this);
			check_max_attr(_this);
		});
	}

	function check_max_attr(_this) {
		if(_this.attr('max') == "") {
			_this.attr('max', _this.val());
		}
	}
	
}(jQuery));
