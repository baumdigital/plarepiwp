        <?php
        /**
         * The template for displaying the footer
         */
        ?>
        </div><!-- .site-content -->

        <footer class="site-footer">
            <div class="site-footer-bottom">
                <div class="container">
                    <div class="site-footer-1">
                        <?php if (is_active_sidebar('sidebar-footer-1')): ?>
                            <div class="widget-footer-container">
                                <?php dynamic_sidebar('sidebar-footer-1'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="site-footer-2">
                        <?php if (is_active_sidebar('sidebar-footer-2')): ?>
                            <div class="widget-footer-container">
                                <?php dynamic_sidebar('sidebar-footer-2'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="site-footer-copy">
                <div class="align-items-center container d-flex flex-md-row flex-sm-column justify-content-md-between justify-content-sm-center">
                <!-- <div class="container"> -->
                    <div class="copy">
                        <?php if (is_active_sidebar('sidebar-footer-copy')): ?>
                            <div class="widget-footer-container">
                                <?php dynamic_sidebar('sidebar-footer-copy'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php do_action('baum_logo_branding'); ?>
                </div>
            </div>
        </footer><!-- .site-footer -->
        </div><!-- .site -->

        <?php wp_footer(); ?>
</body>
</html>
