<?php

/**
** Declare child theme from twentynineteen
**/
function baumchild_child_enqueue_styles() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}
// add_action('wp_enqueue_scripts', 'baumchild_child_enqueue_styles');

/**
** Theme directory URI
**/
function baumchild_dir_uri() {
    return get_stylesheet_directory_uri();
}

/**
** Custom functions
**/
require get_stylesheet_directory() . '/functions/includes.php';

/**
** Manage menus
** Tweentynineteen menus added
**/
function baumchild_manage_menu_locations() {
    add_theme_support('custom-logo', array(
        'height' => '',
        'width' => '',
        'flex-width' => true,
        'flex-height' => true,
    ));

    $menus = array('menu-1', 'primary', 'footer', 'social');

    foreach ($menus as $key => $menu)
    {
        unregister_nav_menu($menu);
    }

    register_nav_menus(array(
        'primary' => __('Menú Principal', 'baumchild'),
        'secondary' => __('Menú Secundario', 'baumchild'),
    ));
}
add_action('after_setup_theme', 'baumchild_manage_menu_locations', 20);

/**
** Enqueue and dequeue CSS and JS
** Dependencias wpdreams-asp-basic y wpdreams-asp-chosen eliminadas desde ASP 4.17.5
**/
function baumchild_enqueue_styles() {
    $theme_path = get_stylesheet_directory_uri();
    $min = !is_baum_localhost() ? '.min' : '';

    $arrays_css = array('select2', 'bootstrap_css', 'font_awesome');
    $arrays_js = array('jquery', 'bootstrap_js', 'select2', 'jquery_mask');

    wp_enqueue_style('bootstrap_css', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    wp_enqueue_style('font_awesome', '//use.fontawesome.com/releases/v5.6.3/css/all.css');
    wp_enqueue_style('baumchild-fonts', baumchild_fonts_url(), $arrays_css, null);
    
    wp_register_script('jquery_mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js', array('jquery'));
    wp_enqueue_script('bootstrap_js', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js');
    wp_enqueue_script('baumchild-script', baumchild_dir_uri() . '/assets/js/all' . $min . '.js', $arrays_js, null, true);
    wp_enqueue_style('baumchild-style', baumchild_dir_uri() . '/assets/css/theme' . $min . '.css', array(), null);
    // wp_enqueue_script('baumchild-plugins', baumchild_dir_uri() . '/assets/js/plugin' . $min . '.js', array('jquery'), null, true);

    /* Adding inline styles */
    wp_add_inline_style('baumchild-style', baumchild_print_inline_css());

    // Declare it to use ajax
    wp_localize_script('baumchild-script', 'baumchild_ajax', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'redirecturl'    => wc_get_checkout_url(),
        'loadingmessage' => __( 'Iniciando sesión' )
    ));
}
add_action('wp_enqueue_scripts', 'baumchild_enqueue_styles');

function baumchild_dequeue_styles() {
    // $styles = array('font-awesome');
    $styles = array('font-awesome', 'woocommerce-smallscreen', 'genericons', 'turbotabs', 'twentynineteen-print-style', 'twentynineteen-style', 'woocommerce-general', 'select2');

    foreach ($styles as $style) {
        wp_dequeue_style($style);
        wp_deregister_style($style);
    }
}
add_action('wp_enqueue_scripts', 'baumchild_dequeue_styles', 99);

function baumchild_dequeue_script() {
    $scripts = array('turbotabs', 'select2', 'selectWoo');

    if (is_front_page())
        $scripts[] = 'wc-cart-fragments';

    foreach ($scripts as $script)
    {
        wp_dequeue_script($script);
    }
}

add_action('wp_print_scripts', 'baumchild_dequeue_script', 100);

/**
** CSS Inline
**/
function baumchild_print_inline_css() {
    $theme_uri = get_stylesheet_directory_uri();
    $css_path = $theme_uri . '/assets/css/theme.min.css';

    $css = baumchild_get_inline_styles();

    // if (!is_baum_localhost())
    // {
    //     $css .= baumchild_get_styles_data($css_path);
    // }

    return $css;
}

function baumchild_get_styles_data($file) {
    $data = wp_remote_fopen($file);

    return $data;
}

/**
** Check URL from localhost
**/
function is_baum_localhost() {
    if (preg_match('/localhost/', site_url()) || ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ))
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
** Favicon
**/
function baumchild_favicon_image() {
    $favicon = baumchild_dir_uri() . '/assets/images/favicon.png';
    if(!has_site_icon()):
    ?>
        <link rel="shortcut icon" href="<?php echo esc_attr($favicon); ?>" > 
    <?php
    endif;
}
add_action('wp_head', 'baumchild_favicon_image');
add_action('admin_head', 'baumchild_favicon_image');
