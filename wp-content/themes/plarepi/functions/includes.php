<?php
/**
** Required plugins
** Removed - 26-11-2019 - KMA
**/

/**
** Custom & override functions
**/
require get_stylesheet_directory() . '/functions/wp-fonts.php';
require get_stylesheet_directory() . '/functions/wp-misc.php';
require get_stylesheet_directory() . '/functions/wp-sidebars.php';
// require get_stylesheet_directory() . '/functions/wp-customizer.php';

/**
** WC Functions
**/
require get_stylesheet_directory() . '/functions/wc/wc-custom-functions.php';
require get_stylesheet_directory() . '/functions/wc/wc-checkout-options.php';