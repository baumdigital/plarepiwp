<?php
/**
** Payment section title in Checkout page
** Removed action woocommerce_checkout_order_review - 28-11-2019 - KMA
**/

/**
** Subscribe link after email field in Checkout form
**/
function baumchild_account_checkout_form_inline() {
	$label = __('Reciba promociones y descuentos en su correo.', 'baumchild');
	?>
	<script type="text/javascript">
		jQuery(function($) {
			<?php if(class_exists('MC4WP_MailChimp')) : ?>
				$('.woocommerce-checkout #billing_email_field').append('<span class="subscribe-field"><label class="checkbox"><input type="checkbox" name="mc4wp-subscribe" value="1" /><?php echo $label ?></label></span>');
			<?php endif; ?>
		});
	</script>
	<?php
}
add_action('woocommerce_after_checkout_form', 'baumchild_account_checkout_form_inline');
add_action('woocommerce_after_edit_account_address_form', 'baumchild_account_checkout_form_inline');

/**
** Enable/Disable field in shipping calculator & checkout
**/
add_filter('woocommerce_enable_order_notes_field', '__return_false');
add_filter('woocommerce_shipping_calculator_enable_city', '__return_true');
add_filter('woocommerce_shipping_calculator_enable_postcode', '__return_true', 999); // Override WC Prov Cant Dist setting.

/**
** Manage billing fields in checkout page
**/
function baumchild_woocommerce_billing_fields($fields) {
	$readonly = ['readonly' => 'readonly'];

	// Unset billing fields - Set hidden class
	$fields['billing_country']['class'] = (!baumchild_check_countries_quantity()) ? array('hidden') : array('');
	$fields['billing_postcode']['class'] = array('hidden');
	$fields['billing_company']['class'] = array('hidden');
	// $fields['billing_postcode']['required'] = baumchild_input_is_require();

	// Order billing fields
	$fields['billing_email'] = array(
		'class' => array('form-row', 'form-row-first'),
		'clear' => true,
		'label' => __('E-mail', 'baumchild'),
		'placeholder' => __('Escriba su e-mail', 'baumchild'),
		'priority' => 24,
		'required' => true,
	);

	$fields['billing_phone'] = array(
		'class' => array('form-row', 'form-row-last'),
		'clear' => true,
		'label' => __('Phone', 'woocommerce'),
		'priority' => 25,
		'placeholder' => __('Escriba su número de teléfono', 'baumchild'),
		'required' => true,
		'type' => 'tel',
		'maxlength' => 8
	);

	unset($fields['billing_country']);
	unset($fields['billing_postcode']);
	unset($fields['billing_company']);

	if ( is_user_logged_in() ) {
		$fields['billing_first_name']['custom_attributes'] = $readonly;
		$fields['billing_last_name']['custom_attributes'] = $readonly;
		$fields['billing_email']['custom_attributes'] = $readonly;
		$fields['billing_phone']['custom_attributes'] = $readonly;
	}

	return $fields;
}
add_filter('woocommerce_billing_fields', 'baumchild_woocommerce_billing_fields');

/**
** Check allowed countries quantity
**/
function baumchild_check_countries_quantity() {
	if(count(get_option('woocommerce_specific_allowed_countries')) > 1) {
		return true;
	}

	return false;
}

/**
** Manage address field in checkout page
**/
function baumchild_woocommerce_default_address_fields($fields) {
	$fields['first_name']['priority'] = 10;
	// $fields['first_name']['class'] = array('form-row', 'form-row-wide');
	$fields['first_name']['placeholder'] = __('Escriba su nombre', 'baumchild');

	$fields['last_name'] = array(
		'class' => array('form-row', 'form-row-last'),
		'label' => __('Apellidos', 'baumchild'),
		'placeholder' => __('Escriba sus apellidos', 'baumchild'),
		'priority' => 20,
		'required' => true
	);

	// $fields['address_1'] = array(
	// 	'class' => array('form-row', 'form-row-wide'),
	// 	'clear' => true,
	// 	'label' => __('Dirección', 'baumchild'),
	// 	'priority' => 50,
	// 	'placeholder' => __('Escriba su dirección', 'baumchild'),
	// 	'required' => baumchild_input_is_require(),
	// 	'type' => 'textarea',
	// );

	// $fields['state']['required'] = baumchild_input_is_require();
	// $fields['state']['class'][] = 'form-row-first';
	// $fields['state']['priority'] = 40;
	// $fields['city']['required'] = baumchild_input_is_require();
	// $fields['city']['class'][] = 'form-row-last';
	// $fields['city']['priority'] = 42;

	// if(!baumchild_check_countries_quantity())
	// 	$fields['country']['class'] = array('hidden');

	unset($fields['state']);
	unset($fields['city']);
	unset($fields['address_1']);
	unset($fields['address_2']);

	return $fields;
}
add_filter('woocommerce_default_address_fields', 'baumchild_woocommerce_default_address_fields', 30);

/**
** Checkout form fields
**/
function baumchild_checkout_fields($checkout_fields) {
	// "billing", "shipping", "account", "order"
	global $woocommerce;

	/** Remove field validation on wc_pickup_store shipping **/
	$shipping_methods = array('wc_pickup_store');
	$chosen_methods = WC()->session->get('chosen_shipping_methods');
	$chosen_shipping = $chosen_methods[0];

	if(in_array($chosen_shipping, $shipping_methods)) {
		unset($checkout_fields['billing']['billing_address_1']['validate']);
		unset($checkout_fields['billing']['billing_city']['validate']);
		unset($checkout_fields['billing']['billing_state']['validate']);
	}

	if ( get_option( 'woocommerce_registration_generate_password' ) == 'no' ) {
		$checkout_fields['account']['account_password'] = array(
			'class' => array('form-row', 'form-row-first'),
			'label' => __('Contraseña', 'baumchild'),
			'placeholder' => _x('Contraseña', 'placeholder', 'baumchild'),
			'required' => false,
			'type' => 'password',
		);

		$checkout_fields['account']['account_confirm_password'] = array(
			'class' => array('form-row', 'form-row-last'),
			'label' => __('Confirmar Contraseña', 'baumchild'),
			'placeholder' => _x('Confirmar Contraseña', 'placeholder', 'baumchild'),
			'required' => false,
			'type' => 'password',
		);
	}

	return $checkout_fields;
}
add_filter('woocommerce_checkout_fields', 'baumchild_checkout_fields', 10, 1);

/**
** Check required fields before allow checkout to proceed.
**/
function baumchild_checkout_custom_validation($posted) {
	$checkout = WC()->checkout;
	if(!is_user_logged_in() && ( $checkout->must_create_account || ! empty( $posted['createaccount']))) {
		if(strcmp( $posted['account_password'], $posted['account_confirm_password']) !== 0) {
			wc_add_notice(__('Contraseñas no coinciden.', 'baumchild'), 'error');
		}
	}

	if(!(preg_match('/^[0-9]{4}-[0-9]{4}$/', $_POST['billing_phone']))) {
		wc_add_notice(__('Teléfono debe contener 8 dígitos.', 'baumchild'), 'error');
	}
}
add_action('woocommerce_after_checkout_validation', 'baumchild_checkout_custom_validation', 10, 2);

/**
** Set order by fields key
** This function solves an error in checkout fields order, WC 3.5
** Function baumchild_set_order_fields_by_key unused since Baum WC Starter 1.0.3 and WC 3.5.3
** 09-01-2018
**/

/**
** Validate Shipping methods for required fields
**/
function baumchild_input_is_require() {
	global $woocommerce;

	$shipping_methods = array('wc_pickup_store');
	$chosen_methods = WC()->session->get('chosen_shipping_methods');
	$chosen_shipping = $chosen_methods[0];

	if (in_array($chosen_shipping, $shipping_methods) && is_checkout()) {		
		return false;
	}

	return true;
}

/**
** Message before password fields in checkout page
**/
function baumchild_account_message($checkout) {
	if ( $checkout->get_checkout_fields( 'account' ) ) :
		$create_password_message = !empty(get_theme_mod('baumchild_create_password_message')) ? '<p>' . get_theme_mod('baumchild_create_password_message') . '</p>' : '';
		?>
		<div class="create-account-message">
			<h3><?= __('Datos de la cuenta', 'baumchild') ?></h3>
			<strong><?php echo sprintf(__('Create una cuenta y recordaremos estos datos para tu próxima compra.', 'baumchild'), get_bloginfo( 'name', 'display' )) ?></strong>
			<?php echo $create_password_message; ?>
		</div>
		<?php
	endif;
}
add_action('woocommerce_before_checkout_registration_form', 'baumchild_account_message');

/**
** Checkout new fields array
**/
function baumchild_checkout_new_fields($key = '') {
	// "billing", "shipping", "account", "order"
	$new_fields = array(
		'billing' => array(
			'billing_celular' => __('Celular', 'baumchild')
		)
	);

	if(!empty($key) && !empty($new_fields[$key])) {
		return $new_fields[$key];
	} else {
		return $new_fields;
	}
}

/**
** Manage new order meta fields
**/
function baumchild_update_wc_custom_fields($order_id) {
	foreach (baumchild_checkout_new_fields() as $field_key => $fields) {
		switch ($field_key) {			
			default:
				foreach ($fields as $key => $field) {
					if (!empty($_POST[$key])) {
						update_post_meta($order_id, $key, sanitize_text_field($_POST[$key]));
					}
				}
				break;
		}
	}
}
add_action('woocommerce_checkout_update_order_meta', 'baumchild_update_wc_custom_fields');

/**
** Adding new fields to billing details, admin page
**/
function baumchild_custom_field_admin_order($order) {
	foreach (baumchild_checkout_new_fields() as $key => $fields) {
		foreach ($fields as $key => $field) {
			$order_id = $order->get_id();
			$value = get_post_meta($order_id, $key, true);
			switch ($key) {
				case 'billing_celular':
					$data = '<a href="tel:' . $value . '">' . get_post_meta($order_id, $key, true) . '</a>';
					break;
				
				default:
					$data = $value;
					break;
			}

			if(!empty($value)) :
				?>
				<p>
					<strong class="title"><?php echo $field . ':' ?></strong>
					<span class="data"><?= $data ?></span>
				</p>
				<?php
			endif;
		}
	}
}
add_action('woocommerce_admin_order_data_after_billing_address', 'baumchild_custom_field_admin_order', 20, 1);

/**
** Adding new fields to profile page
**/
function baumchild_custom_fields_user_profile($user) {
	?>
	<h2><?= __('Campos Personalizados', 'baumchild') ?></h2>
	<table class="form-table">
		<?php foreach (baumchild_checkout_new_fields() as $field_key => $fields) : ?>
			<?php foreach ($fields as $key => $field) : $field_value = get_user_meta($user->ID, $key, true); ?>
				<tr>
					<th><label for="<?= $key ?>"><?= $field; ?> </label></th>
					<td>
						<input type="text" name="<?= $key ?>" value="<?php echo esc_attr($field_value) ?>" class="regular-text" />
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</table>
	<?php
}
add_action('show_user_profile', 'baumchild_custom_fields_user_profile');
add_action('edit_user_profile', 'baumchild_custom_fields_user_profile');

function baumchild_save_extra_custom_fields($user_id) {
	if(isset($_POST['billing_phone']))
		update_user_meta($user_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ));
}
add_action('personal_options_update', 'baumchild_save_extra_custom_fields');
add_action('edit_user_profile_update', 'baumchild_save_extra_custom_fields');
add_action('woocommerce_save_account_details', 'baumchild_save_extra_custom_fields');
add_action('woocommerce_customer_save_address', 'baumchild_save_extra_custom_fields');

/**
** Remove password strength validation
**/
function baumchild_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
add_action('wp_print_scripts', 'baumchild_remove_password_strength', 100);

/**
** Adding custom data to emails before order table
**/
function baumchild_email_before_order_table($order) {
	$order_id = $order->get_id();
	$customer_details = array(
		array(
			'label' => __('Nombre', 'baumchild'),
			'value' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name()
		),
		array(
			'label' => __('Email', 'baumchild'),
			'value' => $order->get_billing_email()
		),
	);
	?>
	<div style="margin-bottom: 20px;">		
		<?php 
		if(!empty($customer_details)) :
			foreach ($customer_details as $key => $detail) :
				?>
				<span><strong><?php echo $detail['label'] ?><span class="colon">:</span></strong> <?php echo esc_html($detail['value']); ?></span><br/>
				<?php
			endforeach;
		endif;
		?>
	</div>

	<?php
		if ($order->needs_payment() && !$sent_to_admin) :
			if($order->get_payment_method() == 'bacs') {
				?>
					<p><a href="<?= esc_url( $order->get_view_order_url() ) ?>"><?= __('Enviar número de transferencia', 'baumchild') ?></a></p>
				<?php
			}
		?>
	<?php endif;
}
add_action('woocommerce_email_before_order_table', 'baumchild_email_before_order_table');

/**
** Showing addresses details to non logged in users
** This is hidden by default
**/
function baumchild_adding_customers_details_to_thankyou($order_id) {
    // Only for non logged in users
    if ( ! $order_id || is_user_logged_in() ) return;

    $order = wc_get_order($order_id);

    wc_get_template( 'order/order-details-customer.php', array('order' => $order ));
}
// add_action('woocommerce_thankyou', 'baumchild_adding_customers_details_to_thankyou');
