<?php

/**
** WC Support
**/
function woocommerce_support()
{
	add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'woocommerce_support');

/**
** Declare WC env variables
**/
if (!defined('WC_LIST_INTERVAL'))
{
	define('WC_LIST_INTERVAL', !empty(get_theme_mod('baumchild_products_per_page')) ? get_theme_mod('baumchild_products_per_page') : 12);
}

/**
** Manage header slider
**/
function baumchild_set_slider()
{
	// $slider_alias = get_theme_mod('baumchild_site_slider');
	$slider_alias = apply_filters('get_option_baumchild_site_slider', get_theme_mod('baumchild_site_slider'));
	if (!empty($slider_alias) && checkRevSliderExists($slider_alias)) :
		?>
		<div class="site-header-slider">
			<?php putRevSlider($slider_alias, "homepage"); ?>
		</div>
		<?php
	endif;
}

add_action('baumchild_slider_header', 'baumchild_set_slider');

/**
** Manage sidebar in WC Pages
**/
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
// add_action('woocommerce_before_shop_loop', 'woocommerce_get_sidebar', 20);
add_action('baumchild_filter_before_loop', 'woocommerce_get_sidebar');

/**
** Posts per page
**/
function baumchild_posts_per_page_dropdown()
{
	if (!wc_get_loop_prop('is_paginated') || !woocommerce_products_will_display())
	{
		return;
	}
	?>
	<form class="woocommerce-posts-per-page" method="get">
		<select name="per_page" class="posts-per-page" style="width: 100%;">
			<?php
			$interval = WC_LIST_INTERVAL;
			$current = isset($_GET['per_page']) ? wc_clean($_GET['per_page']) : WC_LIST_INTERVAL;

			for ($cont = 0; $cont < 4; $cont++)
			{
				?>
				<option value="<?= $interval ?>" <?php selected($current, $interval) ?>><?= sprintf('Mostrar %d', $interval) ?></option>
				<?php
				$interval += WC_LIST_INTERVAL;
			}
			?>
			<option value="-1" <?php selected($current, -1) ?>><?= __('Todos', 'baumchild') ?></option>
		</select>
		<input type="hidden" name="paged" value="1" />
		<?php wc_query_string_form_fields(null, array('per_page', 'submit', 'paged')); ?>
	</form>
	<?php
}

add_action('woocommerce_before_shop_loop', 'baumchild_posts_per_page_dropdown', 30);

/**
** Manage pagination, columns and product limit
**/
function baumchild_alter_product_query_clean($query)
{
	$interval = isset($_GET['per_page']) ? wc_clean($_GET['per_page']) : WC_LIST_INTERVAL;
	$orderby = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));

	if ($query->is_main_query() && ( $query->get('wc_query') === 'product_query' ))
	{
		$meta_query = $query->get('meta_query');
		$query->set('posts_per_page', $interval);
		$query->set('post_type', array('product'));

		if ($orderby == 'price')
		{
			$query->set('orderby', ' meta_value_num title date name ID');
			$query->set('order', 'ASC');
		}
		elseif ($orderby == 'nombre' || $orderby == 'alphabetical')
		{
			$query->set('orderby', 'title');
			$query->set('order', 'ASC');
		}
		elseif ($orderby == 'recientes')
		{
			$query->set('orderby', 'date');
			$query->set('order', 'DESC');
		}
		else
		{
			// $query->set('orderby', 'RAND(' . baumchild_set_random() . ')');
			$query->set('order', 'DESC');
		}
	}

	return $query;
}

add_action('woocommerce_product_query', 'baumchild_alter_product_query_clean');

/**
** Customize nav items in Account Menu
**/
function baumchild_account_menu_items($items)
{
	$logout = $items['customer-logout'];

	unset($items['dashboard']);
	unset($items['edit-address']);
	unset($items['downloads']);
	unset($items['tinv_wishlist']);
	unset($items['customer-logout']);
	$items['orders'] = __('Cotizaciones', 'baumchild');
	// $items['edit-billing-address'] = __('Mi Dirección', 'baumchild');
	// $reorder = array('edit-address' => __('Mi Dirección', 'baumchild'));
	// $items = array_slice( $items, 0, 2, true ) + $reorder + array_slice( $items, 2, NULL, true );

	$items['customer-logout'] = $logout;

	return $items;
}

add_filter('woocommerce_account_menu_items', 'baumchild_account_menu_items');

/**
** Manage endpoints data
**/
function baumchild_manage_endpoint_data($url, $endpoint, $value, $permalink)
{
	switch ($endpoint)
	{
		case 'edit-shipping-address':
			$url = esc_url(wc_get_endpoint_url('edit-address', 'shipping', $permalink));
			break;
		case 'edit-billing-address':
			$url = esc_url(wc_get_endpoint_url('edit-address', 'billing', $permalink));
			break;
	}

	return $url;
}

add_filter('woocommerce_get_endpoint_url', 'baumchild_manage_endpoint_data', 10, 4);

function baumchild_logout_without_confirmation()
{
	global $wp;

	if (isset($wp->query_vars['customer-logout']))
	{
		wp_redirect(str_replace('&amp;', '&', wp_logout_url(wc_get_page_permalink('myaccount'))));

		exit;
	}
}

add_action('template_redirect', 'baumchild_logout_without_confirmation');

/**
** Manage WC Breadcrumbs
**/
function baumchild_manage_wc_breadcrums($defaults)
{
	$defaults['delimiter'] = '<span class="delimiter"> > </span>';
	$defaults['home'] = '';

	return $defaults;
}

add_filter('woocommerce_breadcrumb_defaults', 'baumchild_manage_wc_breadcrums');

function baumchild_remove_wc_breadcrumbs()
{
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
	add_action('woocommerce_archive_description', 'woocommerce_breadcrumb', 30, 0);
}

add_action('init', 'baumchild_remove_wc_breadcrumbs');

/**
** Add container to WC Pages
**/
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

function baumchild_wrapper_start()
{
	echo '<div class="main-container shop-wrapper">';
}

add_action('woocommerce_before_main_content', 'baumchild_wrapper_start', 10);

function baumchild_wrapper_end()
{
	echo '</div>';
}

add_action('woocommerce_after_main_content', 'baumchild_wrapper_end', 10);

/**
** Sorter Options
**/
function baumchild_wc_catalog_orderby($sortby)
{
	// $sortby['date'] = 'Ordenar por';
	$sortby['recientes'] = 'Más Recientes';
	$sortby['price'] = 'Por Precio';
	$sortby['nombre'] = 'Por Nombre';
	unset($sortby["date"]);
	unset($sortby["popularity"]);
	unset($sortby["price"]);
	unset($sortby["price-desc"]);
	unset($sortby["rating"]);

	return $sortby;
}

add_filter('woocommerce_default_catalog_orderby_options', 'baumchild_wc_catalog_orderby');
add_filter('woocommerce_catalog_orderby', 'baumchild_wc_catalog_orderby');

/**
** Update WC Add to Cart in loop
** 26-11-2019 - KMA
**/
function baumchild_loop_add_to_cart_link()
{
	global $product;

	return '<a href="' . $product->get_permalink() . '" class="btn btn-primary btn-view-more">' . __('Ver más', 'baumchild') . '</a>';
}

add_filter('woocommerce_loop_add_to_cart_link', 'baumchild_loop_add_to_cart_link');

/**
** Return to Cart in checkout page
** Function baumchild_cart_button_in_checkout removed
** 27-11-2019 - KMA
**/
/**
** Remove Result count in categories
**/
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

/**
** Change SKU Position, product page & quick view
**/
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 9);

remove_action('xoo-qv-summary', 'woocommerce_template_single_meta', 30);
add_action('xoo-qv-summary', 'woocommerce_template_single_meta', 9);

/**
** Adding Return to Shop button when not products found
**/
function baumchild_link_to_shop_no_products_found()
{
	?>
	<p class="return-to-shop">
		<a class="button wc-backward" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
	<?php _e('Return to shop', 'woocommerce') ?>
		</a>
	</p>
	<?php
}

add_action('woocommerce_no_products_found', 'baumchild_link_to_shop_no_products_found');

/**
** Change column layout to 3 columns
**/
if (!function_exists('loop_columns'))
{

	function loop_columns()
	{
		return 3;
	}

}
add_filter('loop_shop_columns', 'loop_columns');

/**
** Remove sale flash in product listing & Quick view
**/
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

remove_action('xoo-qv-images', 'woocommerce_show_product_sale_flash', 10);

function baumchild_single_product_class_before()
{
	echo '<div class="product-details-container summary">';
}

function baumchild_single_product_class_after()
{
	echo '</div>';
}

remove_action('woocommerce_before_single_product_summary', 'wcbox_quick_view_class_before', 10);
remove_action('woocommerce_after_single_product_summary', 'wcbox_quick_view_class_after', 5);
add_action('woocommerce_before_single_product_summary', 'baumchild_single_product_class_before', 10);
add_action('woocommerce_after_single_product_summary', 'baumchild_single_product_class_after', 5);

/**
** Carousel Related Products
**/
function baumchild_wc_after_single_product_summary()
{
	global $product;

	$descripcion = $product->get_description();

	if (!empty($descripcion))
	{
		?>
		<div class="clear"></div>
		<h3 class="description-title"><?= __('Especificaciones', 'baumchild'); ?></h3>
		<div class="product-description">
		<?= apply_filters('the_content', $descripcion); ?>
		</div>
		<?php }
		if (is_active_sidebar('sidebar-single-product-carousels')) :
			?>
		<div class="clear"></div>
		<div class="carousel-products section-carousel">

		<?php dynamic_sidebar('sidebar-single-product-carousels'); ?>

		</div><!-- end container -->
		<?php
	endif;
}

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_after_single_product_summary', 'baumchild_wc_after_single_product_summary', 30);

/**
** Change WC image placeholder
**/
// Product detail
function baumchild_wc_placeholder_img_src($src)
{
	return baumchild_placeholder_img_src();
}

add_filter('woocommerce_placeholder_img_src', 'baumchild_wc_placeholder_img_src');

function baumchild_placeholder_img_src($ext = 'jpg')
{
	$src = get_stylesheet_directory_uri() . '/assets/images/wc-placeholder.' . $ext;

	return $src;
}

// Products listing
function baumchild_wc_placeholder_img($image_html, $size, $dimensions)
{
	$image = wc_placeholder_img_src($size);
	$image_html = '<img src="' . esc_attr($image) . '" alt="' . esc_attr__('Placeholder', 'woocommerce') . '" width="' . esc_attr($dimensions['width']) . '" class="woocommerce-placeholder wp-post-image" height="' . esc_attr($dimensions['height']) . '" />';

	return $image_html;
}

add_filter('woocommerce_placeholder_img', 'baumchild_wc_placeholder_img', 10, 3);

/**
** Adding "From" to variable products price
**/
function baumchild_custom_variable_price($price, $product)
{
	$prefix = sprintf('%s ', __('', 'baumchild'));
	$min_price_regular = $product->get_variation_regular_price('min', true);
	$min_price_sale = $product->get_variation_sale_price('min', true);
	$max_price = $product->get_variation_price('max', true);
	$min_price = $product->get_variation_price('min', true);

	$price = ( $min_price_sale == $min_price_regular ) ?
			wc_price($min_price_regular) :
			'<ins>' . wc_price($min_price_sale) . '</ins>' . ' <del>' . wc_price($min_price_regular) . '</del>';

	return ( $min_price == $max_price ) ?
			$price :
			sprintf('%s%s', $prefix, $price);
}

add_filter('woocommerce_variable_sale_price_html', 'baumchild_custom_variable_price', 10, 2);
add_filter('woocommerce_variable_price_html', 'baumchild_custom_variable_price', 10, 2);

/**
** Simple product sale price format
**/
function baumchild_custom_single_sale_price($price, $regular_price, $sale_price)
{
	return '<ins>' . ( is_numeric($sale_price) ? wc_price($sale_price) : $sale_price ) . '</ins> <del>' . ( is_numeric($regular_price) ? wc_price($regular_price) : $regular_price ) . '</del>';
}

add_filter('woocommerce_format_sale_price', 'baumchild_custom_single_sale_price', 10, 3);

/**
** Update product title structure in loops
**/
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

function baumchild_template_loop_product_title()
{
	global $product;
	echo '<div class="dotdotdot"><h3 class="woocommerce-loop-product__title">' . get_the_title() . '</h3></div><p class="the-sku">' . $product->get_sku() . '</p>';
}

add_action('woocommerce_shop_loop_item_title', 'baumchild_template_loop_product_title', 10);

/**
** Move single variation div before add to cart button
**/
remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);
add_action('woocommerce_after_add_to_cart_quantity', 'woocommerce_single_variation', 10);

/**
** Remove WC Product Tabs, Cross Sell (Cart Page), Upsell & default Related Products
**/
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

/**
** Listing product structure
**/
function baumchild_open_product_image_container()
{
	echo '<div class="product-wrapper">';
	echo '<div class="product-image-container">';
	echo '<span class="vertical">';
}

add_action('woocommerce_before_shop_loop_item', 'baumchild_open_product_image_container', 5);

function baumchild_close_a_tag()
{
	echo "</a>";
}

add_action('woocommerce_before_shop_loop_item_title', 'baumchild_close_a_tag', 11);

function baumchild_close_div_tag()
{
	// echo do_shortcode("[ti_wishlists_addtowishlist loop=yes]");
	echo "</span></div><!-- .product-image-container -->";
}

add_action('woocommerce_before_shop_loop_item_title', 'baumchild_close_div_tag', 12);

function baumchild_open_new_a_tag()
{
	global $product;

	$link = apply_filters('woocommerce_loop_product_link', get_the_permalink(), $product);

	echo '<div class="product-title-container"><a href="' . esc_url($link) . '" class="woocommerce-loop-product_title_link">';
}

add_action('woocommerce_shop_loop_item_title', 'baumchild_open_new_a_tag', 5);

function baumchild_close_product_title_tag()
{
	echo "</div><!-- .product-title-container -->";
	echo "</div><!-- .product-wrapper -->";
}

add_action('woocommerce_after_shop_loop_item', 'baumchild_close_product_title_tag', 20);

/**
** Fix double a tag in product loop
** Set Quick View button position as "On Image Hover"
**/
remove_action('woocommerce_after_shop_loop_item', 'xoo_qv_button', 11);
add_action('woocommerce_before_shop_loop_item_title', 'xoo_qv_button', 11);

/**
** CRC Currency
**/
function baumchild_currency_symbol($currency_symbol, $currency)
{
	switch ($currency)
	{
		case 'CRC':
			$currency_symbol = '¢';
			break;
	}
	return $currency_symbol;
}

add_filter('woocommerce_currency_symbol', 'baumchild_currency_symbol', 10, 2);

/**
** Update cart quantity in cart page
**/
function baumchild_mini_cart_html()
{
	$inCart = (WC()->cart->get_cart_contents_count() > 0) ? 'items-in-cart' : '';
	?>
	<a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="wcmenucart-contents">
		<i class="icon-dt_cart"></i>
		<span class="texto-mi-carrito"><?= __('Mi cotización', 'baumchild') ?></span>
		<span class="text-shopping-cart">
			<b class="cart-value <?= $inCart ?>"><?php echo wp_kses_data(WC()->cart->get_cart_contents_count()); ?></b>
		</span>
	</a>
	<?php
}

function baumchild_wc_header_add_to_cart_fragment($fragments)
{
	ob_start();
	baumchild_mini_cart_html();

	$fragments['a.wcmenucart-contents'] = ob_get_clean();

	return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'baumchild_wc_header_add_to_cart_fragment');

/**
** Remove cart shipping label
**/
function baumchild_wc_cart_shipping_method_label($label, $method)
{
	if ($method->method_id == 'flat_rate' && ((int) $method->get_cost()) > 0)
	{
		$label = wc_price((int) $method->get_cost());
	}
	elseif (((int) $method->get_cost()) == 0)
	{
		$label = $method->get_label();
	}

	return $label;
}

;
add_filter('woocommerce_cart_shipping_method_full_label', 'baumchild_wc_cart_shipping_method_label', 10, 2);

/**
** Add image placeholder to Ajax Search Pro results
**/
function asp_get_post_format_image($image, $id)
{
	if (empty($image))
	{
		$image = get_stylesheet_directory_uri() . '/assets/images/wc-placeholder.svg';
	}

	return $image;
}

add_filter('asp_result_image_after_prostproc', 'asp_get_post_format_image', 1, 2);

/**
** Hide shipping rates when free shipping is available.
**/
function baumchild_hide_shipping_when_free_is_available($rates)
{
	$free = array();
	$is_free = false;

	foreach ($rates as $rate_id => $rate)
	{
		if ('free_shipping' === $rate->method_id)
		{
			$free[$rate_id] = $rate;

			$is_free = true;
		}

		if (class_exists('WC_PICKUP_STORE') && 'wc_pickup_store' === $rate->method_id && $is_free)
		{
			$free[$rate_id] = $rate;
		}
	}
	return !empty($free) ? $free : $rates;
}

add_filter('woocommerce_package_rates', 'baumchild_hide_shipping_when_free_is_available', 100);

/**
** Remove navigation in accounts pages
**/
remove_action('woocommerce_account_navigation', 'woocommerce_account_navigation');

/**
** Add setting links to WC
**/
function baumchild_wc_settings_admin_submenu()
{
	if (is_plugin_active('woocommerce-products-filter/index.php'))
	{
		add_submenu_page(
				'woocommerce', __('WOOF Products Filter', 'baumchild'), __('WOOF Products Filter', 'baumchild'), 'manage_options', 'admin.php?page=wc-settings&tab=woof'
		);
	}

	if (is_plugin_active('woocommerce-delivery-notes/woocommerce-delivery-notes.php'))
	{
		add_submenu_page(
				'woocommerce', __('Imprimir', 'baumchild'), __('Imprimir', 'baumchild'), 'manage_options', 'admin.php?page=wc-settings&tab=wcdn-settings'
		);
	}
}

add_action('admin_menu', 'baumchild_wc_settings_admin_submenu');

/**
** Remove top price & excerpt
**/
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('xoo-qv-summary', 'woocommerce_template_single_price', 15);
remove_action('xoo-qv-summary', 'woocommerce_template_single_excerpt', 20);

/**
** Product Description
**/
function baumchild_template_product_description()
{
	global $product;
	$descripcion = $product->get_short_description();

	if (!empty($descripcion)) :
		?>
		<div class="product-description clear">
		<?= apply_filters('the_content', $descripcion); ?>
		</div>
		<?php
	endif;
}

add_action('woocommerce_single_product_summary', 'baumchild_template_product_description', 45);
add_action('xoo-qv-summary', 'baumchild_template_product_description', 30);

add_action('xoo-qv-summary', function() {
	add_action('woocommerce_single_product_summary', 'baumchild_template_product_description', 30);
});

/**
** Remove "shipped via" label in order details
**/
add_filter('woocommerce_order_shipping_to_display_shipped_via', function() {
	return false;
});

/**
** Remove shipped_via label in order_item_totals array
** Sufix
**/
add_filter('woocommerce_order_shipping_to_display_shipped_via', function() {
	return '';
});

/**
** WC pagination arguments
**/
function baumchild_pagination_arguments($args)
{
	$args['prev_text'] = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
	$args['next_text'] = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';
	return $args;
}

add_filter('woocommerce_pagination_args', 'baumchild_pagination_arguments');

/**
** Single add to cart button wrapper
**/
function baumchild_add_to_cart_wrapper_open()
{
	echo '<div class="add-to-cart-container">';
}

add_action('woocommerce_after_add_to_cart_quantity', 'baumchild_add_to_cart_wrapper_open');

function baumchild_add_to_cart_wrapper_close()
{
	echo '</div> <!-- .add-to-cart-container -->';
}

add_action('woocommerce_after_add_to_cart_button', 'baumchild_add_to_cart_wrapper_close');

/**
** Add cart in Added to cart popup using Ajax
** 06-12-2019 - KMA
**/
function baumchild_ajax_add_to_cart()
{
	$product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
	$quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
	$variation_id = absint($_POST['variation_id']);
	$passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
	$product_status = get_post_status($product_id);

	if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status)
	{

		do_action('woocommerce_ajax_added_to_cart', $product_id);

		if ('yes' === get_option('woocommerce_cart_redirect_after_add'))
		{
			wc_add_to_cart_message(array($product_id => $quantity), true);
		}

		WC_AJAX::get_refreshed_fragments();
	}
	else
	{

		$data = array(
			'error' => true,
			'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

		echo wp_send_json($data);
	}

	wp_die();
}

add_action('wp_ajax_baumchild_ajax_add_to_cart', 'baumchild_ajax_add_to_cart');
add_action('wp_ajax_nopriv_baumchild_ajax_add_to_cart', 'baumchild_ajax_add_to_cart');

/**
** Update cart in Checkout using Ajax
** 28-11-2019 - KMA
**/
function baumchild_ajax_update_to_cart()
{
	$cart_key = sanitize_text_field($_POST['cart_key']);
	$new_qty = (float) $_POST['quantity'];
	$product_id = (isset($_POST['product_sku']) && !empty($_POST['product_sku'])) ? wc_get_product_id_by_sku($_POST['product_sku']) : 0;

	if (!is_numeric($new_qty) || $new_qty < 0 || !$cart_key)
	{
		wp_send_json(array('error' => __('Something went wrong', 'baumchild')));
	}

	$cart_success = $new_qty == 0 ? WC()->cart->remove_cart_item($cart_key) : WC()->cart->set_quantity($cart_key, $new_qty);
	if ($cart_success)
	{
		if ($product_id)
		{
			do_action('woocommerce_ajax_added_to_cart', $product_id);

			if ('yes' === get_option('woocommerce_cart_redirect_after_add'))
			{
				wc_add_to_cart_message(array($product_id => $new_qty), true);
			}
		}

		WC_AJAX::get_refreshed_fragments();
	}
	else
	{
		if (wc_notice_count('error') > 0)
		{
			echo wc_print_notices();
		}
	}

	wp_die();
}

add_action('wp_ajax_baumchild_ajax_update_to_cart', 'baumchild_ajax_update_to_cart');
add_action('wp_ajax_nopriv_baumchild_ajax_update_to_cart', 'baumchild_ajax_update_to_cart');

/**
** Wrapper for Input quantity and Button add to cart
**/
function baumchild_open_qty_button_wrapper() {
	echo '<div class="qty-add-to-cart-wrapper">';
}
add_action('woocommerce_before_add_to_cart_quantity', 'baumchild_open_qty_button_wrapper');
add_action('woocommerce_before_single_variation', 'baumchild_open_qty_button_wrapper');

function baumchild_close_qty_button_wrapper() {
	echo '</div>';
}
add_action('woocommerce_after_add_to_cart_button', 'baumchild_close_qty_button_wrapper');
add_action('woocommerce_after_single_variation', 'baumchild_close_qty_button_wrapper');

/**
** Cart empty message
**/
function baumchild_wc_empty_cart_message() {
	return __('Su cotización está vacía.  Puede navegar el menú de Productos para agregar items a su cotización', 'baumchild');
}
// add_filter('wc_empty_cart_message', 'baumchild_wc_empty_cart_message');

/** Plarepi **/
/**
** Add to cart redirect URL changed to Checkout
**/
function baumchild_wc_change_add_to_cart_redirect_url($url){
	$url = wc_get_checkout_url();
	return $url;
}
add_filter('woocommerce_add_to_cart_redirect', 'baumchild_wc_change_add_to_cart_redirect_url');

/**
** Remove Thank you page information
**/
remove_action('woocommerce_thankyou', 'woocommerce_order_details_table', 10);

/**
** Thank you page title
**/
function baumchild_title_order_received($title, $id) {
	// if (is_order_received_page() && get_the_ID() === $id) {
	if (is_order_received_page()) {
		$title = __('Gracias por su compra', 'baumchild');
	}
	return $title;
}
add_filter('the_title', 'baumchild_title_order_received', 10, 2);

function baumchild_wc_my_account_my_orders_actions($actions, $order) {
	unset($actions['view']);
	return $actions;
}
add_filter('woocommerce_my_account_my_orders_actions', 'baumchild_wc_my_account_my_orders_actions', 10, 2);

/**
** Add script and enabled Ajax login
** Removed - 13-05-2020 - KMA
**/

/**
 * Validate user login and return json message.
 */
function ajaxlogin() {
	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'woocommerce-login', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info                  = array();
	$info['user_login']    = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember']      = true;

	$user_signon = wp_signon( $info, false );

	if ( is_wp_error( $user_signon ) ) {
		echo json_encode( array( 'loggedin' => false, 'message' => __( 'Nombre de usuario o contraseña incorrectos.' ) ) );
	} else {
		echo json_encode( array( 'loggedin' => true, 'message' => __( 'Inicio de sesión exitoso, por favor espere...' ) ) );
	}

	die();
}
add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajaxlogin' );
// Enable the user with no privileges to run ajaxlogin() in AJAX

/**
** Main thankyou page text
**/
function baumchild_wc_thankyou_order_received_text($thankyou_text) {
	return __('Le enviamos el comprobante de su compra a su correo electrónico.', 'baumchild');
}
add_filter('woocommerce_thankyou_order_received_text', 'baumchild_wc_thankyou_order_received_text');

function baumchild_after_thankyou_page() {
	?>
	<p class="woocommerce-notice--baum"><?= __('Si es un nuevo registro, también enviamos su constraseña por correo. Utilicela para iniciar sesión.', 'baumchild') ?></p>
	<p class="woocommerce-notice"><a href="https://admin.plarepi.com" class="btn btn-plarepi" target="_blank"><?= __('Ir a Plarepi', 'baumchild') ?></a></p>
	<?php
}
add_action('woocommerce_thankyou', 'baumchild_after_thankyou_page');
