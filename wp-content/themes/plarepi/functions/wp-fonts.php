<?php

/**
 * * Including the url of fonts googleapis 
 * */
function baumchild_fonts_url()
{
    $fonts_url = '';
    $fonts = array();
    $subsets = 'latin,latin-ext';

    if ('off' !== _x('on', 'Google fonts: on or off', 'baumchild'))
    {
        $fonts = array('DM+Sans:400,500,700');
    }

    if ($fonts)
    {
        $fonts_url = add_query_arg(array(
            'family' => implode('|', $fonts),
            'display' => 'swap',
            'subset' => urlencode($subsets),
                ), 'https://fonts.googleapis.com/css');
    }

    return $fonts_url;
}

/**
 * * Custom theme fonts
 * */
function baumchild_enqueue_fonts()
{
    
}

add_action('wp_enqueue_scripts', 'baumchild_enqueue_fonts');

/**
 * * Font face and inline styles
 * */
function baumchild_get_inline_styles()
{
    $theme_uri = get_stylesheet_directory_uri();
    $ajax_loader = $theme_uri . '/assets/images/wc-ajax-loader.svg';
    $font_custom = $theme_uri . '/assets/fonts/custom';
    $_THEME = $theme_uri;

    if (isset($_SERVER['HTTP_USER_AGENT']) && ( (strpos($_SERVER['HTTP_USER_AGENT'], 'Edge') !== false) || (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)))
    {
        $ajax_loader = $theme_uri . '/assets/images/wc-ajax-loader.gif';
    }
    ob_start();
    ?>
    <style>

        .woocommerce .blockUI.blockOverlay:before,
        .woocommerce .loader:before,
        .xoo-qv-panel .xoo-qv-preloader .xoo-qv-speeding-wheel:before,
        div.woof_info_popup:before,
        .bm-loader:before,
        .pp_loaderIcon:after {
            background-image: url('<?= $ajax_loader; ?>'); 
        }

        .site-footer-bottom::before {
            background-image: url('<?= $theme_uri; ?>/assets/images/theme/decoration-footer-left.svg');
        }
        .site-footer-bottom::after {
            background-image: url('<?= $theme_uri; ?>/assets/images/theme/decoration-footer-right.svg');
        }
        .section-cta{
            background-image: url('<?= $theme_uri; ?>/assets/images/theme/cover-call-to-action.png');
        }

        ._social-waze {
            background-image: url('<?= $theme_uri; ?>/assets/images/theme/waze.svg');
        }
        ._social-maps {
            background-image: url('<?= $theme_uri; ?>/assets/images/theme/google-maps.svg');
        }

    </style>

    <?php
    $css = ob_get_contents();
    ob_end_clean();
    return $css;
}
