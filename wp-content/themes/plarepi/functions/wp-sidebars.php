<?php

/**
** Remove sidebars from parent theme
**/
function baumchild_remove_parent_sidebars() {
    unregister_sidebar('sidebar-1');
    unregister_sidebar('sidebar-2');
    unregister_sidebar('sidebar-3');
}
add_action('init', 'baumchild_remove_parent_sidebars', 11);

/**
** Add new sidebars
**/
function baumchild_custom_widgets_init() {
    // Footer COL 1
    register_sidebar(array(
        'name' => __('Footer Col 1', 'baumchild'),
        'id' => 'sidebar-footer-1',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-1 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // Footer COL 2
    register_sidebar(array(
        'name' => __('Footer Col 2', 'baumchild'),
        'id' => 'sidebar-footer-2',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-2 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer Copyright text
    register_sidebar(array(
        'name' => __('Footer Copy Text', 'baumchild'),
        'id' => 'sidebar-footer-copy',
        'description' => __('', 'baumchild'),
        'before_widget' => '<div class="copy-text %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}
add_action('widgets_init', 'baumchild_custom_widgets_init');
