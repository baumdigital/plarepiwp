<?php
/**
 * * The template for displaying the header
 * */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <?php do_action('baumchild_after_body_open'); ?>

        <div id="page" class="site">
            <header class="site-header">
                <div id="site-header-menu" class="site-header-menu">
                    <div class="site-header-wrap container">
                        <div class="site-header-nav_toggle">
                            <a href="javascript:void(0);" class="site-header-toggle-btn">
                                <span class="toggle toggle_open icon-dt_menu"></span>
                                <span class="toggle toggle_close icon-dt_cerrar"></span>
                            </a>
                        </div>
                        <div class="site-header-logo">
                            <?php
                            if (function_exists('baumchild_default_logo'))
                            {
                                baumchild_default_logo();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </header>

            <?php do_action('baumchild_slider_header'); ?>

            <div id="content" class="site-content">
