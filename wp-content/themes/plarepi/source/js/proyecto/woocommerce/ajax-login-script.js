    /**
    ** Ajax login on Checkout
    **/
    $('#woo-form-login input').focus(function () {
        $('form#woo-form-login .alert').fadeOut(1000);
    });

    // Perform AJAX login on form submit
    $('form#woo-form-login').on('submit', function (e) {
        $('form#woo-form-login .alert').addClass('alert-info').fadeIn(1000);
        $('form#woo-form-login .alert p').html(baumchild_ajax.loadingmessage + ' <i class="fas fa-circle-notch fa-spin"></i>');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baumchild_ajax.ajax_url,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#woo-form-login #username').val(),
                'password': $('form#woo-form-login #password').val(),
                'security': $('form#woo-form-login #security').val()
            },
            beforeSend: function () {
                $('form#woo-form-login .alert').removeClass('alert-danger alert-success');
            },
            success: function (data) {
                if (data.loggedin == true) {
                    $('form#woo-form-login .alert').removeClass('alert-info').addClass('alert-success');
                    document.location.href = baumchild_ajax.redirecturl;
                } else {
                    $('form#woo-form-login .alert').removeClass('alert-info').addClass('alert-danger');
                }

                $('form#woo-form-login .alert p').text(data.message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('form#woo-form-login .alert').addClass('alert-danger');
                $('form#woo-form-login .alert p').text('Un error ha ocurrido, por favor inténtelo nuevamente.');
                console.log('jqXHR: ' + jqXHR.responseText);
                console.log('textStatus: ' + textStatus);
                console.log('error: ' + errorThrown);
            }
        });

        e.preventDefault();
    });
    