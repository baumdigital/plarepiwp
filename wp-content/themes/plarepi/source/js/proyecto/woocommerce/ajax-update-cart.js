    /**
    ** Update cart in review order table in Checkout
    **/
    $('.woocommerce-checkout').on('change', 'input.qty', function() {
        var _this = $(this);
        var data = {
            action: 'baumchild_ajax_update_to_cart',
            cart_key: _this.closest('tr').data('cart_item_key'),
            product_sku: _this.closest('tr').find('.product-sku').text(),
            quantity: _this.val(),
        };

        if (!$('#order_review').find('.shop_table.bm-loader').length) {
            ajax_update_cart(data, false);
        }
    });

    $(document).on('click', '.remove-product', function() {
        var _this = $(this);
        var data = {
            action: 'baumchild_ajax_update_to_cart',
            cart_key: _this.closest('tr').data('cart_item_key'),
            quantity: 0,
        };
        ajax_update_cart(data, true);

        return false;
    });

    function ajax_update_cart(data, update_checkout) {
        $('#order_review').find('.shop_table').addClass('bm-loader');
        $.post(wc_add_to_cart_params.ajax_url, data)
        .done(function (response) {
            if (response.error & response.product_url) {
                window.location = response.product_url;
                return;
            } else {
                $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
            }

            $(document.body).trigger('update_cart_from_checkout');

            if(update_checkout) {
                setTimeout(function() {
                    $('body').trigger('update_checkout');
                }, 250);
            }
        })
        .fail(function (error) {
            console.log(JSON.stringify(error));
        })
        .always(function() {
            $('#order_review').find('.shop_table').removeClass('bm-loader');
        });
    }
