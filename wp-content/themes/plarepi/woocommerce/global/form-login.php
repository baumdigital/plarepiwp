<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form id="woo-form-login" class="woocommerce-form woocommerce-form-login login" method="post" <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>
    <a class="close" href="javascript:void(0);"><i class="fas fa-times"></i></a>

	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php //echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>
    <h4 class="line"><?php esc_html_e( 'Iniciar sesión', 'baumchild' ); ?></h4>

    <div class="form-row form-row-first">
        <label for="username"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        <input type="text" class="input-text" name="username" id="username" autocomplete="username"/>
    </div>
    <div class="form-row form-row-last">
        <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        <input class="input-text" type="password" name="password" id="password" autocomplete="current-password"/>
    </div>

    <div class="clear"></div>

	<?php do_action( 'woocommerce_login_form' ); ?>

    <div class="lost_password">
        <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
    </div>
    <div class="form-row">
        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
            <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox"
                   id="rememberme" value="forever"/> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
        </label>

		<?php wp_nonce_field( 'woocommerce-login', 'security' ); ?>

        <input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>"/>

        <button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login"
                value="<?php esc_attr_e( 'Iniciar', 'baumchild' ); ?>"><?php esc_html_e( 'Iniciar', 'baumchild' ); ?></button>

        <div class="alert" role="alert" style="display: none">
            <p></p>
        </div>
    </div>

    <div class="clear"></div>

	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>
